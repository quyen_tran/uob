﻿<%@ page import="java.util.*, java.text.SimpleDateFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1, minimum-scale=0.1, maximum-scale=2.0">
	<title>UOB TDSR</title>
	
	<!-- GENERIC START -->
	<link href="assets/css/default.css?v=2" rel="stylesheet">
	<link href="assets/css/calculator.css?v=2" rel="stylesheet">
	<link href="assets/css/results.css?v=2" rel="stylesheet">
	<link href="assets/css/popup.css?v=2" rel="stylesheet">
	<link href="assets/css/tooltipsy.css?v=2" rel="stylesheet">
	<link href="assets/css/icheck/flat.css?v=2" rel="stylesheet">
	<!--[if lt IE 9]>
		<link href="assets/css/ie8overwrite.css?v=2" rel="stylesheet">
	<![endif]-->
	<!--[if gte IE 9]>
	  <style type="text/css">
		.gradient {
		   filter: none;
		}
	  </style>
	<![endif]-->
	
	<script type="text/javascript" src="assets/js/jquery-1.9.0.js"></script>
	<script type="text/javascript" src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="assets/js/fluidDialog.js"></script>
	<script type="text/javascript" src="assets/js/mdetect.js"></script>
	<script type="text/javascript" src="assets/js/jquery.hoverIntent.minified.js"></script>
	<script type="text/javascript" src="assets/js/icheck.min.js"></script>
	<script type="text/javascript" src="assets/js/tooltipsy.min.js"></script>
	<script type="text/javascript" src="assets/js/placeholder.js"></script>
	<!-- GENERIC END -->
	<!--[if lt IE 8]>
		<script type="text/javascript" src="assets/js/IE8.js?v=2"></script>
	<![endif]-->
	
	<script type="text/javascript" src="assets/js/calculator/underscore.string.min.js"></script>
	<script type="text/javascript" src="assets/js/calculator/numeric.min.js"></script>
	<script type="text/javascript" src="assets/js/calculator/accounting.min.js"></script>
	<script type="text/javascript" src="assets/js/calculator/formula.min.js"></script>
	<script type="text/javascript" src="assets/js/calculator/jquery.dateSelectBoxes.js"></script>
	<script type="text/javascript" src="assets/js/calculator/tooltipvalues.js?v=3"></script>
	<script type="text/javascript" src="assets/js/calculator/values.js?v=3"></script>
	<script type="text/javascript" src="assets/js/calculator/calculator-purchase.js?v=3"></script>
	<script type="text/javascript" src="assets/js/calculator/calculator.js?v=3"></script>

	<script type="text/javascript" src="assets/js/form_validation.js?v=3"></script>
	<script type="text/javascript" src="assets/js/form_submission.js?v=3"></script>
	
	
	
</head>
<body>
	<div class="alertDialog">
		<div class="alertDialogContent">
			
		</div>
	</div>
	<div class="tncDialog">
    <div class="logo">
				<img src="assets/img/logo.png" />
	  </div>
	  <div class="tncDialogContent">
		<h1>UOB Property Loans - TDSR Calculator Terms and Conditions</h1>
			<ol>
				<li> 
					The computation above is for illustration purposes only and is not an offer of credit/banking facilities from the Bank. 
					<br />Interest rate used to determine the monthly repayment of the Property Loan Amount are as follows:
					<ul>
						<li>SG Private Residential Property Loan / HDB Home Loan: 3.5% per annum</li>
						<li>SG Commercial Property: 4.5% per annum</li>
						<li>London Property in GBP: 3.75% per annum</li>
						<li>London Property in SGD: 3.5% per annum</li>
						<li>Australia Property in AUD: 5.8% per annum</li>
						<li>Australia Property in SGD: 3.5% per annum</li>
						<li>Japan Property in JPY: 3.85% per annum</li>
						<li>Japan Property in SGD: 3.5% per annum</li>
						<li>Malaysia Residential Property: 3.5% per annum</li>
						<li>Malaysia Commercial Property: 4.5% per annum</li>
						<li>Thailand Property: 6% per annum</li>
					</ul>
				  Rates used above are not considered as rate guarantees. For the purpose of computing TDSR / MSR, minimum interest rates of 3.5% (for residential property) and 4.5% (for non-residential property)  per annum will be used. Actual interest rates may vary.
				</li>
				<li> The Property Loan Tenor and Quantum of Financing are subjected to prevailing regulatory guidelines/requirements and the Bank's approval. </li>
				<li>Where the Property Loan is in foreign currency, the Property Loan Amount computed will be in the SGD equivalent.</li>
				<li>The Bank makes no representation or warranty, whether expressed or implied on the completeness or accuracy of the computation. The Bank accepts no liability for any error, inaccuracy or omission or losses/damages however suffered arising from use of or reliance on the computation herein.</li>
			</ol>
            United Overseas Bank Limited Co. Reg. No. 193500026Z.
		</div>
	</div>

	<div class="mWrapper">
		<div class="iWrapper headerWrapper">
			<div class="logo">
				<img src="assets/img/logo.png" />
			</div>
			<h1 class="txtBlue" align="center">UOB Property Loans - TDSR Calculator</h1>
			<h2 align="center">Calculate your <span class="txtRed">Total Debt Servicing Ratio (TDSR)</span> and <span class="txtRed">MSR</span>*</h2>
			<div class="subtitle" align="center">
				*Mortgage Servicing Ratio (MSR)  is only applicable for HDB flats and Executive Condominium units purchased directly from developer
			</div>
			
		</div>
		
		<div class="calculatorPage" style="display:none">
			<div class="panel panelGradient txtDarkBlue">
				<div class="iWrapper">
					<div class="panelContent">
						<div class="title">
							This loan is for
						</div>
						<div class="radioContainer oHide">
							<div class="radioItem mobileRadioInline">
								<label for="loanType_new"><input type="radio" onclick="GetChar('Loan Type');s_formStart('calculator:loans:tdsr');" id="loanType_new" name="loanType" value="loanType_new"> new purchase</label>
							</div>
							<div class="radioItem mobileRadioInline">
								<label for="loanType_re"><input type="radio" onclick="GetChar('Loan Type');s_formStart('calculator:loans:tdsr');" id="loanType_re" name="loanType" value="loanType_re" > refinancing</label>
							</div>
						</div>
					</div>
					<div class="panelContent">
						<div class="title">
							I want to find out
						</div>
						<div class="radioContainer">
							<div class="radioItem">
								<label for="loanOption_withoutAmt"><input type="radio" onclick="GetChar('Loan Option')" id="loanOption_withoutAmt" name="loanOption" value="loanOption_withoutAmt"> how much I can borrow and the maximum loan tenor</label>
							</div>
							<div class="radioItem">
								<label for="loanOption_withAmt"><input type="radio" onclick="GetChar('Loan Option')" id="loanOption_withAmt" name="loanOption" value="loanOption_withAmt"> my TDSR & MSR based on loan amount S$</label> <input type="text"  onkeydown="GetChar('Loan Option')" id="propertyLoanAmt" name="propertyLoanAmt" class="textBox" data-type="numerals" format="currency" />
							</div>
						</div>
					</div>
					<div class="panelContent last">
						<div class="title">
							Select  property type
						</div>
						<div class="twoColumnsSystem padBottom">
							<div class="column fLeft">
								<div class="innerWrap">
									<div class="selectText fLeft">
										Property Type
									</div>
									<div class="selectItem fLeft">
										<select onchange="GetChar('Property Type')" id="propertyType" name="propertyType" class="selectBox">
											<option value="Private_Residential">Private Residential (Singapore)</option>
											<option value="HDB">HDB Flat</option>
											<option value="Executive_Condominium">Executive Condominium</option>
											<option value="Commercial">Commercial Property (Singapore)</option>
											<option value="International_Property">International Property</option>
										</select>
										
									</div>
								</div>
							</div>
							<div class="column fRight propertyLocationContainer">
								<div class="innerWrap">
									<div class="selectText fLeft">
										Country
									</div>
									<div class="selectItem fLeft">
										<select onchange="GetChar('Country')" id="propertyLocation" name="propertyLocation" class="selectBox">
											<option value="London">London</option>
											<option value="Australia">Australia</option>
											<option value="Malaysia">Malaysia</option>
											<option value="Thailand">Thailand</option>
											<option value="Tokyo">Japan</option>
										</select>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="propertyCurrencyContainer hidden">
							<div class="subtitle">
								For international property loan, please also indicate the country or city you intend to purchase the property and the desired currency denomination of your loan. Where the Property Loan is in foreign currency, the Property Loan Amount computed will be in the SGD equivalent.
							</div>
							<div class="twoColumnsSystem">
								<div class="column fLeft">
									<div class="innerWrap oHide">
										<div class="selectText fLeft">
											Currency
										</div>
										<div class="selectItem fLeft">
											<select onchange="GetChar('Currency')" id="propertyCurrency" name="propertyCurrency" class="selectBox">
												<option value="GBP">British Pound</option>
												<option value="AUD">Australian Dollar</option>
												<option value="SGD">Singapore Dollar</option>
												<option value="USD">US Dollar</option>
												<option value="JYP">Japanese Yen</option>
											</select>
										</div>
									</div>
								</div>
								<div class="column fRight propertyTypeInnerContainer hidden">
									<div class="innerWrap">
										<div class="selectText fLeft">
											Property Type
										</div>
										<div class="selectItem fLeft">
											<select onchange="GetChar('Property Type')" id="propertyTypeInner" name="propertyTypeInner" class="selectBox">
												<option value="Residential">Residential</option>
												<option value="Commercial">Commercial</option>
											</select>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="infoPanel rel">
				<div class="iWrapper">
                <div class="sectionTitle"><strong>Important Notes:</strong></div>
					<div align="justify">The following income, asset and debt details are to be entered in SGD or SGD equivalent. This calculator is to be used for illustration purposes only and the computation obtained is not an offer of credit/banking facilities from the Bank. Computation is based on the information entered and the MAS Notice on Computation of TDSR for Property Loans. All loan applications shall be subjected to Bank's approval and prevailing regulatory guidelines/requirements. </div></div>
			</div>
			
			<div class="applicationPanel rel">
				<div class="applicantTabContainer rel oHide">
					<div class="applicantTabContent oHide fLeft">
						<div class="applicantTab active fLeft" data-applicant="applicant_0" onclick="selectApplicant($(this));">
							<span class="big">Applicant 1</span>
							<span class="small"><span class="addText">Applicant</span> 1</span>
						</div>
					</div>
					<div class="applicantTab addApplicantControl fLeft" onclick="addApplicant();">
						<span class="big">[+] Add Applicant</span>
						<span class="small">[+]</span>
					</div>
				</div>
			
				<div class="panel overlapTop rel">
					<div class="iWrapper">
						<div class="applicantOrigin" id="applicant_0">
							<div class="subPanel expand" data-expand="1">
								<div class="panelHeader rel" onclick="expandPanel($(this));">
									<div class="panelTitle">
										<div class="bigHeader">1 &nbsp; APPLICANT'S INFORMATION</div>
										<div class="smallHeader"></div>
									</div>
								</div>
								<div class="panelReveal">
									<div class="panelContent dob first last">
										<div class="dobText">Date of Birth</div>
										<div class="selectContainer">
											<select onchange="GetChar('DOB')" id="dob_day_0" name="dob_day[]" class="selectBox"> 
											</select> 
											<select onchange="GetChar('DOB')" id="dob_month_0" name="dob_month[]" class="selectBox">
											</select> 
											<select onchange="GetChar('DOB')" id="dob_year_0" name="dob_year[]" class="selectBox"> 
											</select> 
										</div>
										<div class="dobText">(Required Field)</div>
									</div>
								</div>
							</div>
							
							<div class="subPanel expand" data-expand="1">
								<div class="panelHeader rel" onclick="expandPanel($(this));">
									<div class="panelTitle">
										<div class="bigHeader">2 &nbsp; INCOME</div>
										<div class="smallHeader">Please provide the following as reflected on your latest income documents such as Notice of Assessment and payslip.
										Where there is rental income, tenancy agreement/s should have a remaining lease of at least 6 months.</div>
									</div>
								</div>
								<div class="panelReveal">
									<div class="panelContent first">
										<div class="sectionTitle">Fixed Income</div>
										<div class="formInputRow last">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Monthly Fixed Income
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"><img class="hastip tooltipMonthlyFixedIncome" title="XXXXX" src="assets/img/icon_question.png" /></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Monthly Fixed Income')" data-type="numerals" format="currency" class="textBox monthlyFixedIncome" name="monthlyFixedIncome[]" />
													</div>
												</div>	
											</div>
										</div>
									</div>
									
									<div class="panelContent">
										<div class="sectionTitle">Variable Income</div>
										<div class="formInputRow">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Monthly Variable Income
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"><img class="hastip tooltipMonthlyVariableIncome" title="XXXX" src="assets/img/icon_question.png" /></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Monthly Variable Income')" data-type="numerals" format="currency" class="textBox monthlyVariableIncome" name="monthlyVariableIncome[]" />
													</div>
												</div>
											</div>
										</div>
										<div class="formInputRow">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Annual Bonus
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"><img class="hastip tooltipAnnualBonus" title="XXXXX" src="assets/img/icon_question.png" /></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Annual Bonus')" data-type="numerals" format="currency" class="textBox annualBonus" name="annualBonus[]" />
													</div>
												</div>
											</div>
										</div>
										<div class="formInputRow last">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Annual Trade Income
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"><img class="hastip tooltipAnnualTradeIncome" title="XXXXX" src="assets/img/icon_question.png" /></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Annual Trade Income')" data-type="numerals" format="currency" class="textBox annualTradeIncome" name="annualTradeIncome[]" />
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="panelContent last rental">
										<div class="sectionTitle">Rental Income</div>
										<div class="cloneContainer">
											<div class="cloneOrigin oHide">
												<div class="formInputRow fLeft">
													<div class="formWrap oHide">
														<div class="formLabel fLeft">
															<div class="innerWrap">
																<span class="bold rentalNum">Property 1</span><br /> Monthly Rental Income
															</div>
														</div>
														<div class="formIcon fLeft">
															<div class="innerWrap"><img class="hastip tooltipMonthlyRentalIncome" title="XXXXXXXX " src="assets/img/icon_question.png" /></div>
														</div>
														<div class="formCurr fLeft">
																<div class="innerWrap"><span class="currSymbol">S$</span></div>
													  </div>
														<div class="formInput fLeft">
															<div class="innerWrap">
																<input type="text"  onkeydown="GetChar('Rental Income')" data-type="numerals" format="currency" class="textBox monthlyRentalIncome" name="monthlyRentalIncome[]" />
															</div>
														</div>
													</div>
												</div>
												<div class="formInputRow extraCol fRight last">
													<div class="formWrap oHide">
														<div class="formLabel fLeft">
															<div class="innerWrap">
																No. of landlords
															</div>
														</div>
														<div class="formIcon fLeft">
															<div class="innerWrap"><img class="hastipLeft tooltipNumberOfLandlords" title="XXXXX " src="assets/img/icon_question.png" /></div>
														</div>
														<div class="formInput fLeft">
															<div class="innerWrap">
																 <input type="text"  onkeydown="GetChar('No. of landlords')" data-type="numerals" value="1" class="textBox landlordNumber" name="landlordNumber[]" />
															</div>
														</div>
													</div>
												</div>
											</div>
											
											<div class="cloneContent">
											</div>
											<div class="cloneButton" onclick="addRental($(this));">
												<img src="assets/img/icon_plus.png" /> Add Another Rental Property
											</div>
											
										</div>
										
									</div>
									
								</div>
							</div>
							
							
							<div class="subPanel expand" data-expand="1">
								<div class="panelHeader rel" onclick="expandPanel($(this));">
									<div class="panelTitle">
										<div class="bigHeader">3 &nbsp; FINANCIAL ASSETS (OPTIONAL)</div>
										<div class="smallHeader">If you have any of the following financial assets, it can be included as part of your income stream. 
										Recognition of the following financial assets for income computation shall be subjected to the Bank's approval.</div>
									</div>
								</div>
								<div class="panelReveal">
									<div class="sectionTitleStickTop rel oHide">
										<div class="fLeft titleItem">Latest value of:</div>
										<div class="fRight infoItem">Can asset be pledged with UOB for at least 4 years? <!--<img class="hastip" title="XXX" src="assets/img/icon_question.png" />--></div>
									</div>
									
									<div class="panelContent first last">
										
										<div class="assetsRow oHide">
											<div class="formInputRow fLeft">
												<div class="formWrap oHide">
													<div class="formLabel fLeft">
														<div class="innerWrap">
															Unit Trusts
														</div>
													</div>
													<div class="formIcon fLeft">
														<div class="innerWrap"></div>
													</div>
													<div class="formCurr fLeft">
														<div class="innerWrap"><span class="currSymbol">S$</span></div>
													</div>
													<div class="formInput fLeft">
														<div class="innerWrap">
															<input type="text"  onkeydown="GetChar('Unit Trusts')" data-type="numerals" format="currency" class="textBox unitTrusts" name="unitTrusts[]" />
														</div>
													</div>	
												</div>
											</div>
											<div class="formInputRow fRight">
												<div class="checkboxContainer">
													<div class="checkboxItem">
														<input type="checkbox" id="unitTrustsCheck_0" class="unitTrustsCheck icheck" name="unitTrustsCheck[]">
													</div>
													<div class="checkboxLabel">
														<label for="unitTrustsCheck_0"> Yes</label>
													</div>
												</div>
											</div>
										</div>
										
										<div class="assetsRow oHide">
											<div class="formInputRow fLeft">
												<div class="formWrap oHide">
													<div class="formLabel fLeft">
														<div class="innerWrap">
															Bonds, Stocks & Shares
														</div>
													</div>
													<div class="formIcon fLeft">
														<div class="innerWrap"></div>
													</div>
													<div class="formCurr fLeft">
														<div class="innerWrap"><span class="currSymbol">S$</span></div>
													</div>
													<div class="formInput fLeft">
														<div class="innerWrap">
															<input type="text"  onkeydown="GetChar('Bonds, Stocks & Shares')" data-type="numerals" format="currency" class="textBox bonds" name="bonds[]" />
														</div>
													</div>	
												</div>
											</div>
											<div class="formInputRow fRight">
												<div class="checkboxContainer">
													<div class="checkboxItem">
														<input type="checkbox" id="bondsCheck_0" class="bondsCheck icheck" name="bondsCheck[]">
													</div>
													<div class="checkboxLabel">
														<label for="bondsCheck_0"> Yes</label>
													</div>
												</div>
											</div>
										</div>
										
										<div class="assetsRow oHide">
											<div class="formInputRow fLeft">
												<div class="formWrap oHide">
													<div class="formLabel fLeft">
														<div class="innerWrap">
															Foreign Currency Deposits
														</div>
													</div>
													<div class="formIcon fLeft">
														<div class="innerWrap"></div>
													</div>
													<div class="formCurr fLeft">
														<div class="innerWrap"><span class="currSymbol">S$</span></div>
													</div>
													<div class="formInput fLeft">
														<div class="innerWrap">
															<input type="text"  onkeydown="GetChar('Foreign Currency Deposits')" data-type="numerals" format="currency" class="textBox foreignCurrenctDep" name="foreignCurrenctDep[]" />
														</div>
													</div>	
												</div>
											</div>
											<div class="formInputRow fRight">
												<div class="checkboxContainer">
													<div class="checkboxItem">
														<input type="checkbox" id="foreignCurrenctDepCheck_0" class="foreignCurrenctDepCheck icheck" name="foreignCurrenctDepCheck[]">
													</div>
													<div class="checkboxLabel">
														<label for="foreignCurrenctDepCheck_0"> Yes</label>
													</div>
												</div>
											</div>
										</div>
										
										<div class="assetsRow oHide">
											<div class="formInputRow fLeft">
												<div class="formWrap oHide">
													<div class="formLabel fLeft">
														<div class="innerWrap">
															Structured Deposits
														</div>
													</div>
													<div class="formIcon fLeft">
														<div class="innerWrap"></div>
													</div>
													<div class="formCurr fLeft">
														<div class="innerWrap"><span class="currSymbol">S$</span></div>
													</div>
													<div class="formInput fLeft">
														<div class="innerWrap">
															<input type="text"  onkeydown="GetChar('Structured Deposits')" data-type="numerals" format="currency" class="textBox strucDep" name="strucDep[]" />
														</div>
													</div>	
												</div>
											</div>
											<div class="formInputRow fRight">
												<div class="checkboxContainer">
													<div class="checkboxItem">
														<input type="checkbox" id="strucDepCheck_0" class="strucDepCheck icheck" name="strucDepCheck[]">
													</div>
													<div class="checkboxLabel">
														<label for="strucDepCheck_0"> Yes</label>
													</div>
												</div>
											</div>
										</div>
										
										<div class="assetsRow oHide">
											<div class="formInputRow last fLeft">
												<div class="formWrap oHide">
													<div class="formLabel fLeft">
														<div class="innerWrap">
															Fixed / Savings Deposits
															In Singapore Dollar
														</div>
													</div>
													<div class="formIcon fLeft">
														<div class="innerWrap"></div>
													</div>
													<div class="formCurr fLeft">
														<div class="innerWrap"><span class="currSymbol">S$</span></div>
													</div>
													<div class="formInput fLeft">
														<div class="innerWrap">
															<input type="text"  onkeydown="GetChar('Fixed / Savings Deposits')" data-type="numerals" format="currency" class="textBox fixedDep" name="fixedDep[]" />
														</div>
													</div>	
												</div>
											</div>
											<div class="formInputRow last fRight">
												<div class="checkboxContainer">
													<div class="checkboxItem">
														<input type="checkbox" id="fixedDepCheck_0" class="fixedDepCheck icheck" name="fixedDepCheck[]">
													</div>
													<div class="checkboxLabel">
														<label for="fixedDepCheck_0"> Yes</label>
													</div>
												</div>
											</div>
										</div>
										
										
									</div>
								</div>
							</div>
							
							
							
							<div class="subPanel expand" data-expand="1">
								<div class="panelHeader rel" onclick="expandPanel($(this));">
									<div class="panelTitle">
										<div class="bigHeader">4 &nbsp; DEBT OBLIGATIONS</div>
										<div class="smallHeader">Please provide the following as reflected on your latest available statements. </div>
									</div>
								</div>
								<div class="panelReveal">
									<div class="panelContent first">
										<div class="formInputRow fullWidth">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Total Monthly Instalment for all car loans
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Total Monthly Instalment')" data-type="numerals" format="currency" class="textBox monthlyInstalmentCar" name="monthlyInstalmentCar[]" />
													</div>
												</div>	
											</div>
										</div>
										<div class="formInputRow fullWidth">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Total Monthly Instalment for all other property loans
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Total Monthly Instalment')" data-type="numerals" format="currency" class="textBox monthlyInstalmentPropLoans" name="monthlyInstalmentPropLoans[]" />
													</div>
												</div>	
											</div>
										</div>
										<div class="formInputRow last fullWidth">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Total Monthly Instalment for all other personal / renovation loans
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Total Monthly Instalment')" data-type="numerals" format="currency" class="textBox monthlyInstalmentPersonalLoans" name="monthlyInstalmentPersonalLoans[]" />
													</div>
												</div>	
											</div>
										</div>
									</div>
									<div class="panelContent">
										<div class="normalTitle">For credit cards, please indicate which amount you will be providing :</div>
										<div class="radioInlineContainer oHide">
											<div class="radioItem fLeft">
												<label for="cc_creditLimit_0"><input type="radio" onclick="GetChar('Credit Limit')" class="radio cc_creditLimit" id="cc_creditLimit_0" name="ccAmtType_0" value="cc_creditLimit"> Credit Limit</label> <img class="hastip tooltipCreditLimit" title="XXXXX" src="assets/img/icon_question.png" />
											</div>
											<div class="radioItem fLeft">
												<label for="cc_outstanding_0"><input type="radio" onclick="GetChar('Outstanding Balance')" class="radio cc_outstanding" id="cc_outstanding_0" name="ccAmtType_0" value="cc_outstanding"> Outstanding Balance</label> <img class="hastip tooltipOutstandingBalance" title="XXXXXXXXXXXXX" src="assets/img/icon_question.png" />
											</div>
											<div class="radioItem fLeft">
												<label for="cc_minSum_0"><input type="radio" onclick="GetChar('Minimum Sum Payable')" class="radio cc_minSum" id="cc_minSum_0" name="ccAmtType_0" value="cc_minSum"> Minimum Sum Payable</label> <img class="hastip tooltipMinimumSumPayable" title="XXXXXXX " src="assets/img/icon_question.png" />
											</div>
										</div>
										<div class="formInputRow last fullWidth">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Total Amount for all credit cards
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Total Amount')" data-type="numerals" format="currency" class="textBox monthlyRepaymentCC" name="monthlyRepaymentCC[]" />
													</div>
												</div>	
											</div>
										</div>
									</div>
									<div class="panelContent last">
										<div class="normalTitle">For unsecured non-card credit facility (e.g. cash plus, credit line), please indicate the amount you will be providing:</div>
										<div class="radioInlineContainer oHide">
											<div class="radioItem fLeft">
												<label for="rcc_creditLimit_0"><input type="radio" onclick="GetChar('Credit Limit')" class="radio rcc_creditLimit" id="rcc_creditLimit_0" name="rccAmtType_0" value="cc_creditLimit"> Credit Limit</label> <img class="hastip tooltipCreditLimitRev" title="XXXXXX " src="assets/img/icon_question.png" />
											</div>
											<div class="radioItem fLeft">
												<label for="rcc_outstanding_0"><input type="radio" onclick="GetChar('Outstanding Balance')" class="radio rcc_outstanding" id="rcc_outstanding_0" name="rccAmtType_0" value="cc_outstanding"> Outstanding Balance</label> <img class="hastip tooltipOutstandingBalanceRev" title="XXXXXX " src="assets/img/icon_question.png" />
											</div>
											<div class="radioItem fLeft">
												<label for="rcc_minSum_0"><input type="radio" onclick="GetChar('Minimum Sum Payable')" class="radio rcc_minSum" id="rcc_minSum_0" name="rccAmtType_0" value="cc_minSum"> Minimum Sum Payable</label> <img class="hastip tooltipMinimumSumPayableRev" title="XXXXXX " src="assets/img/icon_question.png" />
											</div>
										</div>
										<div class="formInputRow last fullWidth">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Total Amount for all unsecured non-card credit facilities
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Total Amount')" data-type="numerals" format="currency" class="textBox monthlyRepaymentRevolving" name="monthlyRepaymentRevolving[]" />
													</div>
												</div>	
											</div>
										</div>
									</div>
									
								</div>
							</div>
							
							
							<div class="subPanel expand" data-expand="1">
								<div class="panelHeader rel" onclick="expandPanel($(this));">
									<div class="panelTitle">
										<div class="bigHeader">5 &nbsp; GUARANTOR STATUS</div>
										<div class="smallHeader"></div>
									</div>
								</div>
								<div class="panelReveal guarantor">
									<div class="panelContent first last">
										<div class="normalTitle">Are you a Guarantor for any loans? </div>
										<div class="radioInlineContainer halfWidth oHide">
											<div class="radioItem fLeft">
												<label for="guarantor_yes_0">
													<input type="radio" class="radio guarantor_yes" id="guarantor_yes_0" name="guarantorType_0" value="guarantor_yes" onclick="checkGuarantorOption($(this));GetChar('Guarantor');">
													 Yes
												</label>
											</div>
											<div class="radioItem fLeft">
												<label for="guarantor_no_0">
													<input type="radio" class="radio guarantor_no" id="guarantor_no_0" name="guarantorType_0" value="guarantor_no" onclick="checkGuarantorOption($(this));GetChar('Guarantor');">
													 No
												</label>
											</div>
										</div>
										
										<div class="cloneContainer">
											<div class="cloneOrigin oHide">
												<div class="formInputRow fLeft">
													<div class="formWrap oHide">
														<div class="formInput fLeft">
															<div class="innerWrap">
																<select onchange="GetChar('Guarantor Instalment')" class="guarantorInstalmentOpt selectBox" name="guarantorInstalmentOpt[]">
																	<option value="corporate">Corporate Loan</option>
																	<option value="personal">Personal Loan</option>
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="formInputRow extraCol fRight last">
													<div class="formWrap oHide">
														<div class="formLabel fLeft">
															<div class="innerWrap">
																Monthly Instalment
															</div>
														</div>
														<div class="formIcon fLeft">
															<div class="innerWrap"></div>
														</div>
														<div class="formCurr fLeft">
															<div class="innerWrap"><span class="currSymbol">S$</span></div>
														</div>
														<div class="formInput fLeft">
															<div class="innerWrap">
																 <input type="text"  onkeydown="GetChar('Monthly Instalment')" data-type="numerals" format="currency" class="textBox guarantorInstalmentAmt" name="guarantorInstalmentAmt[]" />
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="cloneContent">
											</div>
											<div class="cloneButton" onclick="addGuarantor($(this));">
												<img src="assets/img/icon_plus.png" /> Add more loans
											</div>
											
										</div>
									</div>
										
										
								</div>
							</div>
							
							<div class="buttonContainer oHide">
								<!--<div class="button greyButton fRight" onclick="removeApplicant($(this));">
									Remove Applicant
								</div>-->
								<div class="button greyButton fRight" onclick="addApplicant();">
									Add Applicant
								</div>
							</div>
						</div>
						<div class="applicantClone">
						
						</div>
						
					</div>
				</div>
			</div>
			
			<div class="buttonContainer maxWidth oHide">
				<div class="button fLeft redButton" onclick="startCalculate();GetChar('Submit');">
					Calculate
				</div>
				<div class="button fLeft blueButton" onclick="resetCalculator();">
					Reset
				</div>
			</div>
			<div align="center" >United Overseas Bank Limited Co. Reg. No. 193500026Z.</div>
		</div>
		
		
		<div class="resultsPage" style="display:block">
			
			<div class="form"><a name="form"></a>
				<div class="formContent thankyouForm" style="margin-top:30px">
					<div class="txtRed formTitle">Thank you</div>
					Thank you for your interest in UOB Property Loans. We have received your submission.
					<br />Our Banker will be in touch with you within the next working day. 
				</div>
			</div>

			<div class="optionBox" align="center" style="margin-top:30px">
				<div class="optionCol" >
						<div class="optionTxt">RETURN TO TDSR CALCULATOR</div>
					<a href="index.jsp" style="color:#fff">
						<div class="btnRed" id="btnRecalculate">RECALCULATE</div>
					</a>
				</div>
			</div>
		
		</div>
		
		
	</div>
	
	<script type="text/javascript">
	window.onbeforeunload = function(e) {
       return abandon();
      };
		function abandon(){
		if(s_formTrackField!="Submit"){	
		s_formTrackSendLink(s_formTrackField);
		}
		}
		
		$(function() {
			<%
				//String string1 = "10:00:00";
				//Date time1 = new SimpleDateFormat("HH:mm:ss").parse(string1);
				Calendar calendar1 = Calendar.getInstance();
				calendar1.set(Calendar.HOUR_OF_DAY, 10);
				calendar1.set(Calendar.MINUTE, 0);
				calendar1.set(Calendar.SECOND, 0);
				
				//String string2 = "18:00:00";
				//Date time2 = new SimpleDateFormat("HH:mm:ss").parse(string2);
				Calendar calendar2 = Calendar.getInstance();
				calendar2.set(Calendar.HOUR_OF_DAY, 18);
				calendar2.set(Calendar.MINUTE, 0);
				calendar2.set(Calendar.SECOND, 0);
				
				Calendar cal  = Calendar.getInstance();
				int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
				
				Date timeNow = cal.getTime();
				if (timeNow.before(calendar1.getTime()) || timeNow.after(calendar2.getTime()) || dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
			%>	
				//Outside
				var phoneNumber = "18003882121"; //Call centre number
			<%
				}
				else
				{
			%>	
				var phoneNumberArray = ["92208719", "81861373", "92208719", "81861373"];
				var randomNumber = Math.floor((Math.random()*phoneNumberArray.length));
				var phoneNumber = phoneNumberArray[randomNumber];
			<%	
				}
			%>
			
			$("#contactPhoneNumber").attr("href", "tel:" + phoneNumber);
			//alert(phoneNumber);
			
			
		});
		function GetChar (field){
s_formTrackField=field;
//alert(s_formTrackField);
}
	</script>
<script type="text/javascript" src="assets/js/analytics.js"></script>	
<!--<script type="text/javascript" src="https://uniservices1.uobgroup.com/secure/assets/js/analytics.js"></script>-->

<!-- Google Code for Submissions Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 989083474;
var google_conversion_language = "en";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "cC2tCJ67jgkQ0u7Q1wM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/989083474/?label=cC2tCJ67jgkQ0u7Q1wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>