// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var jshint = require('gulp-jshint');
// var sass = require('gulp-sass');
var concat = require('gulp-concat');
var jade = require('gulp-jade');
var compass = require('gulp-compass');
 
gulp.task('templates', function() {
  var YOUR_LOCALS = {};
 
  gulp.src('./lib/*.jade')
    .pipe(jade({
      locals: YOUR_LOCALS,
      pretty: true
    }))
    .pipe(gulp.dest('./dist/'))
});

gulp.task('compass', function() {
  gulp.src(['sass/**/*.scss' , 'sass/**/*.sass' ])
    .pipe(compass({
      config_file: './config.rb',
      css: './dist/css/',
      sass: 'sass'
    }));
});

// build JS
gulp.task('scripts', function() {
    return gulp.src('./lib/javascripts/*.js')
        .pipe(gulp.dest('./dist/javascripts'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('lib/**/*.js', ['scripts']);
    gulp.watch(['sass/**/*.scss', 'sass/**/*.sass'], ['compass']);
    gulp.watch('lib/**/*.jade', ['templates']);
});


// Default Task
gulp.task('default', ['compass', 'scripts', 'watch']);