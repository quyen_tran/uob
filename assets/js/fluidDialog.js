$(function() {
	$(window).resize(function () {
		fluidDialog();
	});
	
	$(document).on("dialogopen", ".ui-dialog", function (event, ui) {
		fluidDialog();
	});
});

function fluidDialog() {
	var $visible = $(".ui-dialog:visible");
	// each open dialog
	$visible.each(function () {
		var $this = $(this);
		var dialog = $this.find(".ui-dialog-content").data("ui-dialog");
		// if fluid option == true
		if (dialog.options.fluid) {
			var wWidth = $(window).width();
			// check window width against dialog width
			if (wWidth < dialog.options.maxWidth + 50) {
				// keep dialog from filling entire screen
				$this.css("max-width", "80%");
			} else {
				// fix maxWidth bug
				$this.css("max-width", dialog.options.maxWidth);
			}
			//reposition dialog
			dialog.option("position", dialog.options.position);
		}
	});
}