//return error str, if invalid there is error string, if valid then empty string
function validateApplyForm(doAction, field)
{
	var valid = true;
	var errorStr = '';
	
	//Name
	if(field == undefined || field == 'txtName')
	{
		if(trim($('#txtName').val()) == '')
		{
			valid = false;
			errorStr += 'Please fill in your name.<br />';
			if(doAction)
				$("#txtName").addClass("error");
		}
		else
		{
			if(doAction)
				$("#txtName").removeClass("error");
		}
	}
	
	//NRIC
	if(field == undefined || field == 'txtNRIC')
	{
		var fieldval = trim($('#txtNRIC').val());
		if(fieldval == '')
		{
			valid = false;
			errorStr += 'Please fill in your NRIC/passport.<br />';
			if(doAction)
				$("#txtNRIC").addClass("error");
		}
		else
		{
			if (!isValidNRIC(fieldval) && !isValidPassport(fieldval)) 
			{
				valid = false;
				errorStr += 'Please fill in a valid NRIC/passport.<br />';
				if(doAction)
					$("#txtNRIC").addClass("error");
			}
			else {
				if(doAction)
					$("#txtNRIC").removeClass("error");
			}
		}
	}
	
	
	//Contact Number
	if(field == undefined || field == 'txtContactHome' || field == 'txtContactOffice' || field == 'txtContactMobile')
	{
		var fieldval_home = trim($('#txtContactHome').val());
		var fieldval_office = trim($('#txtContactOffice').val());
		var fieldval_mobile = trim($('#txtContactMobile').val());
		
		//reprocess for IE8
		if(fieldval_home == 'Home')
			fieldval_home = '';
		if(fieldval_office == 'Office')
			fieldval_office = '';
		if(fieldval_mobile == 'Mobile')
			fieldval_mobile = '';
			
		if(fieldval_home == '' && fieldval_office == '' && fieldval_mobile == '')
		{
			valid = false;
			errorStr += 'Please fill in at least one contact number<br />';
			if(doAction)
			{
				$("#txtContactHome").addClass("error");
				$("#txtContactOffice").addClass("error");
				$("#txtContactMobile").addClass("error");
			}
		}
		else 
		{
			if ((fieldval_home != '' && !isValidPhoneNumber(fieldval_home))
				|| (fieldval_office != '' && !isValidPhoneNumber(fieldval_office))
				|| (fieldval_mobile != '' && !isValidPhoneNumber(fieldval_mobile))
			)
			{
				valid = false;
				errorStr += 'Please fill in a valid contact number.<br />';
				if(doAction)
				{
					$("#txtContactHome").addClass("error");
					$("#txtContactOffice").addClass("error");
					$("#txtContactMobile").addClass("error");
				}
			}
			else {
				if(doAction)
				{
					$("#txtContactHome").removeClass("error");
					$("#txtContactOffice").removeClass("error");
					$("#txtContactMobile").removeClass("error");
				}
			}
		}
	}
	
	//Email
	if(field == undefined || field == 'txtEmail')
	{
		var fieldval = trim($('#txtEmail').val());
		if(fieldval == '')
		{
			valid = false;
			errorStr += 'Please fill in your email.<br />';
			if(doAction)
				$("#txtEmail").addClass("error");
		}
		else
		{
			if (!isValidEmail(fieldval)) 
			{
				valid = false;
				errorStr += 'Please fill in a valid email.<br />';
				if(doAction)
					$("#txtEmail").addClass("error");
			}
			else {
				if(doAction)
					$("#txtEmail").removeClass("error");
			}
		}
	}
	
	
	//TNC
	if(field == undefined || field == 'chkTNC')
	{
		var fieldval = $("#chkTNC").is(':checked')
		if(!fieldval)
		{
			valid = false;
			errorStr += 'Please agree to the terms and conditions before proceeding.<br />';
			if(doAction)
				$("#chkTNC").addClass("error");
		}
		else
		{
			if(doAction)
				$("#chkTNC").removeClass("error");
		}
	}
	
	return errorStr;
}


//return error str, if invalid there is error string, if valid then empty string
function validateChatForm(doAction, field)
{
	var valid = true;
	var errorStr = '';
	
	//Name
	if(field == undefined || field == 'txtName_Chat')
	{
		if(trim($('#txtName_Chat').val()) == '')
		{
			valid = false;
			errorStr += 'Please fill in your name.<br />';
			if(doAction)
				$("#txtName_Chat").addClass("error");
		}
		else
		{
			if(doAction)
				$("#txtName_Chat").removeClass("error");
		}
	}
	
	//NRIC
	if(field == undefined || field == 'txtNRIC_Chat')
	{
		var fieldval = trim($('#txtNRIC_Chat').val());
		if(fieldval == '')
		{
			valid = false;
			errorStr += 'Please fill in your NRIC/passport.<br />';
			if(doAction)
				$("#txtNRIC_Chat").addClass("error");
		}
		else
		{
			if (!isValidNRIC(fieldval) && !isValidPassport(fieldval)) 
			{
				valid = false;
				errorStr += 'Please fill in a valid NRIC/passport.<br />';
				if(doAction)
					$("#txtNRIC_Chat").addClass("error");
			}
			else {
				if(doAction)
					$("#txtNRIC_Chat").removeClass("error");
			}
		}
	}
	
	
	//Contact Number
	if(field == undefined || field == 'txtContactHome_Chat' || field == 'txtContactOffice_Chat' || field == 'txtContactMobile_Chat')
	{
		var fieldval_home = trim($('#txtContactHome_Chat').val());
		var fieldval_office = trim($('#txtContactOffice_Chat').val());
		var fieldval_mobile = trim($('#txtContactMobile_Chat').val());
		
		//reprocess for IE8
		if(fieldval_home == 'Home')
			fieldval_home = '';
		if(fieldval_office == 'Office')
			fieldval_office = '';
		if(fieldval_mobile == 'Mobile')
			fieldval_mobile = '';
		
		if(fieldval_home == '' && fieldval_office == '' && fieldval_mobile == '')
		{
			valid = false;
			errorStr += 'Please fill in at least one contact number<br />';
			if(doAction)
			{
				$("#txtContactHome_Chat").addClass("error");
				$("#txtContactOffice_Chat").addClass("error");
				$("#txtContactMobile_Chat").addClass("error");
			}
		}
		else 
		{
			if ((fieldval_home != '' && !isValidPhoneNumber(fieldval_home))
				|| (fieldval_office != '' && !isValidPhoneNumber(fieldval_office))
				|| (fieldval_mobile != '' && !isValidPhoneNumber(fieldval_mobile))
			)
			{
				valid = false;
				errorStr += 'Please fill in a valid contact number.<br />';
				if(doAction)
				{
					$("#txtContactHome_Chat").addClass("error");
					$("#txtContactOffice_Chat").addClass("error");
					$("#txtContactMobile_Chat").addClass("error");
				}
			}
			else {
				if(doAction)
				{
					$("#txtContactHome_Chat").removeClass("error");
					$("#txtContactOffice_Chat").removeClass("error");
					$("#txtContactMobile_Chat").removeClass("error");
				}
			}
		}
	}
	
	
	//TNC
	if(field == undefined || field == 'chkTNC_Chat')
	{
		var fieldval = $("#chkTNC_Chat").is(':checked')
		if(!fieldval)
		{
			valid = false;
			errorStr += 'Please agree to the terms and conditions before proceeding.<br />';
			if(doAction)
				$("#chkTNC_Chat").addClass("error");
		}
		else
		{
			if(doAction)
				$("#chkTNC_Chat").removeClass("error");
		}
	}
	
	return errorStr;
}


function isDigit(str) {
	var filter = /^[0-9]*$/;
	return filter.test(str);
}

function isCurrency(str) {
	var filter = /^\$?[0-9]+(\.[0-9][0-9])?$/;
	return filter.test(str);
}

function isValidNRIC(str) {
	var filter = /^[A-Za-z]{1}\d{7,8}[A-Za-z]{0,1}$/;
	return filter.test(str);
}

function isValidPassport(str) {
	var filter = /^[a-zA-Z0-9]{2}[0-9]{5,10}$/;
	return filter.test(str);
}

function isValidPhoneNumber(phone) {
	var filter = /^[0-9-+]{8,10}$/;
	return filter.test(phone);
}

function isValidPostcode(postcode) {
	var filter = /^[0-9]{4}$/;
	return filter.test(postcode);
}

function isValidEmail(email) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	return filter.test(email);
}


function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g, "");
}