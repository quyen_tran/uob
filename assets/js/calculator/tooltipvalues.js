var tooltipMonthlyFixedIncome = 'Monthly gross salary (fixed component) as per your latest pay slip or other acceptable income document. Do not include your employer’s CPF contribution.'; 

var tooltipMonthlyVariableIncome = 'Average monthly commission / overtime / allowances / variable salary component as derived from your last 12 months payslips or other acceptable income document.';

var tooltipAnnualBonus = 'Annual bonus as per your latest pay slip or other acceptable income document.';

var tooltipAnnualTradeIncome = 'Annual trade income as per your latest Notice of Assessment.';

var tooltipMonthlyRentalIncome = 'Monthly rent as per latest stamped tenancy agreement in your name. Tenancy agreement must have remaining lease of not less than 6 months. ';

var tooltipNumberOfLandlords = 'Total number of landlords as stated in the tenancy agreement. ';


var tooltipCreditLimit = 'Total Credit Limit as per your latest statements for all utilized credit cards. <br /> <img src="assets/img/tooltip_creditLimit.jpg" width="230" height="131" />';

var tooltipOutstandingBalance = 'Total Outstanding Balance as per your latest statements for all utilized credit cards. <br /> <img src="assets/img/tooltip_outstanding.jpg" width="230" height="131" />';

var tooltipMinimumSumPayable = 'Total Minimum Sum Payable as per your latest statements for all utilized credit cards. <br /> <img src="assets/img/tooltip_paysum.jpg" width="230" height="131" /> ';


var tooltipCreditLimitRev = 'Total Credit Limit as per your latest statements for all utilized credit lines. <br /> <img src="assets/img/tooltip_creditLimit.jpg" width="230" height="131" />';

var tooltipOutstandingBalanceRev = 'Total Outstanding Balance as per your latest statements for all utilized credit lines. <br /> <img src="assets/img/tooltip_outstanding.jpg" width="230" height="131" />';

var tooltipMinimumSumPayableRev = 'Total Minimum Sum Payable as per your latest statements for all utilized credit lines. <br /> <img src="assets/img/tooltip_paysum.jpg" width="230" height="131" /> ';

