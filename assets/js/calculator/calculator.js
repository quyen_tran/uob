var applicantArray = [true, false, false, false];

var rentalNumber = 1;
var guarantorNumber = 1;

var requestedLoanAmount = 0;
var loanOption = '';

$(function() {
	checkPropertyLocation();
	checkPropertyType();
	
	$("input[name=loanOption]").click(function() {
		//checkLoanOption();
	});
	
	$("#propertyLocation").change(function() {
		checkPropertyLocation();
	});
	$("#propertyType").change(function() {
		checkPropertyType();
	});
	
	$(".alertDialog").dialog({
		resizable: false,
		height: 200,
		width: 'auto',
		maxWidth: 350,
		modal: true,
		fluid: true,
		autoOpen: false,
		closeText: ''
	});
	
	$(".tncDialog").dialog({
		resizable: false,
		width: 'auto',
		maxWidth: 600,
		modal: true,
		fluid: true,
		autoOpen: false,
		closeText: ''
	});
	
	checkGuarantorOption($("input[name=guarantorType_0]"));
	setDobSelect(0);
	
	setupInputs();
	
	
	setupToolTip();
	
	
});

function setupToolTip()
{
	$(".tooltipMonthlyFixedIncome").attr("title", tooltipMonthlyFixedIncome);
	$(".tooltipMonthlyVariableIncome").attr("title", tooltipMonthlyVariableIncome);
	$(".tooltipAnnualBonus").attr("title", tooltipAnnualBonus);
	$(".tooltipAnnualTradeIncome").attr("title", tooltipAnnualTradeIncome);
	$(".tooltipMonthlyRentalIncome").attr("title", tooltipMonthlyRentalIncome);
	$(".tooltipNumberOfLandlords").attr("title", tooltipNumberOfLandlords);
	$(".tooltipCreditLimit").attr("title", tooltipCreditLimit);
	$(".tooltipOutstandingBalance").attr("title", tooltipOutstandingBalance);
	$(".tooltipMinimumSumPayable").attr("title", tooltipMinimumSumPayable);
	$(".tooltipCreditLimitRev").attr("title", tooltipCreditLimitRev);
	$(".tooltipOutstandingBalanceRev").attr("title", tooltipOutstandingBalanceRev);
	$(".tooltipMinimumSumPayableRev").attr("title", tooltipMinimumSumPayableRev);
	
	$('.hastip').tooltipsy({
		offset: [0, -10]	,
		delay: 0,
		showEvent: 'click'
	});
	
	$('.hastipLeft').tooltipsy({
		offset: [0, -10]	,
		delay: 0,
		showEvent: 'click',
		className: 'tooltipsy tooltipsyLeft'
	});
	
	$('.hastipBigWidth').tooltipsy({
		offset: [0, -10]	,
		delay: 0,
		showEvent: 'click',
		className: 'tooltipsy tooltipsyBigWidth'
	});
	
}

function openPanel(panelClass)
{
	var activatePanel = $("." + panelClass);
	$(".form .formContent").not(activatePanel).hide();
	activatePanel.show();
}

function backToCalculator()
{
	$(".calculatorPage").slideDown();
	$(".resultsPage").slideUp();
	$('html, body').animate({
		scrollTop: 0
	},500, 'easeOutExpo');
}

function resetCalculator()
{
	$(".applicationPanel").find("input[type=text]").val("");
	$('input.icheck').iCheck('uncheck');	
	$('input[type=radio]').prop('checked', false);
	$(".selectContainer select").val(0);
	$("#propertyType").val("Private_Residential");
	$("input[type=text]#propertyLoanAmt ").val("");
	checkPropertyType();
	
	selectApplicant($("*[data-applicant=applicant_0]"));
	scrollToObject($(".applicantTabContainer"));
}

function expandPanel(obj)
{
	var subPanel = obj.closest(".subPanel");
	var expand = subPanel.attr("data-expand");
	if(expand == "0")
	{
		subPanel.find(".panelReveal").slideDown();
		subPanel.addClass("expand");
		subPanel.attr("data-expand", "1");
	}
	else
	{
		subPanel.find(".panelReveal").slideUp();
		subPanel.removeClass("expand");
		subPanel.attr("data-expand", "0");
	}
}

function numeralsOnly(event, el) {
	// Allowed key press on input fields only numerals with decimal points
	var val = $(el).val()
	// Force only one "." in the text field
	if (val.indexOf(".") !== -1) { if (event.keyCode == 190 || event.keyCode == 110) { 
		event.preventDefault(); 
	}}
	// Allow: backspace, delete, tab, escape, enter, period, Ctrl+A, home, end, left, right
	if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39)) {
		// let it happen, don't do anything
		return;
	} else {
		// Ensure that it is a number and stop the keypress
		if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
			event.preventDefault(); 
		}   
	}
}

function formatCurrency (event, el) {
	var v = accounting.formatMoney($(el).val(), {symbol:"", precision: 0});
	$(el).val(v);
}

function setupInputs () {
	$("input[data-type=numerals]").unbind();
	$("input[format=currency]").unbind();

	$("input[data-type=numerals]").keydown(function (event) { 	
		numeralsOnly(event, $(this)); 
	})
	$("input[format=currency]").on("blur change",function (event) {	
		formatCurrency(event, $(this)); 
	});
	
	$('input.icheck').iCheck({
		checkboxClass: 'icheckbox_flat',
		radioClass: 'iradio_flat'
	});
	
}


function startCalculate()
{
	$('.thankyouForm').css('display','none');
	
	
	var tdsrFail = false;
	var msrFail = false;

	if(fieldValidation())
	{
		var income = getGrossMonthlyIncome();
		var debt = getMonthlyTotalDebtObligations();
		
		loanOption = $('input[name=loanOption]:checked').val().toLowerCase();
		if(loanOption == "loanoption_withamt")
		{
			requestedLoanAmount = Number($("#propertyLoanAmt").val().replace(/\,/g,""));
		}
		else 
		{
			requestedLoanAmount = 0;	
		}
		
		$(".result_totalIncome").text(accounting.formatMoney(income, {symbol: currencySymbol, precision: 0}));
		$(".result_totalDebt").text(accounting.formatMoney(debt, {symbol: currencySymbol, precision: 0}));
		
		//Step1: Find out applicant's eligibility for loan
		if(income == 0) income = 1;
		
		var tdsr = debt/income * 100;
		tdsr = parseFloat(roundNumber(tdsr, 100, 'up')).toFixed(2);
		var step1pass = true;
		
		if(tdsr > 60)
		{
			tdsrFail = true;
			//step1pass = true;
		}	
		
		if(tdsr > 60 && requestedLoanAmount == 0)
		{
			step1pass = false;
		}
		
		if(step1pass)
		{
			$(".resultsAlert").hide();
			
			//Step2: Find out max loan amount
			var valueArray = getInterestRateAndTenor(); //1 = interest rate, 2 = tenor years
			var interestRate = valueArray[0] / 100;
			var maxLoanTenor = valueArray[1];
			
			
			var pmtForLoanAmt = 0;
			var monthlyPMT = 0;
			var tdsrStep2 = 0;
			
			if(requestedLoanAmount == 0)
			{
				//Option 1
				monthlyPMT = (0.6 * income) - debt;
				monthlyPMT = roundNumber(monthlyPMT, 1, 'down');
				
				maxLoanAmt = roundNumber(-Formula.PV(interestRate / 12, maxLoanTenor * 12, monthlyPMT, 0, 0), 1, 'down');
				
				tdsrStep2 = (debt + monthlyPMT) / income * 100;
				tdsrStep2 = parseFloat(roundNumber(tdsrStep2, 100, 'up')).toFixed(2);
				
				/* the following seems to be redundant */
				pmtForLoanAmt = -Formula.PMT(interestRate / 12, maxLoanTenor * 12, maxLoanAmt, 0, 0);
				pmtForLoanAmt = roundNumber(pmtForLoanAmt, 1, 'down'); 
				
			}
			else
			{
				//Option 2
				pmtForLoanAmt = -Formula.PMT(interestRate / 12, maxLoanTenor * 12, requestedLoanAmount, 0, 0);
				pmtForLoanAmt = roundNumber(pmtForLoanAmt, 1, 'up');
				
				maxLoanAmt = requestedLoanAmount;
				
				tdsrStep2 = (debt + pmtForLoanAmt) / income * 100;
				tdsrStep2 = parseFloat(roundNumber(tdsrStep2, 100, 'up')).toFixed(2);	
				
				if(tdsrStep2 > 60)
				{
					tdsrFail = true;
					//openAlert("Your TDSR is over 60%!");
				}
			}
		
			
			$(".result_loanAmount").text(accounting.formatMoney(requestedLoanAmount, {symbol: currencySymbol, precision: 0}));
			$(".result_interestRate").text(interestRate);
			$(".result_loanTenor").text(maxLoanTenor);
			$(".result_monthlyPMT").text(accounting.formatMoney(monthlyPMT, {symbol: currencySymbol, precision: 0}));
			$(".result_loanPMT").text(accounting.formatMoney(pmtForLoanAmt, {symbol: currencySymbol, precision: 2}));
			$(".result_tdsr").text(tdsrStep2 + "%");
			$(".result_maxLoan").text(accounting.formatMoney(maxLoanAmt, {symbol: currencySymbol, precision: 0}));
			$(".result_msr").text("-");
			
			
			var selectedPropertyType = $("#propertyType").val().toLowerCase();
			if(selectedPropertyType == "executive_condominium" || selectedPropertyType == "hdb")
			{
				var totalMonthlyPropLoans = getMonthlyInstalmentPropLoans();
				var msr = ((pmtForLoanAmt + totalMonthlyPropLoans) / income) * 100;
				msr = parseFloat(roundNumber(msr, 100, 'up')).toFixed(2);
				$(".result_msr").text(msr + "%");
				
				if(requestedLoanAmount == 0 && msr > 30)
				{
					pmtForLoanAmt = 0.3 * income - totalMonthlyPropLoans;
					pmtForLoanAmt = roundNumber(pmtForLoanAmt, 1, 'down');
					msr = ((pmtForLoanAmt + totalMonthlyPropLoans) / income) * 100;
					//msr = parseFloat(msr).toFixed(2);
					msr = parseFloat(roundNumber(msr, 100, 'up')).toFixed(2);
					if(msr > 30) msr = 30; //When requested loan amount is 0, round down msr to 30.
					
					maxLoanAmt = roundNumber(-Formula.PV(interestRate / 12, maxLoanTenor * 12, pmtForLoanAmt, 0, 0), 1, 'down');
					tdsrStep2 = (debt + pmtForLoanAmt) / income * 100;
					tdsrStep2 = parseFloat(roundNumber(tdsrStep2, 100, 'up')).toFixed(2);
					
					if(tdsrStep2 > 60) tdsrStep2 = 60; //When requested loan amount is 0, round down tdsr to 60.
					
					$(".result_tdsr").text(tdsrStep2 + "%");
					$(".result_msr").text(msr + "%");
					$(".result_maxLoan").text(accounting.formatMoney(maxLoanAmt, {symbol: currencySymbol, precision: 0}));
					
				}
				else
				{
					if(msr > 30)
					{
						msrFail = true;
						//openAlert("Your MSR is over 30%!");
					}
				}
				
				
			}
            //calculate for purchase max price
            calculatorPurchaseMaxPrice(maxLoanAmt);
			/*if(loanOption == "loanoption_withamt")
			{
				$(".result_maxLoan").text(accounting.formatMoney(requestedLoanAmount, {symbol: currencySymbol, precision: 0}));
				//$(".txtLoanAmount").text("Property Loan Amount");
			}
			else
			{
				//$(".txtLoanAmount").text("Max Loan Amount");
					
			}*/
			
			$(".calculatorPage").slideUp();
			$(".resultsPage").slideDown(2000);
			s_utilities('calculator:loans:tdsr');
			$('html, body').animate({
				scrollTop: 0
			},500, 'easeOutExpo');
			
			if(msrFail || tdsrFail)
			{
				var errorTxt = "";
				
				if(msrFail) errorTxt += "Your MSR is over 30%. <br />";
				if(tdsrFail) errorTxt += "Your TDSR is over 60%. <br />";
				
				$(".tdsrmsrResults").html(errorTxt);
			}
			else
			{
				$(".tdsrmsrResults").html('');
			}
			
			
		}
		else
		{
			openAlert("Your TDSR is over 60%!");
		}
	}
}

function checkPropertyType()
{
	var propertyType = $("#propertyType").val().toLowerCase();
    $(".propertyNumberHouseLoanContainer").hide();
    $(".propertyLocationContainer").hide();
    $(".propertyCurrencyContainer").hide();
    $(".propertyResidentContainer").hide();
    if(propertyType == 'private_residential'){
        $(".propertyNumberHouseLoanContainer").show();
    }else if(propertyType == 'hdb' || propertyType == 'executive_condominium' || propertyType == 'commercial'){
        $(".propertyResidentContainer").show();
    }
	else if(propertyType == "international_property")
	{
		$(".propertyLocationContainer").show();
		$(".propertyCurrencyContainer").show();
	}
}

function checkPropertyLocation()
{
	var selected = $("#propertyLocation").val().toLowerCase();
	$(".propertyTypeInnerContainer").hide();
	switch(selected)
	{
		case 'london': { 
			var selectedCurr = londonArray;
		} break;
		case 'australia': { 
			var selectedCurr = australiaArray;
		} break;
		case 'tokyo': { 
			var selectedCurr = tokyoArray;
		} break;
		case 'thailand': { 
			var selectedCurr = thailandArray;
		} break;
		case 'malaysia': { 
			var selectedCurr = malaysiaArray;
			$(".propertyTypeInnerContainer").show();
		} break;
	}
	$('#propertyCurrency').empty();
	for(var i = 0; i < selectedCurr.length; i++)
	{
		$('#propertyCurrency').append($('<option>', {
			value: selectedCurr[i][0],
			text: selectedCurr[i][1]
		}));
	}
	$(".propertyCurrencyContainer").show();

}

/*
function checkLoanOption()
{
	var selectedLoan = $('input[name=loanOption]:checked').val();
	if(selectedLoan != undefined)
	{
		selectedLoan = selectedLoan.toLowerCase();
		if(selectedLoan == "loanoption_withamt")
			$(".propertyLoanContainer").show();
		else
			$(".propertyLoanContainer").hide();
	}
}*/

function checkGuarantorOption(obj)
{
	var selected = $("input[name=" + obj.attr("name") + "]:checked").val();
	
	if(selected == "guarantor_yes")
		obj.closest(".panelContent").find(".cloneContainer").show();
	else
	{
		var container = obj.closest(".panelContent").find(".cloneContainer");
		container.find(".guarantorInstalmentAmt").val(0);
		container.hide();
			
	}
}

function addRental(obj)
{
	var cloneItem = obj.closest(".cloneContainer").children(".cloneOrigin").clone();
	
	rentalNumber++;
	cloneItem.find("input").val("");
	cloneItem.find("input.landlordNumber").val("1");
	
	
	var delBut = '<div class="formRemove fLeft" onclick="removeItem($(this));"><div class="innerWrap"><div class="small">Remove</div> <img src="assets/img/icon_remove.png" /></div></div>';
	cloneItem.find(".formInputRow.fRight .formWrap").append(delBut);
	
	cloneItem.find(".rentalNum").text("Property " + rentalNumber);
	cloneItem.appendTo(obj.closest(".cloneContainer").find(".cloneContent"));
	
	cloneItem.find('.formInputRow.fLeft .hastip').attr('title', tooltipMonthlyRentalIncome);
	cloneItem.find('.formInputRow.fRight .hastipLeft').attr('title', tooltipNumberOfLandlords);
	cloneItem.find('.hastip').tooltipsy({
		offset: [0, -10],
		delay: 0,
		showEvent: 'click'
	});
	cloneItem.find('.hastipLeft').tooltipsy({
		offset: [0, -10],
		delay: 0,
		showEvent: 'click',
		className: 'tooltipsy tooltipsyLeft'
	});
	
	setupInputs();
}
function addGuarantor(obj)
{
	var cloneItem = obj.closest(".cloneContainer").children(".cloneOrigin").clone();
	
	guarantorNumber++;
	cloneItem.find("input").val("");
	
	var delBut = '<div class="formRemove fLeft" onclick="removeItem($(this));"><div class="innerWrap"><div class="small">Remove</div> <img src="assets/img/icon_remove.png" /></div></div>';
	cloneItem.find(".formInputRow.fRight .formWrap").append(delBut);
	
	cloneItem.appendTo(obj.closest(".cloneContainer").find(".cloneContent"));
	
	setupInputs();
}

function removeItem(obj)
{
	obj.closest(".cloneOrigin").remove();
}

function pickApplicantIndex()
{
	var applicantIndex = -1;
	for(var i = 0; i < applicantArray.length; i++)
	{
		if(!applicantArray[i])
		{
			applicantIndex = i;
			applicantArray[i] = true;
			break;
		}
	}
	
	return applicantIndex;
}

function getApplicantNum()
{
	var applicantNum = 0;
	for(var i = 0; i < applicantArray.length; i++)
	{
		if(applicantArray[i])
		{
			applicantNum++;
		}
	}
	
	return applicantNum;
}

function addApplicant()
{
	var applicantIndex = pickApplicantIndex();
	if(applicantIndex < 0)
	{
		openAlert("You have added the maximum number of applicants.");
	}
	else
	{
		//Check applicants Number
		if(getApplicantNum() == applicantArray.length)
		{
			$(".addApplicantControl").hide();	
		}
		
		
		var cloneItem = $(".panel .iWrapper").children(".applicantOrigin").clone();
		cloneItem.attr("id", "applicant_" + applicantIndex);
		
		var removeButton = '<div class="button greyButton fRight" onclick="removeApplicant($(this));">Remove Applicant</div>';
		cloneItem.children('.buttonContainer').prepend(removeButton);
		
		var applicantTabHTML = '<div class="applicantTab fLeft" data-applicant="applicant_' + applicantIndex + '" onclick="selectApplicant($(this));">' +
									'<span class="big">Applicant '+ (applicantIndex + 1) +'</span>' +
									'<span class="small"><span class="addText">Applicant</span> '+ (applicantIndex + 1) +'</span>' +
							   '</div>';
		
		$(".applicantTabContent").append(applicantTabHTML);
		
		
		cloneItem.find(".radio").each(function() {
			$(this).attr("name", $(this).attr("name").replace("_0", "_" + applicantIndex));
			var newID = $(this).attr("id").replace("_0", "_" + applicantIndex);
			$(this).attr("id", newID);
			$(this).closest("label").attr("for", newID);
			$(this).prop('checked', false);
		});
		cloneItem.find(".dob").find("select").each(function() {
			$(this).attr("id", $(this).attr("id").replace("_0", "_" + applicantIndex));
		});
		cloneItem.find(".icheck").each(function() {
			var checkboxName = $(this).attr("class");
			var checkboxContainer = $(this).closest(".checkboxContainer");
			var newID = $(this).attr("id").replace("_0", "_" + applicantIndex);
			checkboxContainer.find("label").attr("for", newID);
			checkboxContainer.find(".checkboxItem").remove();
			
			var newCheckBoxItem = '<div class="checkboxItem"><input type="checkbox" id="' + newID + '" class="' + checkboxName + ' icheck" name="' + checkboxName + '[]"></div>';
			checkboxContainer.prepend(newCheckBoxItem);
			//$(this).attr("id", newID);
		});
		
		cloneItem.find(".cloneContainer").children(".cloneContent").empty();
		cloneItem.find("input[type=text]").val("");
		cloneItem.hide();
		cloneItem.appendTo($(".applicationPanel .iWrapper").children(".applicantClone"));
		
		cloneItem.find(".guarantor").find(".cloneContainer").hide();
		
		setDobSelect(applicantIndex);
		setupInputs();
		
		setupToolTip();
	
		selectApplicant($("*[data-applicant=applicant_" + applicantIndex + "]"));
		scrollToObject($(".applicantTabContainer"));
	}
}

function removeApplicant(obj)
{
	var confirmation = window.confirm("Are you sure you want to delete this applicant?")
	if (confirmation)
	{
		var applicantOrigin = obj.closest(".applicantOrigin");
		var applicantIndex = parseInt(applicantOrigin.attr("id").replace("applicant_", ""));
		applicantArray[applicantIndex] = false;
		
		applicantOrigin.remove();
		
		var applicantTab = $("*[data-applicant=applicant_" + applicantIndex + "]");
		if(applicantTab.hasClass("active"))
		{
			selectApplicant($("*[data-applicant=applicant_0]"));
		}
		applicantTab.remove();
		
		$(".addApplicantControl").show();	
		
	}
}

function selectApplicant(obj)
{
	$(".applicantTab.active").removeClass("active");
	obj.addClass("active");
	var applicantPanelId = obj.attr("data-applicant");
	var newActivePanel = $("#" + applicantPanelId);
	$(".applicantOrigin").not(newActivePanel).hide();
	newActivePanel.show();
	
	$(".applicantTab").find(".addText").addClass("hidden");
	obj.find(".addText").removeClass("hidden");
}

function setDobSelect(idnum)
{
	$().dateSelectBoxes({
		monthElement: $('#dob_month_' + idnum),
		dayElement: $('#dob_day_' + idnum),
		yearElement: $('#dob_year_' + idnum),
		keepLabels: true,
		generateOptions: true,
		yearMin: (new Date().getFullYear() - 65)
	});
}


function getGrossMonthlyIncome(applicantID)
{
	if(applicantID == undefined)
		applicantID = -1;

	var totalIncome = 0;
	
	if(applicantID == -1)
	{
		$(".monthlyFixedIncome").each(function() {
			totalIncome += (Number($(this).val().replace(/\,/g,"")) * rate_monthlyFixedIncome);
		});
		
		$(".monthlyVariableIncome").each(function() {
		   totalIncome += (Number($(this).val().replace(/\,/g,"")) * rate_monthlyVariableIncome);
		});
		
		$(".annualBonus").each(function() {
		   totalIncome += (Number($(this).val().replace(/\,/g,"")) / month_annualBonus * rate_annualBonus);
		});
		
		$(".annualTradeIncome").each(function() {
		   totalIncome += (Number($(this).val().replace(/\,/g,"")) / month_annualTradeIncome * rate_annualTradeIncome);
		});
		
		var rentalArray = new Array();
		var landlordArray = new Array();
		
		$(".monthlyRentalIncome").each(function() {
		   rentalArray.push(Number($(this).val().replace(/\,/g,"")));
		});
		$(".landlordNumber").each(function() {
		   landlordArray.push(Number($(this).val().replace(/\,/g,"")));
		});
		
		for(var i = 0; i < rentalArray.length; i++)
		{
			if(landlordArray[i] == 0)
				landlordArray[i] = 1;
				
			totalIncome += ((rentalArray[i] / landlordArray[i]) * rate_monthlyRentalIncome);
		}
		
	}
	else
	{
		
		totalIncome += (Number($("#applicant_" + applicantID).find(".monthlyFixedIncome").val().replace(/\,/g,"")) * rate_monthlyFixedIncome);
		
		totalIncome += (Number($("#applicant_" + applicantID).find(".monthlyVariableIncome").val().replace(/\,/g,"")) * rate_monthlyVariableIncome);
		totalIncome += (Number($("#applicant_" + applicantID).find(".annualBonus").val().replace(/\,/g,"")) / month_annualBonus * rate_annualBonus);
		totalIncome += (Number($("#applicant_" + applicantID).find(".annualTradeIncome").val().replace(/\,/g,"")) / month_annualTradeIncome * rate_annualTradeIncome);
		
		var rentalArray = new Array();
		var landlordArray = new Array();
		
		$("#applicant_" + applicantID).find(".monthlyRentalIncome").each(function() {
		   rentalArray.push(Number($(this).val().replace(/\,/g,"")));
		});
		$("#applicant_" + applicantID).find(".landlordNumber").each(function() {
		   landlordArray.push(Number($(this).val().replace(/\,/g,"")));
		});
		
		for(var i = 0; i < rentalArray.length; i++)
		{
			if(landlordArray[i] == 0)
				landlordArray[i] = 1;
				
			totalIncome += ((rentalArray[i] / landlordArray[i]) * rate_monthlyRentalIncome);
		}
		
	}
	
	//Financial Assets
	totalIncome += getFinancialAssetsTotal("unitTrusts", rate_unitTrusts_pledged, rate_unitTrusts_unpledged, month_unitTrusts_pledged, month_unitTrusts_unpledged, applicantID);
	totalIncome += getFinancialAssetsTotal("bonds", rate_bonds_pledged, rate_bonds_unpledged, month_bonds_pledged, month_bonds_unpledged, applicantID);
	totalIncome += getFinancialAssetsTotal("foreignCurrenctDep", rate_foreignCurrenctDep_pledged, rate_foreignCurrenctDep_unpledged, month_foreignCurrenctDep_pledged, month_foreignCurrenctDep_unpledged, applicantID);
	totalIncome += getFinancialAssetsTotal("strucDep", rate_strucDep_pledged, rate_strucDep_unpledged, month_strucDep_pledged, month_strucDep_unpledged, applicantID);
	totalIncome += getFinancialAssetsTotal("fixedDep", rate_fixedDep_pledged, rate_fixedDep_unpledged, month_fixedDep_pledged, month_fixedDep_unpledged, applicantID);

	return totalIncome;
}

function getMonthlyTotalDebtObligations()
{
	var totalDebt = 0;
	
	$(".monthlyInstalmentCar").each(function() {
	   totalDebt += (Number($(this).val().replace(/\,/g,"")) * rate_monthlyInstalmentCar);
	});
	$(".monthlyInstalmentPropLoans").each(function() {
	   totalDebt += (Number($(this).val().replace(/\,/g,"")) * rate_monthlyInstalmentPropLoans);
	});
	$(".monthlyInstalmentPersonalLoans").each(function() {
	   totalDebt += (Number($(this).val().replace(/\,/g,"")) * rate_monthlyInstalmentPersonalLoans);
	});
	
	totalDebt += getCCRepaymentAmt("monthlyRepaymentCC", rate_monthlyRepaymentCC_creditLimit, rate_monthlyRepaymentCC_outstanding, rate_monthlyRepaymentCC_minSum, "ccAmtType");
	totalDebt += getCCRepaymentAmt("monthlyRepaymentRevolving", rate_monthlyRepaymentRevolving_creditLimit, rate_monthlyRepaymentRevolving_outstanding, rate_monthlyRepaymentRevolving_minSum, "rccAmtType");
	
	totalDebt += getGuarantorDebt();
	
	return totalDebt;
}

function getMonthlyInstalmentPropLoans()
{
	var totalDebt = 0;
	$(".monthlyInstalmentPropLoans").each(function() {
	   totalDebt += (Number($(this).val().replace(/\,/g,"")) * rate_monthlyInstalmentPropLoans);
	});
	return totalDebt;
}

function getFinancialAssetsTotal(className, rate_pledge, rate_unpledge, month_pledge, month_unpledge, applicantID)
{
	var financialArray = new Array();
	var k = 0;
	var returnValue = 0;
	
	if(applicantID == -1)
	{
		$("." + className).each(function() {
			financialArray.push(Number($(this).val().replace(/\,/g,"")));
		});
		$("." + className + "Check").each(function() {	
			
			if($(this).is(':checked'))
			{
				returnValue += ((financialArray[k] * rate_pledge) / month_pledge);
				
			}
			else
			{
				returnValue += ((financialArray[k] * rate_unpledge) / month_unpledge);
			}
			k++;
		});
	}
	else
	{
		var countValue = Number($("#applicant_" + applicantID).find("." + className).val().replace(/\,/g,""));
		if($("#applicant_" + applicantID).find("." + className + "Check").is(':checked'))
			returnValue += ((countValue * rate_pledge) / month_pledge);
		else
			returnValue += ((countValue * rate_unpledge) / month_unpledge);
	}
	
	return roundNumber(returnValue, 1, 'down');
}

function getGuarantorDebt()
{
	var x = 0;
	var returnAmt = 0;
	var guarantorOptArray = new Array();
	$(".guarantorInstalmentOpt").each(function() {
	    guarantorOptArray.push($(this).val());
	});
	$(".guarantorInstalmentAmt").each(function() {
		
		if(guarantorOptArray[x] == "corporate")
		{
			returnAmt += (Number($(this).val().replace(/\,/g,"")) * rate_guarantorInstalmentAmt_corporate);
		}
		else 
		{
			returnAmt += (Number($(this).val().replace(/\,/g,"")) * rate_guarantorInstalmentAmt_personal);
		}
		
		x++;
	});
	
	return returnAmt;
}

function getCCRepaymentAmt(className, rate_creditLimit, rate_outstanding, rate_minSum, radioName)
{
	var p = 0;
	var returnAmt = 0;
	$("." + className).each(function() {
		var ccAmtType = $("input[name=" + radioName + "_" + p + "]:checked").val();
		if(ccAmtType != undefined)
			ccAmtType = ccAmtType.toLowerCase();
		switch(ccAmtType)
		{
			case 'cc_creditlimit': {
				var ccVal = (Number($(this).val().replace(/\,/g,"")) * rate_creditLimit);
				if(ccVal < 50 && ccVal > 0)
				{
					ccVal = 50;
				}
				returnAmt += ccVal;
			} break;
			case 'cc_outstanding': {	
				var ccVal = (Number($(this).val().replace(/\,/g,"")) * rate_outstanding);
				if(ccVal < 50 && ccVal > 0)
				{
					ccVal = 50;
				}
				returnAmt += ccVal;
			} break;
			case 'cc_minsum': {
				returnAmt += (Number($(this).val().replace(/\,/g,"")) * rate_minSum);
			} break;
			default: {
				returnAmt += (Number($(this).val().replace(/\,/g,"")));
			}
		}
		p++;
	});
	
	return returnAmt;
}

function getAge(yearInValue, month, date)
{
	var year = (new Date().getFullYear() - yearInValue + 1);
	var dob = new Date(year, month - 1, date);
	
	if (!Date.now) {
		Date.now = function() {
			return +new Date();
		};
	}
	var ageDifMs = Date.now() - dob.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
	
    return Math.abs(ageDate.getUTCFullYear() - 1970);
	
}

function getInterestRateAndTenor()
{
	var interestRate = 0;
	var maxTenor = 0;
	var loanTenor = 0;
	var weightedAvgAge = 0;
	var selectedPropertyType = $("#propertyType").val().toLowerCase();
	var selectedLoanType = $('input[name="loanType"]:checked').val().toLowerCase();
	
	switch(selectedPropertyType)
	{
		case "international_property":
		{
			var selectedLocation = $("#propertyLocation").val().toLowerCase();
			var selectedPropertyType = $("#propertyTypeInner").val().toLowerCase();
			var selectedCurrency = $("#propertyCurrency").val().toLowerCase();
			switch(selectedLocation)
			{
				case 'london': { 
					if(selectedCurrency == "sgd")
						interestRate = rate_london_sgd;
					else if(selectedCurrency == "gbp")
						interestRate = rate_london_gbp;
				
					maxTenor = tenor_london; 
					weightedAvgAge = weightedAvgAge_london;
				} break;
				case 'australia': { 
					if(selectedCurrency == "sgd")
						interestRate = rate_australia_sgd;
					else if(selectedCurrency == "aud")
						interestRate = rate_australia_aud;
					
					maxTenor = tenor_australia; 
					weightedAvgAge = weightedAvgAge_australia;
				} break;
				case 'tokyo': { 
					if(selectedCurrency == "sgd")
						interestRate = rate_tokyo_sgd;
					else if(selectedCurrency == "jpy")
						interestRate = rate_tokyo_jpy;
					
					maxTenor = tenor_tokyo;
					weightedAvgAge = weightedAvgAge_tokyo;					
				} break;
				case 'thailand': { 
					if(selectedCurrency == "sgd")
						interestRate = rate_thailand_sgd;
					else if(selectedCurrency == "usd")
						interestRate = rate_thailand_usd;
					
					maxTenor = tenor_thailand; 
					weightedAvgAge = weightedAvgAge_thailand;
				} break;
				case 'malaysia': { 
				
					if(selectedPropertyType == "residential")
					{
						interestRate = rate_malaysia_res_sgd;
						maxTenor = tenor_malaysia_res; 
						weightedAvgAge = weightedAvgAge_malaysia_res;
					}
					else if(selectedPropertyType == "commercial")
					{
						interestRate = rate_malaysia_com_sgd;
						maxTenor = tenor_malaysia_com; 
						weightedAvgAge = weightedAvgAge_malaysia_com;
					}
				} break;
			}
		} break;
		case "private_residential": 
		{
			interestRate = rate_ec_sgd;
			if(selectedLoanType == "loantype_new")
			{
				maxTenor = tenor_ec_new;
				weightedAvgAge = weightedAvgAge_ec_new;
			}
			else
			{
				maxTenor = tenor_ec_re;
				weightedAvgAge = weightedAvgAge_ec_re;
			}
		} break;
		case "executive_condominium": 
		{
			interestRate = rate_ec_sgd;
			if(selectedLoanType == "loantype_new")
			{
				maxTenor = tenor_ec_new;
				weightedAvgAge = weightedAvgAge_ec_new;
			}
			else
			{
				maxTenor = tenor_ec_re;
				weightedAvgAge = weightedAvgAge_ec_re;
			}
		} break;
		case "hdb": 
		{
			interestRate = rate_hdb_sgd;
			if(selectedLoanType == "loantype_new")
			{
				maxTenor = tenor_hdb_new;
				weightedAvgAge = weightedAvgAge_hdb_new;
			}
			else
			{
				maxTenor = tenor_hdb_re;
				weightedAvgAge = weightedAvgAge_hdb_re;
			}
		} break;
		case "commercial": 
		{
			interestRate = rate_com_sgd;
			maxTenor = tenor_com;
			weightedAvgAge = weightedAvgAge_com;
		} break;
	}
	
	var applicantNumber = getApplicantNum();
	
	/*if(applicantNumber > 1)
	{*/
		var totalIncome = getGrossMonthlyIncome();
		
		var incomeWeightedAge = 0;
		for(var i = 0; i < applicantNumber; i++)
		{
			var age = getAge($("#dob_year_" + i).val(), $("#dob_month_" + i).val(), $("#dob_day_" + i).val());
			
			var individualIncome = getGrossMonthlyIncome(i);
			//alert('applicant ' + (i+1) + ' individual income: '  + individualIncome + '; applicant ' + (i+1) + ' age: ' + age);
			if(totalIncome > 0)
				incomeWeightedAge += ((individualIncome / totalIncome) * age);
			else
				incomeWeightedAge = 0;
		}
		
		incomeWeightedAge = roundNumber(incomeWeightedAge, 1, 'up');
		
		
		loanTenor = weightedAvgAge - incomeWeightedAge;
		if(loanTenor > maxTenor)
			loanTenor = maxTenor;
	//}
	
	var valueArray = [interestRate, loanTenor];
	
	return valueArray;
}

function fieldValidation()
{
	valid = true;
	errorMsg = '';
	
	var applicantNumber = getApplicantNum();
	for(var i = 0; i < applicantNumber; i++)
	{
		var year = $("#dob_year_" + i).val();
		var month = $("#dob_month_" + i).val();
		var date = $("#dob_day_" + i).val();
		
		if(year == 'Year' || month == 'Month' || date == 'Day')
		{
			valid = false;
			errorMsg += "Please fill in all applicants' age.<br />";
			break;
		}
		
		var applicantAge = getAge(year, month, date);
		
		if(applicantAge < 21 || applicantAge > 65)
		{
			valid = false;
			errorMsg += "Please ensure all applicant's age between 21 and 65.<br />";
			break;	
		}
		
	}
	
	if (!$('[name="loanType"]').is(':checked'))
	{
		valid = false;
		errorMsg += "Please select your loan type.<br />";
	}
	
	if (!$('[name="loanOption"]').is(':checked'))
	{
		valid = false;
		errorMsg += "Please select if you have any requested loan amount.<br />";
	}
	
	//Check rental landlord mandatory
	var rentalArray = new Array();
	var landlordArray = new Array();
	$(".monthlyRentalIncome").each(function() {
	   rentalArray.push(Number($(this).val().replace(/\,/g,"")));
	});
	$(".landlordNumber").each(function() {
	   landlordArray.push(Number($(this).val().replace(/\,/g,"")));
	});
	for(var i = 0; i < rentalArray.length; i++)
	{
		if(rentalArray[i] != 0)
		{
			if(landlordArray[i] == 0)
			{
				valid = false;
				errorMsg += "Please fill in number of landlord for each rental income.<br />";
				break;
			}
		}
	}
	
	//Check credit card minimum sum > 50
	if(!validateMinSum("monthlyRepaymentCC", "ccAmtType"))
	{
		valid = false;
		errorMsg += "Please enter at least $50 for Credit Cards Minimum Sum Payable.<br />";
	}
	if(!validateMinSum("monthlyRepaymentRevolving", "rccAmtType"))
	{
		valid = false;
		errorMsg += "Please enter at least $50 for Revolving Credit Facility Minimum Sum Payable.<br />";
	}
	
	
	if(!valid)
	{
		openAlert(errorMsg);	
	}

	return valid;
}

function validateMinSum(className, radioName)
{
	var p = 0;
	var valid = true;
	$("." + className).each(function() {
		var ccAmtType = $("input[name=" + radioName + "_" + p + "]:checked").val();
		if(ccAmtType != undefined && ccAmtType.toLowerCase() == 'cc_minsum')
		{
			var amount = Number($(this).val().replace(/\,/g,""));
			if(amount > 0 && amount < 50)
			{
				valid = false;
			}
		}
		p++;
	});
	return valid;
}

//Round Up or Round down, please key in rlength in 10, 100, 1000 and so on
function roundNumber(rnum, rlength, method) {
	var newnumber = 0;
	if(rlength == 0)
		rlength == 1;
		
	if(method == 'up')
	{
		newnumber = Math.ceil(rnum * rlength)/rlength;
	}
	else if(method == 'down')
	{
		newnumber = Math.floor(rnum * rlength)/rlength;
	}
	else
	{
		newnumber = Math.round(rnum * rlength)/rlength;
	}
    
	return newnumber;
}

function openAlert(txt)
{
	$(".alertDialogContent").html(txt);
	$(".alertDialog").dialog('open');	
}

function openTnc()
{
	$(".tncDialog").dialog('open');	
	$(".ui-dialog").css('top', '+=20px');
}

function scrollToAnchor(anchorKey)
{
	var top = $('body').find("." + anchorKey).offset().top;
	$('html, body').animate({
		scrollTop: top
	}, 1000, 'easeOutExpo');
}

function scrollToObject(obj)
{
	var top = obj.offset().top;
	$('html, body').animate({
		scrollTop: top
	}, 1000, 'easeOutExpo');
}
