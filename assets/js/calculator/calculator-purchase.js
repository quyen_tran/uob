var propertyType,
    numberHouseLoan,
    ltv = 0,
    purchaseMaxPrice,
    estimatedCapitalOutlay,
    downPaymentCash = 0,
    downPaymentCashPercent = 0,
    downPaymentCashCPF = 0,
    downPaymentCashCPFPercent = 0,
    legalFee = 0,
    BSD = 0,
    ABSD = 0,
    ABSDPercent = 0,
    GST = 0,
    GSTPercent = 0,
    monthlyInstalments;

var interestSingapore = [0.015, 0.025, 0.035, 0.045, 0.055];
var interestInternational = [0.035, 0.045, 0.055];

function getGST(){
    if(propertyType == 'commercial'){
        GSTPercent = 0.07;
    }
    GST = purchaseMaxPrice * GSTPercent;
}

function getLTV(){
    if(propertyType == 'private_residential'){
        if(numberHouseLoan == 'first'){
            ltv = 0.8;
        }else if(numberHouseLoan == 'second'){
            ltv = 0.5;
        }else if(numberHouseLoan == 'third'){
            ltv = 0.4;
        }
    }else if(propertyType == 'hdb' || propertyType == 'executive_condominium' || propertyType == 'commercial'){
        ltv = 0.8;
    }else if(propertyType == 'international_property'){
        ltv = 0.7;
    }
}

function getDownPaymentCash(){
    if(propertyType == 'private_residential'){
        if(numberHouseLoan == 'first'){
            downPaymentCashPercent = 0.05;
        }else if(numberHouseLoan == 'second' || numberHouseLoan == 'third'){
            downPaymentCashPercent =  0.25;
        }
    }else if(propertyType == 'hdb' || propertyType == 'executive_condominium'){
        downPaymentCashPercent = 0.05;
    }else if(propertyType == 'international_property'){
        downPaymentCashPercent = 0.3;
    }
    downPaymentCash = purchaseMaxPrice * downPaymentCashPercent;
}

function getDownPaymentCashCPF(){
    if(propertyType == 'private_residential'){
        if(numberHouseLoan == 'first'){
            downPaymentCashCPFPercent = 0.15;
        }else if(numberHouseLoan == 'second'){
            downPaymentCashCPFPercent = 0.25;
        }
        else if(numberHouseLoan == 'third'){
            downPaymentCashCPFPercent = 0.35;
        }
    }else if(propertyType == 'hdb' || propertyType == 'executive_condominium'){
        downPaymentCashCPFPercent = 0.15;
    }
    else if(propertyType == 'commercial'){
        downPaymentCashCPFPercent = 0.20;
    }
    downPaymentCashCPF = purchaseMaxPrice * downPaymentCashCPFPercent;
}

function getBSD(){
    var firstValue = 180000;
    var nextValue = 360000;
    if(purchaseMaxPrice <= firstValue){
        BSD = purchaseMaxPrice * 0.01;
    }else if(purchaseMaxPrice > firstValue && purchaseMaxPrice <= nextValue){
        BSD = (firstValue * 0.01) + (firstValue * 0.02);
    }else if(purchaseMaxPrice > nextValue){
        BSD = (firstValue * 0.01) + (firstValue * 0.02) + ((purchaseMaxPrice - nextValue) * 0.03);
    }
}

function getABSD(){
    var profileBuyer = '';
    var newAbsdPercent = 0;
    for (i = 0; i <= 3; i++){
        profileBuyer = $("#profileBuyer_" + i + ":checked").val();
        if(profileBuyer){
            profileBuyer = profileBuyer.toLowerCase();
            if(profileBuyer == 'foreigner'){
                newAbsdPercent = 0.15;
            }else if(profileBuyer == 'singapore_permanent_resident'){
                if(numberHouseLoan == 'first'){
                    newAbsdPercent = 0.05;
                }else if(numberHouseLoan == 'second' || numberHouseLoan == 'third'){
                    newAbsdPercent = 0.1;
                }

            }else if(profileBuyer == 'singapore_citizen'){
                if(numberHouseLoan == 'first'){
                    newAbsdPercent = 0;
                }else if(numberHouseLoan == 'second'){
                    newAbsdPercent = 0.07;
                }else if(numberHouseLoan == 'third'){
                    newAbsdPercent = 0.1;
                }
            }
            if(newAbsdPercent > ABSDPercent){
                ABSDPercent = newAbsdPercent;
            }
        }
    }
    ABSD = ABSDPercent * purchaseMaxPrice;
}

function getLegalFee(){
    if(propertyType != 'international_property'){
        legalFee = 3000;
    }
}

function getMonthlyInstalments(loanAmount){
    var maxLoanTenor = $(".result_loanTenor").text();
    var interestRate = [];
    if(propertyType != 'international_property'){
        for(i = 0; i < interestSingapore.length; i++){
            interestRate.push(-Formula.PMT(interestSingapore[i] / 12, maxLoanTenor * 12, loanAmount, 0, 0));
        }
    }else {
        for(i = 0; i < interestInternational.length; i++){
            interestRate.push(-Formula.PMT(interestInternational[i] / 12, maxLoanTenor * 12, loanAmount, 0, 0));
        }
    }
    return interestRate;
}

function setValueDisplay(){
    $(".purchase-maximum-price").text(purchaseMaxPrice);
    $(".estimated-capital-outplay").text(estimatedCapitalOutlay);
    $(".downpayment-cash").text(downPaymentCash);
    $(".downpayment-cash-percent").text(downPaymentCashPercent);
    $(".downpayment-cash-cpf").text(downPaymentCashCPF);
    $(".downpayment-cash-cpf-percent").text(downPaymentCashCPFPercent);
    $(".buyer-stamp-duty").text(BSD);
    $(".additional-buyer-stamp-duty").text(ABSD);
    $(".legal-fee").text(legalFee);
}

function calculatorPurchaseMaxPrice(propertyLoanAmount){
    propertyType = $("#propertyType").val().toLowerCase();
    numberHouseLoan = $("#propertyNumberHouseLoan").val().toLowerCase();
    getLTV();
    purchaseMaxPrice = propertyLoanAmount / ltv;

    getDownPaymentCash();
    getDownPaymentCashCPF();
    getBSD();
    getABSD();
    getGST();
    getLegalFee();
    estimatedCapitalOutlay = downPaymentCash + downPaymentCashCPF + BSD + ABSD + legalFee;

    monthlyInstalments = getMonthlyInstalments(propertyLoanAmount);

    setValueDisplay();
    /*console.log(selectedPropertyType);
    console.log(numberHouseLoan);
    console.log(ltv);
    console.log(purchaseMaxPrice);
    console.log(downPaymentCash);
    console.log(downPaymentCashCPF);
    console.log(BSD);
    console.log(ABSD);
    console.log(GST);
    console.log(estimatedCapitalOutlay);
    console.log(monthlyInstalments);*/
}

function reCalculateEstimateCapital(newEstimate){
    estimatedCapitalOutlay = newEstimate;
    //New Maximum Purchase Price for Private Property or HDB at 80% LTV
    if(ltv == 0.8 && propertyType == 'hdb' && propertyType == 'private_residential'){
        if(estimatedCapitalOutlay > (80000 + (ABSDPercent *
            (estimatedCapitalOutlay - legalFee + 5400) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.03) ) )){
            purchaseMaxPrice = (estimatedCapitalOutlay - legalFee + 5400) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.03);

        }else if(estimatedCapitalOutlay > (40800 + (ABSDPercent *
            (estimatedCapitalOutlay - legalFee + 1800) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.02)) ) ){
            purchaseMaxPrice = (estimatedCapitalOutlay - legalFee + 5400) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.03);

        }else{
            purchaseMaxPrice = (estimatedCapitalOutlay - legalFee) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.01);

        }
    }
    //New Maximum Purchase Price for Private Property at 50% LTV
    else if(ltv == 0.5 && propertyType == 'private_residential'){
        if(estimatedCapitalOutlay > (188400 + (ABSDPercent *
            (estimatedCapitalOutlay - legalFee + 5400) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.03) ) )){
            purchaseMaxPrice = (estimatedCapitalOutlay - legalFee + 5400) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.03);

        }else if(estimatedCapitalOutlay > (94800 + (ABSDPercent *
            (estimatedCapitalOutlay - legalFee + 1800) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.02) ) )){
            purchaseMaxPrice = (estimatedCapitalOutlay - legalFee + 1800) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.02);

        }else{
            purchaseMaxPrice = (estimatedCapitalOutlay - legalFee) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.01);
        }
    }
    //New Maximum Purchase Price for Private Property at 40% LTV
    else if(ltv == 0.4 && propertyType == 'private_residential'){
        if(estimatedCapitalOutlay > (224400 + (ABSDPercent *
            (estimatedCapitalOutlay - legalFee + 5400) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.03) ) )){
            purchaseMaxPrice = (estimatedCapitalOutlay - legalFee + 5400) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.03);

        }else if(estimatedCapitalOutlay > (112800 + (ABSDPercent *
            (estimatedCapitalOutlay - legalFee + 1800) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.02) ) )){
            purchaseMaxPrice = (estimatedCapitalOutlay - legalFee + 1800) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.02);

        }else{
            purchaseMaxPrice = (estimatedCapitalOutlay - legalFee) / (downPaymentCashPercent + downPaymentCashCPFPercent + ABSDPercent + 0.01);
        }
    }
    //New Maximum Purchase Price for Commercial Property at 80% LTV
    else if(ltv == 0.8 && propertyType == 'commercial'){
        if(estimatedCapitalOutlay > 105600){
            purchaseMaxPrice = (estimatedCapitalOutlay - legalFee + 5400) / (downPaymentCashPercent + GSTPercent + 0.03);

        }else if(estimatedCapitalOutlay > 53400){
            purchaseMaxPrice = (estimatedCapitalOutlay - legalFee + 1800) / (downPaymentCashPercent + GSTPercent + 0.02);

        }else{
            purchaseMaxPrice = (estimatedCapitalOutlay - legalFee) / (downPaymentCashPercent + GSTPercent + 0.01);
        }
    }
    //International Property at 70% LTV
    else if(ltv == 0.7 && propertyType == 'international_property'){
        purchaseMaxPrice = estimatedCapitalOutlay / downPaymentCashPercent;
    }

    setValueDisplay();
}
