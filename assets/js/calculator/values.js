var currencySymbol = "S$";

//CurrencyArray
var londonArray = [
	["GBP", "British Pound"],
	["SGD", "Singapore Dollar"]
];
var australiaArray = [
	["AUD", "Australian Dollar"],
	["SGD", "Singapore Dollar"]
];
var malaysiaArray = [
	["SGD", "Singapore Dollar"]
];
var thailandArray = [
	["USD", "US Dollar"],
	["SGD", "Singapore Dollar"]
];
var tokyoArray = [
	["JPY", "Japanese Yen"],
	["SGD", "Singapore Dollar"]
];

//Income
var rate_monthlyFixedIncome = 1;
var rate_monthlyVariableIncome = 0.7;
var rate_annualBonus = 0.7;
var month_annualBonus = 12;
var rate_annualTradeIncome = 0.7;
var month_annualTradeIncome = 12;
var rate_monthlyRentalIncome = 0.7;

//Financial Assets
var rate_unitTrusts_unpledged = 0.3;
var month_unitTrusts_unpledged = 48;
var rate_unitTrusts_pledged = 0.7;
var month_unitTrusts_pledged = 48;

var rate_bonds_unpledged = 0.3;
var month_bonds_unpledged = 48;
var rate_bonds_pledged = 0.7;
var month_bonds_pledged = 48;

var rate_foreignCurrenctDep_unpledged = 0.3;
var month_foreignCurrenctDep_unpledged = 48;
var rate_foreignCurrenctDep_pledged = 0.7;
var month_foreignCurrenctDep_pledged = 48;

var rate_strucDep_unpledged = 0.3;
var month_strucDep_unpledged = 48;
var rate_strucDep_pledged = 0.7;
var month_strucDep_pledged = 48;

var rate_fixedDep_unpledged = 0.3;
var month_fixedDep_unpledged = 48;
var rate_fixedDep_pledged = 1;
var month_fixedDep_pledged = 48;

//Debts
var rate_monthlyInstalmentCar = 1; 
var rate_monthlyInstalmentPropLoans = 1;
var rate_monthlyInstalmentPersonalLoans = 1;

var rate_monthlyRepaymentCC_creditLimit = 0.024;
var rate_monthlyRepaymentCC_outstanding = 0.03;
var rate_monthlyRepaymentCC_minSum = 1;

var rate_monthlyRepaymentRevolving_creditLimit = 0.024;
var rate_monthlyRepaymentRevolving_outstanding = 0.03;
var rate_monthlyRepaymentRevolving_minSum = 1;

var rate_guarantorInstalmentAmt_corporate = 0.2;
var rate_guarantorInstalmentAmt_personal = 0.2;


//Interest Rate
var rate_london_sgd = 3.5;
var rate_london_gbp = 3.75;

var rate_australia_sgd = 3.5;
var rate_australia_aud = 5.8;

var rate_tokyo_sgd = 3.5;
var rate_tokyo_jpy = 3.85;

var rate_thailand_sgd = 6;
var rate_thailand_usd = 6;

var rate_malaysia_res_sgd = 3.5;
var rate_malaysia_com_sgd = 4.5;

var rate_ec_sgd = 3.5;
var rate_hdb_sgd = 3.5;
var rate_com_sgd = 4.5;


//Loan Tenor
var tenor_london = 30;
var tenor_australia = 30;
var tenor_tokyo = 30;
var tenor_thailand = 30;
var tenor_malaysia_res = 35;
var tenor_malaysia_com = 30;

var tenor_ec_new = 30;
var tenor_ec_re = 35;
var tenor_hdb_new = 25;
var tenor_hdb_re = 30;
var tenor_com = 30;


//Weighted Avg Age
var weightedAvgAge_london = 75;
var weightedAvgAge_australia = 75;
var weightedAvgAge_tokyo = 75;
var weightedAvgAge_thailand = 70;
var weightedAvgAge_malaysia_res = 70;
var weightedAvgAge_malaysia_com = 70;

var weightedAvgAge_ec_new = 65;
var weightedAvgAge_ec_re = 75;
var weightedAvgAge_hdb_new = 65;
var weightedAvgAge_hdb_re = 75;
var weightedAvgAge_com = 70;