var ajaxSubmitting = false;

$(function() {
	checkPropertyOption();
	$(".loadingMsg").hide();
	
	$('#btnApply').click(function(){
		$('.applyForm').css('display','block');
		$('.chatForm').css('display','none');
		$('.thankyouForm').css('display','none');
	});
	
	$('#btnChat').click(function(){
		$('.applyForm').css('display','none');
		$('.chatForm').css('display','block');
		$('.thankyouForm').css('display','none');
	});
	/*$('#btnRecalculate').click(function(){
		$('.applyForm').css('display','none');
		$('.chatForm').css('display','none');
		$('.thankyouForm').css('display','none');
	});*/
	
	$().dateSelectBoxes({
		dayElement: $('#xxxxxxxxx'),
		monthElement: $('#selectRenovationMonth'),
		yearElement: $('#selectRenovationYear'),
		keepLabels: true,
		generateOptions: true,
		yearMin: (new Date().getFullYear() - 100)
	});
	
	setPlaceHolder('txtContactHome', 'Home');
	setPlaceHolder('txtContactOffice', 'Office');
	setPlaceHolder('txtContactMobile', 'Mobile');
	setPlaceHolder('txtContactHome_Chat', 'Home');
	setPlaceHolder('txtContactOffice_Chat', 'Office');
	setPlaceHolder('txtContactMobile_Chat', 'Mobile');
});

function checkPropertyOption()
{
	var hdbArray = [
		["", "Please Select"],
		["3-Room", "3-Room"],
		["4-Room", "4-Room"],
		["5-Room", "5-Room"],
		["Executive / Maisonette", "Executive / Maisonette"],
		["Others", "Others"]
	];
	var prArray = [
		["", "Please Select"],
		["Bungalow", "Bungalow"],
		["Semi-Detached", "Semi-Detached"],
		["Intermediate / Corner Terrace", "Intermediate / Corner Terrace"],
		["Condominium", "Condominium"],
		["Executive Condominium", "Executive Condominium"],
		["Apartment", "Apartment"],
		["HUDC", "HUDC"],
		["Others", "Others"]
	];
	var commArray = [
		["", "Please Select"],
		["HDB Shop Unit", "HDB Shop Unit"],
		["Office Unit", "Office Unit"],
		["HDB Shophouse", "HDB Shophouse"],
		["Conservation Shophouse", "Conservation Shophouse"],
		["Non-conservation Shophouse", "Non-conservation Shophouse"],
		["Coffeeshop", "Coffeeshop"],
		["Clinic", "Clinic"],
		["Retail Shop", "Retail Shop"]
	];
	
	var selected = $("#selectPropertyType").val().toLowerCase();
	var selectedArray;
	switch(selected)
	{
		case 'hdb': { 
			selectedArray = hdbArray;
		} break;
		case 'private residential': { 
			selectedArray = prArray;
		} break;
		case 'commercial': { 
			selectedArray = commArray;
		} break;
		default: {
			selectedArray = [];
		}
	}	
	
	
	$('#selectPropertyOption').empty();
	for(var i = 0; i < selectedArray.length; i++)
	{
		$('#selectPropertyOption').append($('<option>', {
			value: selectedArray[i][0],
			text: selectedArray[i][1]
		}));
	}
	
	checkPropertyLandArea();
}

function checkPropertyLandArea()
{
	var selected = $("#selectPropertyOption").val();
	var selectedType = $("#selectPropertyType").val();
	if(selected == "Bungalow" || selected == "Intermediate / Corner Terrace" || selected == "Semi-Detached" || selectedType == "Commercial")
	{
		$(".landAreaReveal").show();
	}
	else
	{
		$(".landAreaReveal").hide();
	}
}

function submitApplyForm()
{
	var errorStr = validateApplyForm(false);
	if(errorStr != '')
	{
		openAlert(errorStr);
	}
	else
	{
		//IsValid
		ajaxSubmission('apply');
		s_formComplete('applicationform:loans:applyFormTDSR');
	}
}

function submitChatForm()
{
	var errorStr = validateChatForm(false);
	if(errorStr != '')
	{
		openAlert(errorStr);
	}
	else
	{
		//IsValid	
		ajaxSubmission('chat');
		s_formComplete('contactus:loans:chatFormTDSR');
	}
}

function ajaxSubmission(formType)
{
	if(!ajaxSubmitting)
	{
		ajaxSubmitting = true;
		$(".loadingMsg").show();
		
		//Process selectedYear
		var renovationYear = parseInt($("#selectRenovationYear").val());
		if(renovationYear != 0)
		{
			renovationYear = (new Date().getFullYear()) - renovationYear + 1;
		}
		
		var applicant1Bday = 0;
		if($("#dob_year_0").val() != 0)
		{
			applicant1Bday = $("#dob_day_0").val() + "/" + $("#dob_month_0").val() + "/" + (new Date().getFullYear() - $("#dob_year_0").val() + 1);
		}
		var applicant2Bday = 0;
		if($("#dob_year_1").val() != 0)
		{
			applicant2Bday = $("#dob_day_1").val() + "/" + $("#dob_month_1").val() + "/" + (new Date().getFullYear() - $("#dob_year_1").val() + 1);
		}
		var applicant3Bday = 0;
		if($("#dob_year_2").val() != 0)
		{
			applicant3Bday = $("#dob_day_2").val() + "/" + $("#dob_month_2").val() + "/" + (new Date().getFullYear() - $("#dob_year_2").val() + 1);
		}
		var applicant4Bday = 0;
		if($("#dob_year_3").val() != 0)
		{
			applicant4Bday = $("#dob_day_3").val() + "/" + $("#dob_month_3").val() + "/" + (new Date().getFullYear() - $("#dob_year_3").val() + 1);
		}
		
		//reprocess for IE8
		var fieldval_home = $("#txtContactHome").val(), 
			fieldval_home_chat = $("#txtContactHome_Chat").val(),
			fieldval_office = $("#txtContactOffice").val(), 
			fieldval_office_chat = $("#txtContactOffice_Chat").val(),
			fieldval_mobile = $("#txtContactMobile").val(), 
			fieldval_mobile_chat = $("#txtContactMobile_Chat").val();
		if(fieldval_home == 'Home')
			fieldval_home = '';
		if(fieldval_office == 'Office')
			fieldval_office = '';
		if(fieldval_mobile == 'Mobile')
			fieldval_mobile = '';
		if(fieldval_home_chat == 'Home')
			fieldval_home_chat = '';
		if(fieldval_office_chat == 'Office')
			fieldval_office_chat = '';
		if(fieldval_mobile_chat == 'Mobile')
			fieldval_mobile_chat = '';
		
		var dataArray = {
			formType: formType,
			name: encodeURIComponent($("#txtName").val()),
			nric: $("#txtNRIC").val(),
			contactHome: fieldval_home,
			contactOffice: fieldval_office,
			contactMobile: fieldval_mobile,
			email: encodeURIComponent($("#txtEmail").val()),
			propertyAddress: encodeURIComponent($("#txtPropertyAddress").val()),
			propertyType: $("#selectPropertyType").val(),
			propertyOpt: $("#selectPropertyOption").val(),
			builtInArea: $("#txtPropertyBuiltInArea").val(),
			builtInAreaUnit: $("#selectPropertyBuiltInArea").val(),
			landArea: $("#txtPropertyLandArea").val(),
			landAreaUnit: $("#selectPropertyLandArea").val(),
			isRenovated: $('input[name=radioRenovation]:checked').val(),
			renovationCost: $("#txtRenovationCost").val(),
			renovationMonth: translateMonth($("#selectRenovationMonth").val()),
			renovationYear: renovationYear,
			maxLoanAmount: $(".result_maxLoan").text(),
			loanTenor: $(".result_loanTenor").text(),
			TDSR: $(".result_tdsr").text(),
			MSR: $(".result_msr").text(),
			loanType: $('input[name="loanType"]:checked').val().toLowerCase(),
			loanOption: loanOption,
			requestedLoanAmount: requestedLoanAmount,
			intendedPropertyType: $("#propertyType").val(),
			intendedPropertyCountry: $("#propertyLocation").val(),
			intendedPropertyCurrency: $("#propertyCurrency").val(),
			intendedPropertyTypeMy: $("#propertyTypeInner").val(),
			
			applicant1Bday: applicant1Bday,
			applicant1MonthlyFixedIncome: $("#applicant_0").find(".monthlyFixedIncome").val(),
			applicant1MonthlyVariableIncome: $("#applicant_0").find(".monthlyVariableIncome").val(),
			applicant1AnnualBonus: $("#applicant_0").find(".annualBonus").val(),
			applicant1AnnualTradeIncome: $("#applicant_0").find(".annualTradeIncome").val(),
			applicant1RentalIncome: getRentalInformation(0),
			applicant1UnitTrusts: $("#applicant_0").find(".unitTrusts").val(),
			applicant1UnitTrustsPledged: processBooleanVal($("#unitTrustsCheck_0").is(":checked")),
			applicant1BondsAndStocksAndShares: $("#applicant_0").find(".bonds").val(),
			applicant1BondsAndStocksAndSharesPledged: processBooleanVal($("#bondsCheck_0").is(":checked")),
			applicant1ForeignCurrencyDep: $("#applicant_0").find(".foreignCurrenctDep").val(),
			applicant1ForeignCurrencyDepPledged: processBooleanVal($("#foreignCurrenctDepCheck_0").is(":checked")),
			applicant1StructuredDep: $("#applicant_0").find(".strucDep").val(),
			applicant1StructuredDepPledged: processBooleanVal($("#strucDepCheck_0").is(":checked")),
			applicant1FixedOrSavingsDep: $("#applicant_0").find(".fixedDep").val(),
			applicant1FixedOrSavingsDepPledged:processBooleanVal( $("#fixedDepCheck_0").is(":checked")),
			applicant1TotalCarLoans: $("#applicant_0").find(".monthlyInstalmentCar").val(),
			applicant1TotalPropertyLoans: $("#applicant_0").find(".monthlyInstalmentPropLoans").val(),
			applicant1TotalPersonalRenovationLoans: $("#applicant_0").find(".monthlyInstalmentPersonalLoans").val(),
			applicant1CCAmountType: processCCAmountType($('input[name=ccAmtType_0]:checked').val()),
			applicant1CCRepaymentAmount: $("#applicant_0").find(".monthlyRepaymentCC").val(),
			applicant1RCCAmountType: processCCAmountType($('input[name=rccAmtType_0]:checked').val()),
			applicant1RCCRepaymentAmount: $("#applicant_0").find(".monthlyRepaymentRevolving").val(),
			applicant1IsGuarantor: processGuarantorVal($('input[name=guarantorType_0]:checked').val()),
			applicant1GuarantorInfo: getGuarantorInformation(0)
		};
	
	
		if(formType == 'chat')
		{
			dataArray.name = encodeURIComponent($("#txtName_Chat").val());
			dataArray.nric = $("#txtNRIC_Chat").val();
			dataArray.designation = $("#selectDesignation_Chat").val();
			
			dataArray.contactHome = fieldval_home_chat;
			dataArray.contactOffice = fieldval_office_chat;
			dataArray.contactMobile = fieldval_mobile_chat;
		}
		
		//Process Applicant2-4
		if($("#applicant_1").length > 0)
		{
			dataArray.applicant2Bday = applicant2Bday;
			dataArray.applicant2MonthlyFixedIncome = $("#applicant_1").find(".monthlyFixedIncome").val();
			dataArray.applicant2MonthlyVariableIncome = $("#applicant_1").find(".monthlyVariableIncome").val();
			dataArray.applicant2AnnualBonus = $("#applicant_1").find(".annualBonus").val();
			dataArray.applicant2AnnualTradeIncome = $("#applicant_1").find(".annualTradeIncome").val();
			dataArray.applicant2RentalIncome = getRentalInformation(1);
			dataArray.applicant2UnitTrusts = $("#applicant_1").find(".unitTrusts").val();
			dataArray.applicant2UnitTrustsPledged = processBooleanVal($("#unitTrustsCheck_1").is(":checked"));
			dataArray.applicant2BondsAndStocksAndShares = $("#applicant_1").find(".bonds").val();
			dataArray.applicant2BondsAndStocksAndSharesPledged = processBooleanVal($("#bondsCheck_1").is(":checked"));
			dataArray.applicant2ForeignCurrencyDep = $("#applicant_1").find(".foreignCurrenctDep").val();
			dataArray.applicant2ForeignCurrencyDepPledged = processBooleanVal($("#foreignCurrenctDepCheck_1").is(":checked"));
			dataArray.applicant2StructuredDep = $("#applicant_1").find(".strucDep").val();
			dataArray.applicant2StructuredDepPledged = processBooleanVal($("#strucDepCheck_1").is(":checked"));
			dataArray.applicant2FixedOrSavingsDep = $("#applicant_1").find(".fixedDep").val();
			dataArray.applicant2FixedOrSavingsDepPledged = processBooleanVal($("#fixedDepCheck_1").is(":checked"));
			dataArray.applicant2TotalCarLoans = $("#applicant_1").find(".monthlyInstalmentCar").val();
			dataArray.applicant2TotalPropertyLoans = $("#applicant_1").find(".monthlyInstalmentPropLoans").val();
			dataArray.applicant2TotalPersonalRenovationLoans = $("#applicant_1").find(".monthlyInstalmentPersonalLoans").val();
			dataArray.applicant2CCAmountType = processCCAmountType($('input[name=ccAmtType_1]:checked').val());
			dataArray.applicant2CCRepaymentAmount = $("#applicant_1").find(".monthlyRepaymentCC").val();
			dataArray.applicant2RCCAmountType = processCCAmountType($('input[name=rccAmtType_1]:checked').val());
			dataArray.applicant2RCCRepaymentAmount = $("#applicant_1").find(".monthlyRepaymentRevolving").val();
			dataArray.applicant2IsGuarantor = processGuarantorVal($('input[name=guarantorType_1]:checked').val());
			dataArray.applicant2GuarantorInfo = getGuarantorInformation(1);
		}
		if($("#applicant_2").length > 0)
		{
			dataArray.applicant3Bday = applicant3Bday;
			dataArray.applicant3MonthlyFixedIncome = $("#applicant_2").find(".monthlyFixedIncome").val();
			dataArray.applicant3MonthlyVariableIncome = $("#applicant_2").find(".monthlyVariableIncome").val();
			dataArray.applicant3AnnualBonus = $("#applicant_2").find(".annualBonus").val();
			dataArray.applicant3AnnualTradeIncome = $("#applicant_2").find(".annualTradeIncome").val();
			dataArray.applicant3RentalIncome = getRentalInformation(2);
			dataArray.applicant3UnitTrusts = $("#applicant_2").find(".unitTrusts").val();
			dataArray.applicant3UnitTrustsPledged = processBooleanVal($("#unitTrustsCheck_2").is(":checked"));
			dataArray.applicant3BondsAndStocksAndShares = $("#applicant_2").find(".bonds").val();
			dataArray.applicant3BondsAndStocksAndSharesPledged = processBooleanVal($("#bondsCheck_2").is(":checked"));
			dataArray.applicant3ForeignCurrencyDep = $("#applicant_2").find(".foreignCurrenctDep").val();
			dataArray.applicant3ForeignCurrencyDepPledged = processBooleanVal($("#foreignCurrenctDepCheck_2").is(":checked"));
			dataArray.applicant3StructuredDep = $("#applicant_2").find(".strucDep").val();
			dataArray.applicant3StructuredDepPledged = processBooleanVal($("#strucDepCheck_2").is(":checked"));
			dataArray.applicant3FixedOrSavingsDep = $("#applicant_2").find(".fixedDep").val();
			dataArray.applicant3FixedOrSavingsDepPledged = processBooleanVal($("#fixedDepCheck_2").is(":checked"));
			dataArray.applicant3TotalCarLoans = $("#applicant_2").find(".monthlyInstalmentCar").val();
			dataArray.applicant3TotalPropertyLoans = $("#applicant_2").find(".monthlyInstalmentPropLoans").val();
			dataArray.applicant3TotalPersonalRenovationLoans = $("#applicant_2").find(".monthlyInstalmentPersonalLoans").val();
			dataArray.applicant3CCAmountType = processCCAmountType($('input[name=ccAmtType_2]:checked').val());
			dataArray.applicant3CCRepaymentAmount = $("#applicant_2").find(".monthlyRepaymentCC").val();
			dataArray.applicant3RCCAmountType = processCCAmountType($('input[name=rccAmtType_2]:checked').val());
			dataArray.applicant3RCCRepaymentAmount = $("#applicant_2").find(".monthlyRepaymentRevolving").val();
			dataArray.applicant3IsGuarantor = processGuarantorVal($('input[name=guarantorType_2]:checked').val());
			dataArray.applicant3GuarantorInfo = getGuarantorInformation(2);
			
			
		}
		if($("#applicant_3").length > 0)
		{
			dataArray.applicant4Bday = applicant4Bday;
			dataArray.applicant4MonthlyFixedIncome = $("#applicant_3").find(".monthlyFixedIncome").val();
			dataArray.applicant4MonthlyVariableIncome = $("#applicant_3").find(".monthlyVariableIncome").val();
			dataArray.applicant4AnnualBonus = $("#applicant_3").find(".annualBonus").val();
			dataArray.applicant4AnnualTradeIncome = $("#applicant_3").find(".annualTradeIncome").val();
			dataArray.applicant4RentalIncome = getRentalInformation(3);
			dataArray.applicant4UnitTrusts = $("#applicant_3").find(".unitTrusts").val();
			dataArray.applicant4UnitTrustsPledged = processBooleanVal($("#unitTrustsCheck_3").is(":checked"));
			dataArray.applicant4BondsAndStocksAndShares = $("#applicant_3").find(".bonds").val();
			dataArray.applicant4BondsAndStocksAndSharesPledged = processBooleanVal($("#bondsCheck_3").is(":checked"));
			dataArray.applicant4ForeignCurrencyDep = $("#applicant_3").find(".foreignCurrenctDep").val();
			dataArray.applicant4ForeignCurrencyDepPledged = processBooleanVal($("#foreignCurrenctDepCheck_3").is(":checked"));
			dataArray.applicant4StructuredDep = $("#applicant_3").find(".strucDep").val();
			dataArray.applicant4StructuredDepPledged = processBooleanVal($("#strucDepCheck_3").is(":checked"));
			dataArray.applicant4FixedOrSavingsDep = $("#applicant_3").find(".fixedDep").val();
			dataArray.applicant4FixedOrSavingsDepPledged = processBooleanVal($("#fixedDepCheck_3").is(":checked"));
			dataArray.applicant4TotalCarLoans = $("#applicant_3").find(".monthlyInstalmentCar").val();
			dataArray.applicant4TotalPropertyLoans = $("#applicant_3").find(".monthlyInstalmentPropLoans").val();
			dataArray.applicant4TotalPersonalRenovationLoans = $("#applicant_3").find(".monthlyInstalmentPersonalLoans").val();
			dataArray.applicant4CCAmountType = processCCAmountType($('input[name=ccAmtType_3]:checked').val());
			dataArray.applicant4CCRepaymentAmount = $("#applicant_3").find(".monthlyRepaymentCC").val();
			dataArray.applicant4RCCAmountType = processCCAmountType($('input[name=rccAmtType_3]:checked').val());
			dataArray.applicant4RCCRepaymentAmount = $("#applicant_3").find(".monthlyRepaymentRevolving").val();
			dataArray.applicant4IsGuarantor = processGuarantorVal($('input[name=guarantorType_3]:checked').val());
			dataArray.applicant4GuarantorInfo = getGuarantorInformation(3);
		}
		
		$.ajax({
			url: 'formSubmit.jsp',
			type: "POST",
			data: dataArray,
			success: function(msg){      
				$('.applyForm').css('display','none');
				$('.chatForm').css('display','none');
				$('.thankyouForm').css('display','block');
				
				ajaxSubmitting = false;
				$(".loadingMsg").hide();
			}
		});
	}
}

function getRentalInformation(applicantID)
{
	var rentalArray = new Array();
	var landlordArray = new Array();
	var rentalStr = '';
	
	$("#applicant_" + applicantID).find(".monthlyRentalIncome").each(function() {
	   rentalArray.push($(this).val());
	});
	$("#applicant_" + applicantID).find(".landlordNumber").each(function() {
	   landlordArray.push($(this).val());
	});
	
	for(var i = 0; i < rentalArray.length; i++)
	{
		if(rentalArray[i] == 0)
			continue;
		
		if(landlordArray[i] == 0)
			landlordArray[i] = 1;
			
		rentalStr += 'Rental' + (i + 1) + ':' + currencySymbol + rentalArray[i] + '-owner' + landlordArray[i] + '|';
	}
	
	return rentalStr;
	
}

function getGuarantorInformation(applicantID)
{
	var str = '';
	var x = 0;
	var guarantorOptArray = new Array();
	$("#applicant_" + applicantID).find(".guarantorInstalmentOpt").each(function() {
	    guarantorOptArray.push($(this).val());
	});
	$("#applicant_" + applicantID).find(".guarantorInstalmentAmt").each(function() {
		var guarantorAmt = $(this).val();
		if(guarantorAmt != 0)
		{
			str += 'Guarantor' + (x + 1) + ':' + currencySymbol + $(this).val() + '-' + guarantorOptArray[x] + '|';
		}
		
		x++;
	});
	
	return str;
	
}

function processBooleanVal(boolVal)
{
	if(boolVal)
		return "Yes";
		
	return "No";	
}
function processGuarantorVal(boolVal)
{
	if(boolVal == "guarantor_yes")
		return "Yes";
		
	return "No";	
}
function processCCAmountType(type)
{
	if(type == "cc_creditLimit")
		return "Credit Limit";
	else if(type == "cc_outstanding")
		return "Outstanding Balance";
	else if(type == "cc_minSum")
		return "Minimum Sum Payable";
		
	return "";	
}

function popitup(url,windowName,width,height) {
	newwindow=window.open(url,windowName,'height=' + height + ',width=' + width + ',scrollbars=1,resizable=1');
	if (window.focus) {newwindow.focus()}
	return false;
}

function translateMonth(val)
{
	var txtMonth = '';
	switch (val)
	{
		case '1': txtMonth = 'January'; break;
		case '2': txtMonth = 'February'; break;
		case '3': txtMonth = 'March'; break;
		case '4': txtMonth = 'April'; break;
		case '5': txtMonth = 'May'; break;
		case '6': txtMonth = 'June'; break;
		case '7': txtMonth = 'July'; break;
		case '8': txtMonth = 'August'; break;
		case '9': txtMonth = 'September'; break;
		case '10': txtMonth = 'October'; break;
		case '11': txtMonth = 'November'; break;
		case '12': txtMonth = 'December'; break;
		default: txtMonth = ''; break;
	}
	return txtMonth;
}