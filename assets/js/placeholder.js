function testAttribute(element, attribute)
{
	var test = document.createElement(element);
	if (attribute in test)
		return true;
	else
		return false;
}

function setPlaceHolder(id, text)
{
	if (!testAttribute("input", "placeholder"))
	{
		var demo = document.getElementById(id);
		var text_content = text;
		
		demo.style.color = "#cecece";
		demo.value = text_content;

		demo.onfocus = function() {
		if (this.style.color == "#cecece")
		{ 
			this.value = ""; this.style.color = "#000000" }
		}
		
		demo.onblur = function() {
		if (this.value == "")
		{ 
			this.style.color = "#cecece"; this.value = text_content; }
		}
	}
}