﻿<%@ page import="java.util.*, java.text.SimpleDateFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1, minimum-scale=0.1, maximum-scale=2.0">
	<title>UOB TDSR</title>
	
	<!-- GENERIC START -->
	<link href="assets/css/default.css?v=2" rel="stylesheet">
	<link href="assets/css/calculator.css?v=2" rel="stylesheet">
	<link href="assets/css/results.css?v=2" rel="stylesheet">
	<link href="assets/css/popup.css?v=2" rel="stylesheet">
	<link href="assets/css/tooltipsy.css?v=2" rel="stylesheet">
	<link href="assets/css/icheck/flat.css?v=2" rel="stylesheet">
	<!--[if lt IE 9]>
		<link href="assets/css/ie8overwrite.css?v=2" rel="stylesheet">
	<![endif]-->
	<!--[if gte IE 9]>
	  <style type="text/css">
		.gradient {
		   filter: none;
		}
	  </style>
	<![endif]-->

    <link rel="stylesheet" href="assets/css/uob.css">
    <link rel="stylesheet" href="assets/css/uob/theme.css">
    <link rel="stylesheet" href="assets/css/uob/responsive.css">
    <link rel="stylesheet" href="assets/css/uob/responsive.phone.ie8.css" media="screen and (max-width: 560px)">
    <link rel="stylesheet" href="assets/css/uob/responsive.tablet.ie8.css" media="screen and (min-width: 560px) and (max-width: 768px)">
    <script type="text/javascript" src="assets/javascripts/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/modernizr.custom.62013.js"></script>
    <script type="text/javascript" src="assets/javascripts/respond.src.js"></script>

	<script type="text/javascript" src="assets/js/jquery-1.9.0.js"></script>
	<script type="text/javascript" src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="assets/js/fluidDialog.js"></script>
	<script type="text/javascript" src="assets/js/mdetect.js"></script>
	<script type="text/javascript" src="assets/js/jquery.hoverIntent.minified.js"></script>
	<script type="text/javascript" src="assets/js/icheck.min.js"></script>
	<script type="text/javascript" src="assets/js/tooltipsy.min.js"></script>
	<script type="text/javascript" src="assets/js/placeholder.js"></script>
	<!-- GENERIC END -->
	<!--[if lt IE 8]>
		<script type="text/javascript" src="assets/js/IE8.js?v=2"></script>
	<![endif]-->
	
	<script type="text/javascript" src="assets/js/calculator/underscore.string.min.js"></script>
	<script type="text/javascript" src="assets/js/calculator/numeric.min.js"></script>
	<script type="text/javascript" src="assets/js/calculator/accounting.min.js"></script>
	<script type="text/javascript" src="assets/js/calculator/formula.min.js"></script>
	<script type="text/javascript" src="assets/js/calculator/jquery.dateSelectBoxes.js"></script>
	<script type="text/javascript" src="assets/js/calculator/tooltipvalues.js?v=3"></script>
	<script type="text/javascript" src="assets/js/calculator/values.js?v=3"></script>
	<script type="text/javascript" src="assets/js/calculator/calculator-purchase.js?v=3"></script>
	<script type="text/javascript" src="assets/js/calculator/calculator.js?v=3"></script>

	<script type="text/javascript" src="assets/js/form_validation.js?v=3"></script>
	<script type="text/javascript" src="assets/js/form_submission.js?v=3"></script>
	
	
	
</head>
<body>
	<header>
      <div class="container">
        <figure class="box-img box-real"><img src="assets/images/logo.png" alt="logo UOB">
          <figcaption>UOB Property Loans - Affordability Calculator</figcaption>
        </figure>
      </div>
    </header>
	<div class="alertDialog">
		<div class="alertDialogContent">
			
		</div>
	</div>
	<div class="tncDialog">
      <div class="logo">
				<img src="assets/img/logo.png" />
	  </div>
	  <div class="tncDialogContent">
		<h1>UOB Property Loans - TDSR Calculator Terms and Conditions</h1>
			<ol>
				<li> 
					The computation above is for illustration purposes only and is not an offer of credit/banking facilities from the Bank. 
					<br />Interest rate used to determine the monthly repayment of the Property Loan Amount are as follows:
					<ul>
						<li>SG Private Residential Property Loan / HDB Home Loan: 3.5% per annum</li>
						<li>SG Commercial Property: 4.5% per annum</li>
						<li>London Property in GBP: 3.75% per annum</li>
						<li>London Property in SGD: 3.5% per annum</li>
						<li>Australia Property in AUD: 5.8% per annum</li>
						<li>Australia Property in SGD: 3.5% per annum</li>
						<li>Japan Property in JPY: 3.85% per annum</li>
						<li>Japan Property in SGD: 3.5% per annum</li>
						<li>Malaysia Residential Property: 3.5% per annum</li>
						<li>Malaysia Commercial Property: 4.5% per annum</li>
						<li>Thailand Property: 6% per annum</li>
					</ul>
				  Rates used above are not considered as rate guarantees. For the purpose of computing TDSR / MSR, minimum interest rates of 3.5% (for residential property) and 4.5% (for non-residential property)  per annum will be used. Actual interest rates may vary.
				</li>
				<li> The Property Loan Tenor and Quantum of Financing are subjected to prevailing regulatory guidelines/requirements and the Bank's approval. </li>
				<li>Where the Property Loan is in foreign currency, the Property Loan Amount computed will be in the SGD equivalent.</li>
				<li>The Bank makes no representation or warranty, whether expressed or implied on the completeness or accuracy of the computation. The Bank accepts no liability for any error, inaccuracy or omission or losses/damages however suffered arising from use of or reliance on the computation herein.</li>
			</ol>
            United Overseas Bank Limited Co. Reg. No. 193500026Z.
		</div>
	</div>

	<div class="mWrapper">
		<div class="calculatorPage section">
			<div class="iWrapper headerWrapper">
				<h1 class="txtBlue" align="center">UOB Property Loans - TDSR Calculator</h1>
				<h2 align="center">Calculate your <span class="txtRed">Total Debt Servicing Ratio (TDSR)</span> and <span class="txtRed">MSR</span>*</h2>
				<div class="subtitle" align="center">
					*Mortgage Servicing Ratio (MSR)  is only applicable for HDB flats and Executive Condominium units purchased directly from developer
				</div>
				
			</div>
			<div class="panel panelGradient txtDarkBlue">
				<div class="iWrapper">
					<div class="panelContent">
						<div class="title">
							This loan is for
						</div>
						<div class="radioContainer oHide">
							<div class="radioItem mobileRadioInline">
								<label for="loanType_new"><input type="radio" onclick="GetChar('Loan Type');s_formStart('calculator:loans:tdsr');" id="loanType_new" name="loanType" value="loanType_new"> new purchase</label>
							</div>
							<div class="radioItem mobileRadioInline">
								<label for="loanType_re"><input type="radio" onclick="GetChar('Loan Type');s_formStart('calculator:loans:tdsr');" id="loanType_re" name="loanType" value="loanType_re" > refinancing</label>
							</div>
						</div>
					</div>
					<div class="panelContent">
						<div class="title">
							I want to find out
						</div>
						<div class="radioContainer">
							<div class="radioItem">
								<label for="loanOption_withoutAmt"><input type="radio" onclick="GetChar('Loan Option')" id="loanOption_withoutAmt" name="loanOption" value="loanOption_withoutAmt"> how much I can borrow and the maximum loan tenor</label>
							</div>
							<div class="radioItem">
								<label for="loanOption_withAmt"><input type="radio" onclick="GetChar('Loan Option')" id="loanOption_withAmt" name="loanOption" value="loanOption_withAmt"> my TDSR & MSR based on loan amount S$</label> <input type="text"  onkeydown="GetChar('Loan Option')" id="propertyLoanAmt" name="propertyLoanAmt" class="textBox" data-type="numerals" format="currency" />
							</div>
						</div>
					</div>
					<div class="panelContent last">
						<div class="title">
							Select  property type
						</div>
						<div class="twoColumnsSystem padBottom">
							<div class="column">
								<div class="innerWrap">
									<div class="selectText fLeft">
										Property Type
									</div>
									<div class="selectItem fLeft">
										<select onchange="GetChar('Property Type');" id="propertyType" name="propertyType" class="selectBox">
											<option value="Private_Residential">Private Residential (Singapore)</option>
											<option value="HDB">HDB Flat</option>
											<option value="Executive_Condominium">Executive Condominium</option>
											<option value="Commercial">Commercial Property (Singapore)</option>
											<option value="International_Property">International Property</option>
										</select>
									</div>
								</div>
							</div>
							<div class="column fRight propertyLocationContainer">
								<div class="innerWrap">
									<div class="selectText fLeft">
										Country
									</div>
									<div class="selectItem fLeft">
										<select onchange="GetChar('Country')" id="propertyLocation" name="propertyLocation" class="selectBox">
											<option value="London">London</option>
											<option value="Australia">Australia</option>
											<option value="Malaysia">Malaysia</option>
											<option value="Thailand">Thailand</option>
											<option value="Tokyo">Japan</option>
										</select>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="propertyCurrencyContainer hidden">
							<div class="subtitle">
								For international property loan, please also indicate the country or city you intend to purchase the property and the desired currency denomination of your loan. Where the Property Loan is in foreign currency, the Property Loan Amount computed will be in the SGD equivalent.
							</div>
							<div class="twoColumnsSystem">
								<div class="column fLeft">
									<div class="innerWrap oHide">
										<div class="selectText fLeft">
											Currency
										</div>
										<div class="selectItem fLeft">
											<select onchange="GetChar('Currency');" id="propertyCurrency" name="propertyCurrency" class="selectBox">
												<option value="GBP">British Pound</option>
												<option value="AUD">Australian Dollar</option>
												<option value="SGD">Singapore Dollar</option>
												<option value="USD">US Dollar</option>
												<option value="JYP">Japanese Yen</option>
											</select>
										</div>
									</div>
								</div>
								<div class="column fRight propertyTypeInnerContainer hidden">
									<div class="innerWrap">
										<div class="selectText fLeft">
											Property Type
										</div>
										<div class="selectItem fLeft">
											<select onchange="GetChar('Property Type')" id="propertyTypeInner" name="propertyTypeInner" class="selectBox">
												<option value="Residential">Residential</option>
												<option value="Commercial">Commercial</option>
											</select>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
                        <div class="propertyNumberHouseLoanContainer hidden">
                            <div class="column fLeft">
                                <div class="innerWrap oHide">
                                    <div class="selectText fLeft">
                                        This is / will be my
                                        <select onchange="GetChar('Number House Loan')" id="propertyNumberHouseLoan" name="propertyNumberHouseLoan" class="selectBox">
                                            <option value="first">First</option>
                                            <option value="second">Second</option>
                                            <option value="third">Third or subsequent</option>
                                        </select>
                                        outstanding Housing Loan
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
						</div>
                        <div class="propertyResidentContainer hidden">
                            <div class="column fLeft">
                                <div class="innerWrap oHide">
                                    <div class="selectText fLeft">
                                        This is my
                                        <select onchange="GetChar('Resident')" id="propertyResident" name="propertyResident" class="selectBox">
                                            <option value="first">First</option>
                                            <option value="second">Second</option>
                                            <option value="third">Third or subsequent</option>
                                        </select>
                                        residential property in Singapore
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="infoPanel rel">
				<div class="iWrapper">
                <div class="sectionTitle"><strong>Important Notes:</strong></div>
					<div align="justify">The following income, asset and debt details are to be entered in SGD or SGD equivalent. This calculator is to be used for illustration purposes only and the computation obtained is not an offer of credit/banking facilities from the Bank. Computation is based on the information entered and the MAS Notice on Computation of TDSR for Property Loans. All loan applications shall be subjected to Bank's approval and prevailing regulatory guidelines/requirements. </div></div>
			</div>
			
			<div class="applicationPanel rel">
				<div class="applicantTabContainer rel oHide">
					<div class="applicantTabContent oHide fLeft">
						<div class="applicantTab active fLeft" data-applicant="applicant_0" onclick="selectApplicant($(this));">
							<span class="big">Applicant 1</span>
							<span class="small"><span class="addText">Applicant</span> 1</span>
						</div>
					</div>
					<div class="applicantTab addApplicantControl fLeft" onclick="addApplicant();">
						<span class="big">[+] Add Applicant</span>
						<span class="small">[+]</span>
					</div>
				</div>
			
				<div class="panel overlapTop rel">
					<div class="iWrapper">
						<div class="applicantOrigin" id="applicant_0">
							<div class="subPanel expand" data-expand="1">
								<div class="panelHeader rel" onclick="expandPanel($(this));">
									<div class="panelTitle">
										<div class="bigHeader">1 &nbsp; APPLICANT'S INFORMATION</div>
										<div class="smallHeader"></div>
									</div>
								</div>
								<div class="panelReveal">
									<div class="panelContent dob first last">
										<div class="dobText">Date of Birth</div>
										<div class="selectContainer">
											<select onchange="GetChar('DOB')" id="dob_day_0" name="dob_day[]" class="selectBox"> 
											</select> 
											<select onchange="GetChar('DOB')" id="dob_month_0" name="dob_month[]" class="selectBox">
											</select> 
											<select onchange="GetChar('DOB')" id="dob_year_0" name="dob_year[]" class="selectBox"> 
											</select> 
										</div>
										<div class="dobText">(Required Field)</div>
									</div>
								</div>
								<div class="panelReveal">
									<div class="panelContent dob first last">
										<div class="dobText">National: </div>
										<input class="radio" type="radio" name="profileBuyer_0" id="profileBuyer_0" value="singapore_permanent_resident" checked="checked"/>
										<span>Singapore Permanent Resident</span>
										<input class="radio" type="radio" name="profileBuyer_0" id="profileBuyer_0" value="singapore_citizen"/>
										<span>Singapore Citizen</span>
                                        <input class="radio" type="radio" name="profileBuyer_0" id="profileBuyer_0" value="foreigner" />
                                        <span>Foreigner</span>
									</div>
								</div>
							</div>
							
							<div class="subPanel expand" data-expand="1">
								<div class="panelHeader rel" onclick="expandPanel($(this));">
									<div class="panelTitle">
										<div class="bigHeader">2 &nbsp; INCOME</div>
										<div class="smallHeader">Please provide the following as reflected on your latest income documents such as Notice of Assessment and payslip.
		Where there is rental income, tenancy agreement/s should have a remaining lease of at least 6 months.</div>
									</div>
								</div>
								<div class="panelReveal">
									<div class="panelContent first">
										<div class="sectionTitle">Fixed Income</div>
										<div class="formInputRow last">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Monthly Fixed Income
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"><img class="hastip tooltipMonthlyFixedIncome" title="XXXXX" src="assets/img/icon_question.png" /></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Monthly Fixed Income')" data-type="numerals" format="currency" class="textBox monthlyFixedIncome" name="monthlyFixedIncome[]" />
													</div>
												</div>	
											</div>
										</div>
									</div>
									
									<div class="panelContent">
										<div class="sectionTitle">Variable Income</div>
										<div class="formInputRow">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Monthly Variable Income
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"><img class="hastip tooltipMonthlyVariableIncome" title="XXXX" src="assets/img/icon_question.png" /></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Monthly Variable Income')" data-type="numerals" format="currency" class="textBox monthlyVariableIncome" name="monthlyVariableIncome[]" />
													</div>
												</div>
											</div>
										</div>
										<div class="formInputRow">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Annual Bonus
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"><img class="hastip tooltipAnnualBonus" title="XXXXX" src="assets/img/icon_question.png" /></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Annual Bonus')" data-type="numerals" format="currency" class="textBox annualBonus" name="annualBonus[]" />
													</div>
												</div>
											</div>
										</div>
										<div class="formInputRow last">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Annual Trade Income
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"><img class="hastip tooltipAnnualTradeIncome" title="XXXXX" src="assets/img/icon_question.png" /></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Annual Trade Income')" data-type="numerals" format="currency" class="textBox annualTradeIncome" name="annualTradeIncome[]" />
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="panelContent last rental">
										<div class="sectionTitle">Rental Income</div>
										<div class="cloneContainer">
											<div class="cloneOrigin oHide">
												<div class="formInputRow fLeft">
													<div class="formWrap oHide">
														<div class="formLabel fLeft">
															<div class="innerWrap">
																<span class="bold rentalNum">Property 1</span><br /> Monthly Rental Income
															</div>
														</div>
														<div class="formIcon fLeft">
															<div class="innerWrap"><img class="hastip tooltipMonthlyRentalIncome" title="XXXXXXXX " src="assets/img/icon_question.png" /></div>
														</div>
														<div class="formCurr fLeft">
																<div class="innerWrap"><span class="currSymbol">S$</span></div>
													  </div>
														<div class="formInput fLeft">
															<div class="innerWrap">
																<input type="text"  onkeydown="GetChar('Rental Income')" data-type="numerals" format="currency" class="textBox monthlyRentalIncome" name="monthlyRentalIncome[]" />
															</div>
														</div>
													</div>
												</div>
												<div class="formInputRow extraCol fRight last">
													<div class="formWrap oHide">
														<div class="formLabel fLeft">
															<div class="innerWrap">
																No. of landlords
															</div>
														</div>
														<div class="formIcon fLeft">
															<div class="innerWrap"><img class="hastipLeft tooltipNumberOfLandlords" title="XXXXX " src="assets/img/icon_question.png" /></div>
														</div>
														<div class="formInput fLeft">
															<div class="innerWrap">
																 <input type="text"  onkeydown="GetChar('No. of landlords')" data-type="numerals" value="1" class="textBox landlordNumber" name="landlordNumber[]" />
															</div>
														</div>
													</div>
												</div>
											</div>
											
											<div class="cloneContent">
											</div>
											<div class="cloneButton" onclick="addRental($(this));">
												<img src="assets/img/icon_plus.png" /> Add Another Rental Property
											</div>
											
										</div>
										
									</div>
									
								</div>
							</div>
							
							
							<div class="subPanel expand" data-expand="1">
								<div class="panelHeader rel" onclick="expandPanel($(this));">
									<div class="panelTitle">
										<div class="bigHeader">3 &nbsp; FINANCIAL ASSETS (OPTIONAL)</div>
										<div class="smallHeader">If you have any of the following financial assets, it can be included as part of your income stream. 
		Recognition of the following financial assets for income computation shall be subjected to the Bank's approval.</div>
									</div>
								</div>
								<div class="panelReveal">
									<div class="sectionTitleStickTop rel oHide">
										<div class="fLeft titleItem">Latest value of:</div>
										<div class="fRight infoItem">Can asset be pledged with UOB for at least 4 years? <!--<img class="hastip" title="XXX" src="assets/img/icon_question.png" />--></div>
									</div>
									
									<div class="panelContent first last">
										
										<div class="assetsRow oHide">
											<div class="formInputRow fLeft">
												<div class="formWrap oHide">
													<div class="formLabel fLeft">
														<div class="innerWrap">
															Unit Trusts
														</div>
													</div>
													<div class="formIcon fLeft">
														<div class="innerWrap"></div>
													</div>
													<div class="formCurr fLeft">
														<div class="innerWrap"><span class="currSymbol">S$</span></div>
													</div>
													<div class="formInput fLeft">
														<div class="innerWrap">
															<input type="text"  onkeydown="GetChar('Unit Trusts')" data-type="numerals" format="currency" class="textBox unitTrusts" name="unitTrusts[]" />
														</div>
													</div>	
												</div>
											</div>
											<div class="formInputRow fRight">
												<div class="checkboxContainer">
													<div class="checkboxItem">
														<input type="checkbox" id="unitTrustsCheck_0" class="unitTrustsCheck icheck" name="unitTrustsCheck[]">
													</div>
													<div class="checkboxLabel">
														<label for="unitTrustsCheck_0"> Yes</label>
													</div>
												</div>
											</div>
										</div>
										
										<div class="assetsRow oHide">
											<div class="formInputRow fLeft">
												<div class="formWrap oHide">
													<div class="formLabel fLeft">
														<div class="innerWrap">
															Bonds, Stocks & Shares
														</div>
													</div>
													<div class="formIcon fLeft">
														<div class="innerWrap"></div>
													</div>
													<div class="formCurr fLeft">
														<div class="innerWrap"><span class="currSymbol">S$</span></div>
													</div>
													<div class="formInput fLeft">
														<div class="innerWrap">
															<input type="text"  onkeydown="GetChar('Bonds, Stocks & Shares')" data-type="numerals" format="currency" class="textBox bonds" name="bonds[]" />
														</div>
													</div>	
												</div>
											</div>
											<div class="formInputRow fRight">
												<div class="checkboxContainer">
													<div class="checkboxItem">
														<input type="checkbox" id="bondsCheck_0" class="bondsCheck icheck" name="bondsCheck[]">
													</div>
													<div class="checkboxLabel">
														<label for="bondsCheck_0"> Yes</label>
													</div>
												</div>
											</div>
										</div>
										
										<div class="assetsRow oHide">
											<div class="formInputRow fLeft">
												<div class="formWrap oHide">
													<div class="formLabel fLeft">
														<div class="innerWrap">
															Foreign Currency Deposits
														</div>
													</div>
													<div class="formIcon fLeft">
														<div class="innerWrap"></div>
													</div>
													<div class="formCurr fLeft">
														<div class="innerWrap"><span class="currSymbol">S$</span></div>
													</div>
													<div class="formInput fLeft">
														<div class="innerWrap">
															<input type="text"  onkeydown="GetChar('Foreign Currency Deposits')" data-type="numerals" format="currency" class="textBox foreignCurrenctDep" name="foreignCurrenctDep[]" />
														</div>
													</div>	
												</div>
											</div>
											<div class="formInputRow fRight">
												<div class="checkboxContainer">
													<div class="checkboxItem">
														<input type="checkbox" id="foreignCurrenctDepCheck_0" class="foreignCurrenctDepCheck icheck" name="foreignCurrenctDepCheck[]">
													</div>
													<div class="checkboxLabel">
														<label for="foreignCurrenctDepCheck_0"> Yes</label>
													</div>
												</div>
											</div>
										</div>
										
										<div class="assetsRow oHide">
											<div class="formInputRow fLeft">
												<div class="formWrap oHide">
													<div class="formLabel fLeft">
														<div class="innerWrap">
															Structured Deposits
														</div>
													</div>
													<div class="formIcon fLeft">
														<div class="innerWrap"></div>
													</div>
													<div class="formCurr fLeft">
														<div class="innerWrap"><span class="currSymbol">S$</span></div>
													</div>
													<div class="formInput fLeft">
														<div class="innerWrap">
															<input type="text"  onkeydown="GetChar('Structured Deposits')" data-type="numerals" format="currency" class="textBox strucDep" name="strucDep[]" />
														</div>
													</div>	
												</div>
											</div>
											<div class="formInputRow fRight">
												<div class="checkboxContainer">
													<div class="checkboxItem">
														<input type="checkbox" id="strucDepCheck_0" class="strucDepCheck icheck" name="strucDepCheck[]">
													</div>
													<div class="checkboxLabel">
														<label for="strucDepCheck_0"> Yes</label>
													</div>
												</div>
											</div>
										</div>
										
										<div class="assetsRow oHide">
											<div class="formInputRow last fLeft">
												<div class="formWrap oHide">
													<div class="formLabel fLeft">
														<div class="innerWrap">
															Fixed / Savings Deposits
															In Singapore Dollar
														</div>
													</div>
													<div class="formIcon fLeft">
														<div class="innerWrap"></div>
													</div>
													<div class="formCurr fLeft">
														<div class="innerWrap"><span class="currSymbol">S$</span></div>
													</div>
													<div class="formInput fLeft">
														<div class="innerWrap">
															<input type="text"  onkeydown="GetChar('Fixed / Savings Deposits')" data-type="numerals" format="currency" class="textBox fixedDep" name="fixedDep[]" />
														</div>
													</div>	
												</div>
											</div>
											<div class="formInputRow last fRight">
												<div class="checkboxContainer">
													<div class="checkboxItem">
														<input type="checkbox" id="fixedDepCheck_0" class="fixedDepCheck icheck" name="fixedDepCheck[]">
													</div>
													<div class="checkboxLabel">
														<label for="fixedDepCheck_0"> Yes</label>
													</div>
												</div>
											</div>
										</div>
										
										
									</div>
								</div>
							</div>
							
							
							
							<div class="subPanel expand" data-expand="1">
								<div class="panelHeader rel" onclick="expandPanel($(this));">
									<div class="panelTitle">
										<div class="bigHeader">4 &nbsp; DEBT OBLIGATIONS</div>
										<div class="smallHeader">Please provide the following as reflected on your latest available statements. </div>
									</div>
								</div>
								<div class="panelReveal">
									<div class="panelContent first">
										<div class="formInputRow fullWidth">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Total Monthly Instalment for all car loans
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Total Monthly Instalment')" data-type="numerals" format="currency" class="textBox monthlyInstalmentCar" name="monthlyInstalmentCar[]" />
													</div>
												</div>	
											</div>
										</div>
										<div class="formInputRow fullWidth">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Total Monthly Instalment for all other property loans
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Total Monthly Instalment')" data-type="numerals" format="currency" class="textBox monthlyInstalmentPropLoans" name="monthlyInstalmentPropLoans[]" />
													</div>
												</div>	
											</div>
										</div>
										<div class="formInputRow last fullWidth">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Total Monthly Instalment for all other personal / renovation loans
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Total Monthly Instalment')" data-type="numerals" format="currency" class="textBox monthlyInstalmentPersonalLoans" name="monthlyInstalmentPersonalLoans[]" />
													</div>
												</div>	
											</div>
										</div>
									</div>
									<div class="panelContent">
										<div class="normalTitle">For credit cards, please indicate which amount you will be providing :</div>
										<div class="radioInlineContainer oHide">
											<div class="radioItem fLeft">
												<label for="cc_creditLimit_0"><input type="radio" onclick="GetChar('Credit Limit')" class="radio cc_creditLimit" id="cc_creditLimit_0" name="ccAmtType_0" value="cc_creditLimit"> Credit Limit</label> <img class="hastip tooltipCreditLimit" title="XXXXX" src="assets/img/icon_question.png" />
											</div>
											<div class="radioItem fLeft">
												<label for="cc_outstanding_0"><input type="radio" onclick="GetChar('Outstanding Balance')" class="radio cc_outstanding" id="cc_outstanding_0" name="ccAmtType_0" value="cc_outstanding"> Outstanding Balance</label> <img class="hastip tooltipOutstandingBalance" title="XXXXXXXXXXXXX" src="assets/img/icon_question.png" />
											</div>
											<div class="radioItem fLeft">
												<label for="cc_minSum_0"><input type="radio" onclick="GetChar('Minimum Sum Payable')" class="radio cc_minSum" id="cc_minSum_0" name="ccAmtType_0" value="cc_minSum"> Minimum Sum Payable</label> <img class="hastip tooltipMinimumSumPayable" title="XXXXXXX " src="assets/img/icon_question.png" />
											</div>
										</div>
										<div class="formInputRow last fullWidth">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Total Amount for all credit cards
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Total Amount')" data-type="numerals" format="currency" class="textBox monthlyRepaymentCC" name="monthlyRepaymentCC[]" />
													</div>
												</div>	
											</div>
										</div>
									</div>
									<div class="panelContent last">
										<div class="normalTitle">For unsecured non-card credit facility (e.g. cash plus, credit line), please indicate the amount you will be providing:</div>
										<div class="radioInlineContainer oHide">
											<div class="radioItem fLeft">
												<label for="rcc_creditLimit_0"><input type="radio" onclick="GetChar('Credit Limit')" class="radio rcc_creditLimit" id="rcc_creditLimit_0" name="rccAmtType_0" value="cc_creditLimit"> Credit Limit</label> <img class="hastip tooltipCreditLimitRev" title="XXXXXX " src="assets/img/icon_question.png" />
											</div>
											<div class="radioItem fLeft">
												<label for="rcc_outstanding_0"><input type="radio" onclick="GetChar('Outstanding Balance')" class="radio rcc_outstanding" id="rcc_outstanding_0" name="rccAmtType_0" value="cc_outstanding"> Outstanding Balance</label> <img class="hastip tooltipOutstandingBalanceRev" title="XXXXXX " src="assets/img/icon_question.png" />
											</div>
											<div class="radioItem fLeft">
												<label for="rcc_minSum_0"><input type="radio" onclick="GetChar('Minimum Sum Payable')" class="radio rcc_minSum" id="rcc_minSum_0" name="rccAmtType_0" value="cc_minSum"> Minimum Sum Payable</label> <img class="hastip tooltipMinimumSumPayableRev" title="XXXXXX " src="assets/img/icon_question.png" />
											</div>
										</div>
										<div class="formInputRow last fullWidth">
											<div class="formWrap oHide">
												<div class="formLabel fLeft">
													<div class="innerWrap">
														Total Amount for all unsecured non-card credit facilities
													</div>
												</div>
												<div class="formIcon fLeft">
													<div class="innerWrap"></div>
												</div>
												<div class="formCurr fLeft">
													<div class="innerWrap"><span class="currSymbol">S$</span></div>
												</div>
												<div class="formInput fLeft">
													<div class="innerWrap">
														<input type="text"  onkeydown="GetChar('Total Amount')" data-type="numerals" format="currency" class="textBox monthlyRepaymentRevolving" name="monthlyRepaymentRevolving[]" />
													</div>
												</div>	
											</div>
										</div>
									</div>
									
								</div>
							</div>
							
							
							<div class="subPanel expand" data-expand="1">
								<div class="panelHeader rel" onclick="expandPanel($(this));">
									<div class="panelTitle">
										<div class="bigHeader">5 &nbsp; GUARANTOR STATUS</div>
										<div class="smallHeader"></div>
									</div>
								</div>
								<div class="panelReveal guarantor">
									<div class="panelContent first last">
										<div class="normalTitle">Are you a Guarantor for any loans? </div>
										<div class="radioInlineContainer halfWidth oHide">
											<div class="radioItem fLeft">
												<label for="guarantor_yes_0">
													<input type="radio" class="radio guarantor_yes" id="guarantor_yes_0" name="guarantorType_0" value="guarantor_yes" onclick="checkGuarantorOption($(this));GetChar('Guarantor');">
													 Yes
												</label>
											</div>
											<div class="radioItem fLeft">
												<label for="guarantor_no_0">
													<input type="radio" class="radio guarantor_no" id="guarantor_no_0" name="guarantorType_0" value="guarantor_no" onclick="checkGuarantorOption($(this));GetChar('Guarantor');">
													 No
												</label>
											</div>
										</div>
										
										<div class="cloneContainer">
											<div class="cloneOrigin oHide">
												<div class="formInputRow fLeft">
													<div class="formWrap oHide">
														<div class="formInput fLeft">
															<div class="innerWrap">
																<select onchange="GetChar('Guarantor Instalment')" class="guarantorInstalmentOpt selectBox" name="guarantorInstalmentOpt[]">
																	<option value="corporate">Corporate Loan</option>
																	<option value="personal">Personal Loan</option>
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="formInputRow extraCol fRight last">
													<div class="formWrap oHide">
														<div class="formLabel fLeft">
															<div class="innerWrap">
																Monthly Instalment
															</div>
														</div>
														<div class="formIcon fLeft">
															<div class="innerWrap"></div>
														</div>
														<div class="formCurr fLeft">
															<div class="innerWrap"><span class="currSymbol">S$</span></div>
														</div>
														<div class="formInput fLeft">
															<div class="innerWrap">
																 <input type="text"  onkeydown="GetChar('Monthly Instalment')" data-type="numerals" format="currency" class="textBox guarantorInstalmentAmt" name="guarantorInstalmentAmt[]" />
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="cloneContent">
											</div>
											<div class="cloneButton" onclick="addGuarantor($(this));">
												<img src="assets/img/icon_plus.png" /> Add more loans
											</div>
											
										</div>
									</div>
										
										
								</div>
							</div>
							
							<div class="buttonContainer oHide">
								<!--<div class="button greyButton fRight" onclick="removeApplicant($(this));">
									Remove Applicant
								</div>-->
								<div class="button greyButton fRight" onclick="addApplicant();">
									Add Applicant
								</div>
							</div>
						</div>
						<div class="applicantClone">
						
						</div>
						
					</div>
				</div>
			</div>
			
			<div class="buttonContainer maxWidth oHide">
				<div class="button fLeft redButton" onclick="startCalculate();GetChar('Submit');">
					Calculate
				</div>
				<div class="button fLeft blueButton" onclick="resetCalculator();">
					Reset
				</div>
			</div>
			<div align="center" >United Overseas Bank Limited Co. Reg. No. 193500026Z.</div>
		</div>
		
		
		<div class="resultsPage">
			<section class="section section-home-i-borrow theme-light">
		      <div class="container text-center">
		        <div class="paragraph">
		          <div class="title">
		            <hr class="hr-sort red">
		            <h3>How much can I borrow? </h3>
		          </div>
		          <p>The indicative results below are based on your inputs</p>
		        </div>
		        <div class="list list-4 list-md-2 list-xs-1 text-center">
		              <div class="property">
		                <div class="wrapper">
		                  <div class="content">
		                    <div class="box-img box-real"><img src="assets/images/home.png" alt="property loan amount"></div>
		                    <div class="caption text-uppercase">PROPERTY LOAN AMOUNT</div>
		                    <hr class="dark">
		                    <div class="price price-sky medium"><span class="result_maxLoan">0</span></div>
		                  </div>
		                </div>
		              </div>
		              <div class="property">
		                <div class="wrapper">
		                  <div class="content">
		                    <div class="box-img box-real"><img src="assets/images/price.png" alt="property loan amount"></div>
		                    <div class="caption text-uppercase">PROPERTY LOAN TENOR</div>
		                    <hr class="dark">
		                    <div class="price price-sky medium"><span class="result_loanTenor">0</span> Years</div>
		                  </div>
		                </div>
		              </div>
		              <div class="property">
		                <div class="wrapper">
		                  <div class="content">
		                    <div class="box-img box-real"><img src="assets/images/market.png" alt="property loan amount"></div>
		                    <div class="caption text-uppercase">TOTAL DEPT SERVICING RATIO</div>
		                    <hr class="dark">
		                    <div class="price price-sky medium"><span class="result_tdsr">0</span></div>
		                  </div>
		                </div>
		              </div>
		              <div class="property">
		                <div class="wrapper">
		                  <div class="content">
		                    <div class="box-img box-real"><img src="assets/images/home.png" alt="property loan amount"></div>
		                    <div class="caption text-uppercase">TOTAL MORTGAGE SERVICING RATIO</div>
		                    <hr class="dark">
		                    <div class="price price-sky medium"><span class="result_msr">0</span></div>
		                  </div>
		                </div>
		              </div>
		              <div class="property">
						<div class="tdsrmsrResults">
							
						</div>
					</div>
		        </div>
		      </div>
		    </section>
		    <section class="section section-home-purchase-price theme-dark">
		      <div class="container text-center">
		        <div class="paragraph">
		          <div class="title">
		            <hr class="red">
		            <h3>What is the property purchase price I can afford?</h3>
		          </div>
		          <p>Maximum purchase price and estimated monthly instalment (reflected in SGD/SGD equivalent)</p>
		        </div>
		        <div class="list list-2 list-afford">
		              <div class="property">
		                <div class="wrapper">
		                  <div class="content">
		                    <div class="caption text-uppercase">MAXIMUM PURCHASE PRICE</div>
		                    <hr class="dark">
		                    <div class="price price-sky medium"><span class="purchase-maximum-price">0</span></div>
		                  </div>
		                </div>
		              </div>
		              <div class="property">
		                <div class="wrapper">
		                  <div class="content">
		                    <div class="caption text-uppercase">ESTIMATED CAPITAL OUTPLAY</div>
		                    <hr class="dark">
		                    <div class="price price-sky medium"><span class="estimated-capital-outplay">0</span></div>
		                  </div>
		                </div>
		              </div>
		        </div>
		        <div class="instalment">
		          <h4>Monthly instalment over a loan tenor of <span class="result_loanTenor">0</span> years</h4>
		          <div class="table table-vertical">
		            <div class="title-col">
		              <div class="wrapper">
		                <div class="list list-1 list-md-1">
		                  <div class="property">
		                    <p>If Interest rate is</p>
		                    <p class="text-sky">Your Monthly Instalment is</p>
		                  </div>
		                </div>
		              </div>
		            </div>
		            <div class="content-col">
		              <div class="wrapper">
		                <div class="list list-5 list-md-1 list-xs-1">
                            <div class="property">
                              <p>1.5%p.a.</p>
                              <p>$3,000</p>
                            </div>
                            <div class="property">
                              <p>1.5%p.a.</p>
                              <p>$3,000</p>
                            </div>
                            <div class="property">
                              <p>1.5%p.a.</p>
                              <p>$3,000</p>
                            </div>
                            <div class="property">
                              <p>1.5%p.a.</p>
                              <p>$3,000</p>
                            </div>
                            <div class="property">
                              <p>1.5%p.a.</p>
                              <p>$3,000</p>
                            </div>
		                </div>
		              </div>
		            </div>
		          </div>
		        </div>
		        <div class="break-down"> 
		          <h4>Capital Outlay Breakdown</h4>
		          <div class="list list-5 list-md-2 list-xs-1 list-break-down">
		                <div class="property">
		                  <div class="wrapper">
		                    <div class="content">
		                      <div class="caption text-uppercase">DOWN PAYMENT CASH @ <span class="downpayment-cash-percent">0</span></div>
		                      <hr class="dark">
		                      <div class="price price-sky medium"><span class="downpayment-cash">0</span></div>
		                    </div>
		                  </div>
		                </div>
		                <div class="property">
		                  <div class="wrapper">
		                    <div class="content">
		                      <div class="caption text-uppercase">DOWN PAYMENT CASH/CPF @ <span class="downpayment-cash-cpf-percent">0</span></div>
		                      <hr class="dark">
		                      <div class="price price-sky medium"><span class="downpayment-cash-cpf">0</span></div>
		                    </div>
		                  </div>
		                </div>
		                <div class="property">
		                  <div class="wrapper">
		                    <div class="content">
		                      <div class="caption text-uppercase">BUYER'S STAMP DUTY</div>
		                      <hr class="dark">
		                      <div class="price price-sky medium"><span class="buyer-stamp-duty">0</span></div>
		                    </div>
		                  </div>
		                </div>
		                <div class="property">
		                  <div class="wrapper">
		                    <div class="content">
		                      <div class="caption text-uppercase">ADDITIONAL BUYER'S STAMP DUTY</div>
		                      <hr class="dark">
		                      <div class="price price-sky medium"><span class="additional-buyer-stamp-duty">0</span></div>
		                    </div>
		                  </div>
		                </div>
		                <div class="property">
		                  <div class="wrapper">
		                    <div class="content">
		                      <div class="caption text-uppercase">LEGAL FEES</div>
		                      <hr class="dark">
		                      <div class="price price-sky medium"><span class="legal-fee">0</span></div>
		                    </div>
		                  </div>
		                </div>
		          </div>
		          <button class="btn btn-second text-center">REDUCE CAPITAL OUTLAY</button>
		        </div>
		      </div>
		    </section>

		    <section class="section section-home-like-to-proceed">
		      <div class="container text-center">
		        <p>The computation above is for illustration purposes only and is not an offer of credit/banking facilities from the Bank. </p>
		        <p class="text-red"><a href="#" onclick="openTnc(); return false;" title="terms &amp; conditions">Click here to view full Terms &amp; Conditions.</a>
		          
		        </p>
		        <figure class="box-img"><img src="assets/images/banking.png" alt="banking"></figure>
		        <h3>How would you like to proceed?</h3>
		        <div class="optionBox list list-3 list-xs-1 list-proceed" align="center">
		        	<div class="property" onclick="openPanel('applyForm'); scrollToAnchor('form'); s_formStart('applicationform:loans:applyFormTDSR');">
                      <div class="wrapper">
                        <figure class="box-img box-real"><img src="assets/images/edit.png" alt="apply now">
                          <figcaption class="caption text-uppercase">I AM READY TO APPLY </figcaption>
                        </figure>
                        <button class="btn btn-block btn-primary">APPLY NOW</button>
                      </div>
                    </div>
                    <div class="property" onclick="openPanel('chatForm'); scrollToAnchor('form');s_formStart('contactus:loans:chatFormTDSR');">
                      <div class="wrapper">
                        <figure class="box-img box-real"><img src="assets/images/comment.png" alt="apply now">
                          <figcaption class="caption text-uppercase">I NEED MORE INFORMATION </figcaption>
                        </figure>
                        <button class="btn btn-block btn-primary">LET'S CHAT</button>
                      </div>
                    </div>
                    <div class="property" onclick="backToCalculator();">
                      <div class="wrapper">
                        <figure class="box-img box-real"><img src="assets/images/return.png" alt="apply now">
                          <figcaption class="caption text-uppercase">RETURN TO TDSR CALCULATOR</figcaption>
                        </figure>
                        <button class="btn btn-block btn-primary">RECALCULATE</button>
                      </div>
                    </div>
				</div>
		      </div>
		    </section>

			<!-- <div align="center" class="table">
				<div class="table-inner">
					<div class="table-top">
						Indicative Results
					</div>
					<div class="table-row">
						<div class="tableCell tableLeft tableBlue"><span class="txtLoanAmount">Property Loan Amount</span></div>
						<div class="tableCell tableRight tableBlue"><span class="result_maxLoan">0</span></div>
					</div>
					<div class="table-row">
						<div class="tableCell tableLeft">Property Loan Tenor</div>
						<div class="tableCell tableRight"><span class="result_loanTenor">0</span> years</div>
					</div>
					<div class="table-row">
						<div class="tableCell tableLeft tableBlue">TDSR</div>
						<div class="tableCell tableRight tableBlue"><span class="result_tdsr">0</span></div>
					</div>
					<div class="table-row">
						<div class="tableCell table-bottomLeft">MSR</div>
						<div class="tableCell table-bottomRight"><span class="result_msr">0</span></div>
					</div>
					<div align="center" class="tablePrints">
						<div class="tdsrmsrResults">
							
						</div>
					</div>
					<div align="center" class="tablePrints">
						The computation above is for illustration purposes only and is not an offer of credit/banking facilities from the Bank.
						<br/><br/>
						<a href="#" onclick="openTnc(); return false;"><span class="txtRed">Click here to view full Terms &amp; Conditions.</span></a>
					</div>
				</div>
			</div> -->
            <!-- <div align="center" class="tablePrints">
                <div>Maximum Purchase Price: <span class="purchase-maximum-price"></span></div>
                <div>Estimated Capital Outplay: <span class="estimated-capital-outplay"></span></div>
                <div>Down Payment Cash: <span class="downpayment-cash"></span></div>
                <div>Down Payment Cash / CPF: <span class="downpayment-cash-cpf"></span></div>
                <div>Buyer's Stamp Duty: <span class="buyer-stamp-duty"></span></div>
                <div>Additional Buyer's Stamp Duty: <span class="additional-buyer-stamp-duty"></span></div>
                <div>Legal Fees: <span class="legal-fee"></span></div>
            </div> -->
            <!-- <div align="center" class="tablePrints">
            	<img src="assets/img/TDSR_ResultsPage_banner.jpg"/>
            </div> -->
            <!-- <div align="center" class="tablePrints">
            *Strictly for new property loans only (excluding gear up term loan on existing property). No third-party referrals (such as agent/IAF/welcome back/other third-party referral) or other benefits accorded. Other terms and conditions apply.<br /><br />
            <a href="assets/pdfs/TDSRGiftTC4MAY15.pdf" target="_blank"><span class="txtRed">Other Terms and Conditions apply.</span></a>
            </div> -->
			
			<!-- <div class="optionBox" align="center">
				<div class="optionTitle">How would you like to proceed?</div>
				<div class="optionCol borderR" onclick="openPanel('applyForm'); scrollToAnchor('form'); s_formStart('applicationform:loans:applyFormTDSR');">
					<div class="optionImg"><img src="assets/img/apply_now.png"/></div>
					<div class="optionTxt">I AM READY TO APPLY</div>
					<div class="btnRed" id="btnApply">APPLY NOW</div>
				</div>
				<div class="optionCol borderR" onclick="openPanel('chatForm'); scrollToAnchor('form');s_formStart('contactus:loans:chatFormTDSR');">
					<div class="optionImg"><img src="assets/img/contact_us.png"/></div>
					<div class="optionTxt">I NEED MORE INFORMATION</div>
					<div class="btnRed" id="btnChat">LET'S CHAT</div>
				</div>
				<div class="optionCol"  onclick="backToCalculator();">
					<div class="optionImg"><img src="assets/img/return.png"/></div>
					<div class="optionTxt">RETURN TO TDSR CALCULATOR</div>
					<div class="btnRed" id="btnRecalculate">RECALCULATE</div>
				</div>
			</div>  -->
			
			<div class="form"><a name="form"></a>
				<div class="formContent applyForm">
					<div class="txtRed formTitle">Apply Now</div>
					(For new loan applications only. If you wish to enquire about your existing loan with us, please click <a href="https://uniservices1.uobgroup.com/secure/forms/loans/online_assistance.html"  onclick="s_popup('contactus:loans:online_assistance')" target="_blank">here</a>)
					<br/><br/>
					
					<div class="formLabel">Name (as in NRIC/Passport)</div>
					<div class="formInput"><input id="txtName" name="txtName" type="text"  onkeydown="GetChar('Name')" class="inputTxt" /></div>
					
					<div class="formLabel">NRIC / Passport no</div>
					<div class="formInput"><input id="txtNRIC" name="txtNRIC" type="text"  onkeydown="GetChar('NRIC/Passport')" class="inputTxt" /></div>
					
					<div class="formLabel contactNo">
					Contact Number<br/>
					(please provide at least one contact number)
					</div>
					<div class="formInput">
						<input id="txtContactHome" name="txtContactHome" type="text"  onkeydown="GetChar('Contact Number')" class="inputTxt" placeholder="Home"/><br/>
						<input id="txtContactOffice" name="txtContactOffice" type="text"  onkeydown="GetChar('Contact Number')" class="inputTxt" placeholder="Office"/><br/> 
						<input id="txtContactMobile" name="txtContactMobile" type="text"  onkeydown="GetChar('Contact Number')" class="inputTxt" placeholder="Mobile"/> 
					</div>
					
					<div class="formLabel">Email Address</div>
					<div class="formInput"><input id="txtEmail" name="txtEmail" type="text"  onkeydown="GetChar('Email Address')" class="inputTxt"/></div>
					
					<br/>
					<div style="padding:5px;">Include request for an indicative property valuation:</div>
					
					<div class="formLabel">Property Address</div>
					<div class="formInput"><input id="txtPropertyAddress" name="txtPropertyAddress" type="text"  onkeydown="GetChar('Property Address')" class="inputTxt"/></div>
					
					<div class="formLabel">Property Type</div>
					<div class="formInput">
						<div class="styled-select">
							<select id="selectPropertyType" onchange="checkPropertyOption();GetChar('Property Type');" name="selectPropertyType">
							   <option value="">Please Select</option>
							   <option value="HDB">HDB</option>
							   <option value="Private Residential">Private Residential</option>
                               <option value="Commercial">Commercial</option>
							</select>
						</div>
					</div>
					
					<div class="formLabel ">Type</div>
					<div class="formInput">
						<div class="styled-select">
							<select  id="selectPropertyOption" onchange="checkPropertyLandArea();GetChar('Type');" name="selectPropertyOption">
							   
							</select>
						</div>
					</div>
					
					<div class="formLabel">Built in Area</div>
					<div class="formInput">
						<input type="text"  onkeydown="GetChar('Built in Area')" id="txtPropertyBuiltInArea" data-type="numerals" name="txtPropertyBuiltInArea" class="inputShortTxt"/>
						<div class="styled-select-short">
							<select onchange="GetChar('Built in Area')" id="selectPropertyBuiltInArea" name="selectPropertyBuiltInArea">
							    <option value="">Please Select</option>
							    <option value="Square Feet">Square Feet</option>
							    <option value="Square Meter">Square Meter</option>
							</select>
						</div>
					</div>
					
					<div class="formLabel landAreaReveal">Land Area</div>
					<div class="formInput landAreaReveal">
						<input type="text"  onkeydown="GetChar('Land Area')" id="txtPropertyLandArea" data-type="numerals" name="txtPropertyLandArea" class="inputShortTxt"/>
						<div class="styled-select-short">
							<select onchange="GetChar('Land Area')" id="selectPropertyLandArea" name="selectPropertyLandArea">
								<option value="">Please Select</option>
							    <option value="Square Feet">Square Feet</option>
							    <option value="Square Meter">Square Meter</option>
							</select>
						</div>
					</div>
					
					<div class="formLabel">Any renovation?</div>
					<div class="formInput">
						<label class="radio-inline">
						  <input type="radio" onclick="GetChar('Any renovation')" name="radioRenovation" value="Yes"></input>
						  Yes
						</label>
						<label class="radio-inline">
						  <input type="radio" onclick="GetChar('Any renovation')" name="radioRenovation" value="No"></input>
						  No
						</label>
						<br/>
						<br/>
					</div>
					
					<div class="formLabel">Renovation Cost S$</div>
					<div class="formInput"><input type="text"  onkeydown="GetChar('Renovation Cost')" data-type="numerals" format="currency" id="txtRenovationCost" name="txtRenovationCost" class="inputTxt"/></div>
					
					<div class="formLabel">When was it renovated?</div>
					<div class="formInput">
						<div class="styled-select-short" style="float:left; margin-right:10px;">
							<select onchange="GetChar('When was it renovated')" id="selectRenovationMonth" name="selectRenovationMonth">
							</select>
						</div>
						<div class="styled-select-short">
							<select onchange="GetChar('When was it renovated')" id="selectRenovationYear" name="selectRenovationYear">
							</select>
						</div>
					</div>
					
					<div class="tnc_txt">
						<b>Declaration and Authorisation</b>
						<div>&nbsp;</div>
						<div align="justify">By clicking on 'Submit' below,<br />
I/We confirm that I/we have read and understood the Bank&rsquo;s Privacy Notice  (Individual) (available at <a href="http://www.uob.com.sg/">www.uob.com.sg</a> and  the Bank&rsquo;s branches) which forms part of the terms and conditions governing  my/our relationship with the Bank. I/We consent to the Bank collecting, using  and disclosing my/our personal data for Basic Banking Purposes, Co-Branding  Purpose, Research Purpose and Marketing Purpose as described in the Bank&rsquo;s  Privacy Notice (Individual). I/We note that (a) I/we may withdraw consent for  any or all of the purposes at any time; (b) if I/we withdraw consent for Basic  Banking Purposes and/or Co-branding Purpose, the Bank may not be able to  continue to provide the products and services to me/us; (c) if I/we withdraw  consent for Research Purpose and Marketing Purpose, my/our personal data will  not be used for these purposes unless I/we expressly and separately consent to  the same again. </div>                        </div>
					
					<div class="submit_txt">
                    <table width="690" border="0">
  <tr>
    <td width="20"><input type="checkbox" align="top" id="chkTNC" name="chkTNC" class="submit_check icheck" /></td>
    <td width="967">When you click on “Submit” below, all the data provided to compute your TDSR will be sent to the Bank. <br />Our personal banker will contact you by telephone or email within the next working day about the UOB <br />Property Loan application.</td>
  </tr>
</table>&nbsp;</div>
					
					<div class="buttonContainer smallWidth oHide">
						<div class="button redButton" onclick="submitApplyForm();GetChar('Submit');">
							SUBMIT
						</div>
					</div>
					<div align="center" >United Overseas Bank Limited Co. Reg. No. 193500026Z.</div>
					<div class="loadingMsg">
						Your form is being submitted, please wait.
					</div>
				</div>
				
				
				<div class="formContent chatForm">
					<div class="txtRed formTitle">Let's Chat</div>
					(For new loan applications only. If you wish to enquire about your existing loan with us, please click <a href="https://uniservices1.uobgroup.com/secure/forms/loans/online_assistance.html" onclick="s_popup('contactus:loans:online_assistance')" target="_blank">here</a>)
					<br/><br/>
					
					
					<div class="contact_mobile">
						<a id="contactPhoneNumber" href="tel:000" onclick="s_popup('contactus:loans:contact_mobile_tdsr');"><img src="assets/img/btn_callup.png"/></a><br/><br/>
						Or leave us your contact and our bankers will get in touch.
					</div>
					
					<div class="formLabelContact">Name</div>
					<div class="formInput" style="margin-bottom:-15px;">
						<div class="styled-select-status fLeft" style="margin-bottom:0px;">
							<select onchange="GetChar('Name')" id="selectDesignation_Chat" class="designation">
							   <option value="Mr">Mr</option>
							   <option value="Mrs">Mrs</option>
							   <option value="Ms">Ms</option>
							   <option value="Mdm">Mdm</option>
							   <option value="Dr">Dr</option>
							</select>
						</div>&nbsp;&nbsp;
						<input type="text"  onkeydown="GetChar('Name')" id="txtName_Chat" name="txtName_Chat" class="inputTxt inputContactName"/>
					</div><br/>
					
					<div class="formLabelContact">NRIC</div>
					<div class="formInput"><input id="txtNRIC_Chat" name="txtNRIC_Chat" type="text"  onkeydown="GetChar('NRIC')" class="inputTxt inputTxtContact"/></div>
					
					<div class="formLabelContact contactNo2">
					Contact Number
					</div>
					<div class="formInput">
						<input id="txtContactHome_Chat" name="txtContactHome_Chat" type="text"  onkeydown="GetChar('Contact Number')" class="inputTxt inputTxtContact" placeholder="Home"/><br/>
						<input id="txtContactOffice_Chat" name="txtContactOffice_Chat" type="text"  onkeydown="GetChar('Contact Number')" class="inputTxt inputTxtContact" placeholder="Office"/><br/> 
						<input id="txtContactMobile_Chat" name="txtContactMobile_Chat" type="text"  onkeydown="GetChar('Contact Number')" class="inputTxt inputTxtContact" placeholder="Mobile"/><br/>
						
					</div>
					
					
					<div class="tnc_txt">
                    <b>Declaration and Authorisation</b>
                    <div>&nbsp;</div>
						<div align="justify">By clicking on 'Submit' below,<br />
						  I/We confirm that I/we have read and understood the Bank’s Privacy Notice (Individual) (available at <a href="http://www.uob.com.sg/">www.uob.com.sg</a> and the Bank’s branches) which forms part of the terms and conditions governing my/our relationship with the Bank. I/We consent to the Bank collecting, using and disclosing my/our personal data for Basic Banking Purposes, Co-Branding Purpose, Research Purpose and Marketing Purpose as described in the Bank’s Privacy Notice (Individual). I/We note that (a) I/we may withdraw consent for any or all of the purposes at any time; (b) if I/we withdraw consent for Basic Banking Purposes and/or Co-branding Purpose, the Bank may not be able to continue to provide the products and services to me/us; (c) if I/we withdraw consent for Research Purpose and Marketing Purpose, my/our personal data will not be used for these purposes unless I/we expressly and separately consent to the same again.<br /></div>
						</div>
					<div>&nbsp;</div>
					<div class="submit_txt">
                    <table width="718" border="0">
  <tr>
    <td width="20" align="justify"><input type="checkbox" id="chkTNC_Chat" name="chkTNC_Chat" class="submit_check icheck" /></td>
    <td width="671" align="justify"><label for="chkTNC_Chat">When you click on “Submit” below, all the data provided to compute your TDSR will be sent to the Bank. <br />Our personal banker will contact you by telephone or email within the next working day about UOB <br />Property Loans.</label></td>
  </tr>
</table>&nbsp;</div>
					
					<div class="buttonContainer smallWidth oHide">
						<div class="button redButton" onclick="submitChatForm();GetChar('Submit');">
							SUBMIT
						</div>
					</div>
					<div align="center" >United Overseas Bank Limited Co. Reg. No. 193500026Z.</div>
					<div class="loadingMsg">
						Your form is being submitted, please wait.
					</div>
				
				</div>
				
				
				
				<div class="formContent thankyouForm">
					<div class="txtRed formTitle">Thank you</div>
					Thank you for your interest in UOB Property Loans. We have received your submission.
					<br />Our Banker will be in touch with you within the next working day. 
				</div>
				
				
			</div>
		
		</div>
		
		
	</div>
	
	<script type="text/javascript">
	window.onbeforeunload = function(e) {
       return abandon();
      };
		function abandon(){
		if(s_formTrackField!="Submit"){	
		s_formTrackSendLink(s_formTrackField);
		}
		}
		
		$(function() {
			<%
				//String string1 = "10:00:00";
				//Date time1 = new SimpleDateFormat("HH:mm:ss").parse(string1);
				Calendar calendar1 = Calendar.getInstance();
				calendar1.set(Calendar.HOUR_OF_DAY, 10);
				calendar1.set(Calendar.MINUTE, 0);
				calendar1.set(Calendar.SECOND, 0);
				
				//String string2 = "18:00:00";
				//Date time2 = new SimpleDateFormat("HH:mm:ss").parse(string2);
				Calendar calendar2 = Calendar.getInstance();
				calendar2.set(Calendar.HOUR_OF_DAY, 18);
				calendar2.set(Calendar.MINUTE, 0);
				calendar2.set(Calendar.SECOND, 0);
				
				Calendar cal  = Calendar.getInstance();
				int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
				
				Date timeNow = cal.getTime();
				if (timeNow.before(calendar1.getTime()) || timeNow.after(calendar2.getTime()) || dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
			%>	
				//Outside
				var phoneNumber = "18003882121"; //Call centre number
			<%
				}
				else
				{
			%>	
				var phoneNumberArray = ["92208719", "81861373", "92208719", "81861373"];
				var randomNumber = Math.floor((Math.random()*phoneNumberArray.length));
				var phoneNumber = phoneNumberArray[randomNumber];
			<%	
				}
			%>
			
			$("#contactPhoneNumber").attr("href", "tel:" + phoneNumber);
			//alert(phoneNumber);
			
			
		});
		function GetChar (field){
s_formTrackField=field;
//alert(s_formTrackField);
}
	</script>
<script type="text/javascript" src="assets/js/analytics.js"></script>	
<!--<script type="text/javascript" src="https://uniservices1.uobgroup.com/secure/assets/js/analytics.js"></script>-->

<!-- Google Code for Remarketing Tag -->
<!-- ------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
-------------------------------------------------- -->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 989083474;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/989083474/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</body>
</html>