<%@ page import="java.io.*,java.util.*,javax.mail.*"%>
<%@ page import="javax.mail.internet.*,javax.activation.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>


<%
	//Configuration
	String applyFilePath = "/prodlib/WSM/forms/generic/TDSR_ApplyNow_datafile.txt";
	String chatFilePath = "/prodlib/WSM/forms/generic/TDSR_LetsChat_datafile.txt";
	//Email Config
	//String[] to = {"SecuredLoans@UOBgroup.com","Kong.LeeSan@UOBgroup.com"};
	//String from = "UOB_WEB_SERVER@uobgroup.com";
	//String host = "172.28.4.71";
	//String smtpPort = "25";
	//String user = "SMTPUser";
	//String pass = "PASSWORD";

	String[] to = {"quyen.tran@opendigital.asia"};
	String from = "quyen.tran@opendigital.asia";
	String host = "smtp.gmail.com";
	String smtpPort = "465";
	String user = "quyen.tran@opendigital.asia";
	String pass = "quyen187205";

    //Personal Particulars
	String formType = request.getParameter("formType");
	String name = java.net.URLDecoder.decode(request.getParameter("name"));
	String nric = processPostValue(request.getParameter("nric"), false);
	String designation = processPostValue(request.getParameter("designation"), false);
	String contactHome = processPostValue(request.getParameter("contactHome"), false); 
	String contactOffice = processPostValue(request.getParameter("contactOffice"), false);
	String contactMobile = processPostValue(request.getParameter("contactMobile"), false);
	String email = (request.getParameter("email") == null)? "" : java.net.URLDecoder.decode(request.getParameter("email"));
	String propertyAddress =  (request.getParameter("propertyAddress") == null)? "" : java.net.URLDecoder.decode(request.getParameter("propertyAddress"));
	String propertyType = processPostValue(request.getParameter("propertyType"), false);   
	String propertyOpt = processPostValue(request.getParameter("propertyOpt"), false);
	String builtInArea = processPostValue(request.getParameter("builtInArea"), false);      
	String builtInAreaUnit = (request.getParameter("builtInAreaUnit") == null || builtInArea.equals(""))? "" : request.getParameter("builtInAreaUnit");
	String landArea = processPostValue(request.getParameter("landArea"), false);
	String landAreaUnit = (request.getParameter("landAreaUnit") == null || landArea.equals(""))? "" : request.getParameter("landAreaUnit");
	String isRenovated = (request.getParameter("isRenovated") == null)? "" : request.getParameter("isRenovated");
	String renovationCost = processPostValue(request.getParameter("renovationCost"), true);
	String renovationMonth = processPostValue(request.getParameter("renovationMonth"), false); 
	String renovationYear = processPostValue(request.getParameter("renovationYear"), false);  
	
	//Calculator Results
	String maxLoanAmount =  processPostValue(request.getParameter("maxLoanAmount"), false);
	String loanTenor = processPostValue(request.getParameter("loanTenor"), false);
	String TDSR = processPostValue(request.getParameter("TDSR"), false);
	String MSR = processPostValue(request.getParameter("MSR"), false);
	String loanType = processPostValue(request.getParameter("loanType"), false);
	String loanOption = processPostValue(request.getParameter("loanOption"), false);
	String requestedLoanAmount = processPostValue(request.getParameter("requestedLoanAmount"), true);
	String intendedPropertyType = processPostValue(request.getParameter("intendedPropertyType"), false);
	String intendedPropertyCountry = processPostValue(request.getParameter("intendedPropertyCountry"), false);
	String intendedPropertyCurrency = processPostValue(request.getParameter("intendedPropertyCurrency"), false);
	String intendedPropertyTypeMy = processPostValue(request.getParameter("intendedPropertyTypeMy"), false);
	
	//Applicant 1
	String applicant1Bday = processPostValue(request.getParameter("applicant1Bday"), false);
	String applicant1MonthlyFixedIncome = processPostValue(request.getParameter("applicant1MonthlyFixedIncome"), true);
	String applicant1MonthlyVariableIncome = processPostValue(request.getParameter("applicant1MonthlyVariableIncome"), true);
	String applicant1AnnualBonus = processPostValue(request.getParameter("applicant1AnnualBonus"), true);
	String applicant1AnnualTradeIncome = processPostValue(request.getParameter("applicant1AnnualTradeIncome"), true);
	String applicant1RentalIncome = processPostValue(request.getParameter("applicant1RentalIncome"), false);
	String applicant1UnitTrusts = processPostValue(request.getParameter("applicant1UnitTrusts"), true);
	String applicant1UnitTrustsPledged = processPostValue(request.getParameter("applicant1UnitTrustsPledged"), false);
	String applicant1BondsAndStocksAndShares = processPostValue(request.getParameter("applicant1BondsAndStocksAndShares"), true);
	String applicant1BondsAndStocksAndSharesPledged = processPostValue(request.getParameter("applicant1BondsAndStocksAndSharesPledged"), false);
	String applicant1ForeignCurrencyDep = processPostValue(request.getParameter("applicant1ForeignCurrencyDep"), true);
	String applicant1ForeignCurrencyDepPledged = processPostValue(request.getParameter("applicant1ForeignCurrencyDepPledged"), false);
	String applicant1StructuredDep = processPostValue(request.getParameter("applicant1StructuredDep"), true);
	String applicant1StructuredDepPledged = processPostValue(request.getParameter("applicant1StructuredDepPledged"), false);
	String applicant1FixedOrSavingsDep = processPostValue(request.getParameter("applicant1FixedOrSavingsDep"), true);
	String applicant1FixedOrSavingsDepPledged = processPostValue(request.getParameter("applicant1FixedOrSavingsDepPledged"), false);
	String applicant1TotalCarLoans = processPostValue(request.getParameter("applicant1TotalCarLoans"), true);
	String applicant1TotalPropertyLoans = processPostValue(request.getParameter("applicant1TotalPropertyLoans"), true);
	String applicant1TotalPersonalRenovationLoans = processPostValue(request.getParameter("applicant1TotalPersonalRenovationLoans"), true);
	String applicant1CCAmountType = processPostValue(request.getParameter("applicant1CCAmountType"), false);
	String applicant1CCRepaymentAmount = processPostValue(request.getParameter("applicant1CCRepaymentAmount"), true);
	String applicant1RCCAmountType = processPostValue(request.getParameter("applicant1RCCAmountType"), false);
	String applicant1RCCRepaymentAmount = processPostValue(request.getParameter("applicant1RCCRepaymentAmount"), true);
	String applicant1IsGuarantor = processPostValue(request.getParameter("applicant1IsGuarantor"), false);
	String applicant1GuarantorInfo = processPostValue(request.getParameter("applicant1GuarantorInfo"), false);
	
	//Applicant 2
	String applicant2Bday = processPostValue(request.getParameter("applicant2Bday"), false);
	String applicant2MonthlyFixedIncome = processPostValue(request.getParameter("applicant2MonthlyFixedIncome"), true);
	String applicant2MonthlyVariableIncome = processPostValue(request.getParameter("applicant2MonthlyVariableIncome"), true);
	String applicant2AnnualBonus = processPostValue(request.getParameter("applicant2AnnualBonus"), true);
	String applicant2AnnualTradeIncome = processPostValue(request.getParameter("applicant2AnnualTradeIncome"), true);
	String applicant2RentalIncome = processPostValue(request.getParameter("applicant2RentalIncome"), false);
	String applicant2UnitTrusts = processPostValue(request.getParameter("applicant2UnitTrusts"), true);
	String applicant2UnitTrustsPledged = processPostValue(request.getParameter("applicant2UnitTrustsPledged"), false);
	String applicant2BondsAndStocksAndShares = processPostValue(request.getParameter("applicant2BondsAndStocksAndShares"), true);
	String applicant2BondsAndStocksAndSharesPledged = processPostValue(request.getParameter("applicant2BondsAndStocksAndSharesPledged"), false);
	String applicant2ForeignCurrencyDep = processPostValue(request.getParameter("applicant2ForeignCurrencyDep"), true);
	String applicant2ForeignCurrencyDepPledged = processPostValue(request.getParameter("applicant2ForeignCurrencyDepPledged"), false);
	String applicant2StructuredDep = processPostValue(request.getParameter("applicant2StructuredDep"), true);
	String applicant2StructuredDepPledged = processPostValue(request.getParameter("applicant2StructuredDepPledged"), false);
	String applicant2FixedOrSavingsDep = processPostValue(request.getParameter("applicant2FixedOrSavingsDep"), true);
	String applicant2FixedOrSavingsDepPledged = processPostValue(request.getParameter("applicant2FixedOrSavingsDepPledged"), false);
	String applicant2TotalCarLoans = processPostValue(request.getParameter("applicant2TotalCarLoans"), true);
	String applicant2TotalPropertyLoans = processPostValue(request.getParameter("applicant2TotalPropertyLoans"), true);
	String applicant2TotalPersonalRenovationLoans = processPostValue(request.getParameter("applicant2TotalPersonalRenovationLoans"), true);
	String applicant2CCAmountType = processPostValue(request.getParameter("applicant2CCAmountType"), false);
	String applicant2CCRepaymentAmount = processPostValue(request.getParameter("applicant2CCRepaymentAmount"), true);
	String applicant2RCCAmountType = processPostValue(request.getParameter("applicant2RCCAmountType"), false);
	String applicant2RCCRepaymentAmount = processPostValue(request.getParameter("applicant2RCCRepaymentAmount"), true);
	String applicant2IsGuarantor = processPostValue(request.getParameter("applicant2IsGuarantor"), false);
	String applicant2GuarantorInfo = processPostValue(request.getParameter("applicant2GuarantorInfo"), false);
	
	//Applicant 3
	String applicant3Bday = processPostValue(request.getParameter("applicant3Bday"), false);
	String applicant3MonthlyFixedIncome = processPostValue(request.getParameter("applicant3MonthlyFixedIncome"), true);
	String applicant3MonthlyVariableIncome = processPostValue(request.getParameter("applicant3MonthlyVariableIncome"), true);
	String applicant3AnnualBonus = processPostValue(request.getParameter("applicant3AnnualBonus"), true);
	String applicant3AnnualTradeIncome = processPostValue(request.getParameter("applicant3AnnualTradeIncome"), true);
	String applicant3RentalIncome = processPostValue(request.getParameter("applicant3RentalIncome"), false);
	String applicant3UnitTrusts = processPostValue(request.getParameter("applicant3UnitTrusts"), true);
	String applicant3UnitTrustsPledged = processPostValue(request.getParameter("applicant3UnitTrustsPledged"), false);
	String applicant3BondsAndStocksAndShares = processPostValue(request.getParameter("applicant3BondsAndStocksAndShares"), true);
	String applicant3BondsAndStocksAndSharesPledged = processPostValue(request.getParameter("applicant3BondsAndStocksAndSharesPledged"), false);
	String applicant3ForeignCurrencyDep = processPostValue(request.getParameter("applicant3ForeignCurrencyDep"), true);
	String applicant3ForeignCurrencyDepPledged = processPostValue(request.getParameter("applicant3ForeignCurrencyDepPledged"), false);
	String applicant3StructuredDep = processPostValue(request.getParameter("applicant3StructuredDep"), true);
	String applicant3StructuredDepPledged = processPostValue(request.getParameter("applicant3StructuredDepPledged"), false);
	String applicant3FixedOrSavingsDep = processPostValue(request.getParameter("applicant3FixedOrSavingsDep"), true);
	String applicant3FixedOrSavingsDepPledged = processPostValue(request.getParameter("applicant3FixedOrSavingsDepPledged"), false);
	String applicant3TotalCarLoans = processPostValue(request.getParameter("applicant3TotalCarLoans"), true);
	String applicant3TotalPropertyLoans = processPostValue(request.getParameter("applicant3TotalPropertyLoans"), true);
	String applicant3TotalPersonalRenovationLoans = processPostValue(request.getParameter("applicant3TotalPersonalRenovationLoans"), true);
	String applicant3CCAmountType = processPostValue(request.getParameter("applicant3CCAmountType"), false);
	String applicant3CCRepaymentAmount = processPostValue(request.getParameter("applicant3CCRepaymentAmount"), true);
	String applicant3RCCAmountType = processPostValue(request.getParameter("applicant3RCCAmountType"), false);
	String applicant3RCCRepaymentAmount = processPostValue(request.getParameter("applicant3RCCRepaymentAmount"), true);
	String applicant3IsGuarantor = processPostValue(request.getParameter("applicant3IsGuarantor"), false);
	String applicant3GuarantorInfo = processPostValue(request.getParameter("applicant3GuarantorInfo"), false);
	
	//Applicant 4
	String applicant4Bday = processPostValue(request.getParameter("applicant4Bday"), false);
	String applicant4MonthlyFixedIncome = processPostValue(request.getParameter("applicant4MonthlyFixedIncome"), true);
	String applicant4MonthlyVariableIncome = processPostValue(request.getParameter("applicant4MonthlyVariableIncome"), true);
	String applicant4AnnualBonus = processPostValue(request.getParameter("applicant4AnnualBonus"), true);
	String applicant4AnnualTradeIncome = processPostValue(request.getParameter("applicant4AnnualTradeIncome"), true);
	String applicant4RentalIncome = processPostValue(request.getParameter("applicant4RentalIncome"), false);
	String applicant4UnitTrusts = processPostValue(request.getParameter("applicant4UnitTrusts"), true);
	String applicant4UnitTrustsPledged = processPostValue(request.getParameter("applicant4UnitTrustsPledged"), false);
	String applicant4BondsAndStocksAndShares = processPostValue(request.getParameter("applicant4BondsAndStocksAndShares"), true);
	String applicant4BondsAndStocksAndSharesPledged = processPostValue(request.getParameter("applicant4BondsAndStocksAndSharesPledged"), false);
	String applicant4ForeignCurrencyDep = processPostValue(request.getParameter("applicant4ForeignCurrencyDep"), true);
	String applicant4ForeignCurrencyDepPledged = processPostValue(request.getParameter("applicant4ForeignCurrencyDepPledged"), false);
	String applicant4StructuredDep = processPostValue(request.getParameter("applicant4StructuredDep"), true);
	String applicant4StructuredDepPledged = processPostValue(request.getParameter("applicant4StructuredDepPledged"), false);
	String applicant4FixedOrSavingsDep = processPostValue(request.getParameter("applicant4FixedOrSavingsDep"), true);
	String applicant4FixedOrSavingsDepPledged = processPostValue(request.getParameter("applicant4FixedOrSavingsDepPledged"), false);
	String applicant4TotalCarLoans = processPostValue(request.getParameter("applicant4TotalCarLoans"), true);
	String applicant4TotalPropertyLoans = processPostValue(request.getParameter("applicant4TotalPropertyLoans"), true);
	String applicant4TotalPersonalRenovationLoans = processPostValue(request.getParameter("applicant4TotalPersonalRenovationLoans"), true);
	String applicant4CCAmountType = processPostValue(request.getParameter("applicant4CCAmountType"), false);
	String applicant4CCRepaymentAmount = processPostValue(request.getParameter("applicant4CCRepaymentAmount"), true);
	String applicant4RCCAmountType = processPostValue(request.getParameter("applicant4RCCAmountType"), false);
	String applicant4RCCRepaymentAmount = processPostValue(request.getParameter("applicant4RCCRepaymentAmount"), true);
	String applicant4IsGuarantor = processPostValue(request.getParameter("applicant4IsGuarantor"), false);
	String applicant4GuarantorInfo = processPostValue(request.getParameter("applicant4GuarantorInfo"), false);
	
	String txtStr = "";
	String txtFilePath = "";
	
	
	//Renaming some fields' value
	if(loanType.equals("loantype_new"))
		loanType = "New Purchase";
	else if(loanType.equals("loantype_re"))
		loanType = "Refinancing";
	
	if(loanOption.equals("loanoption_withoutamt"))
		loanOption = "Find out maximum loan amount and loan tenor";
	else if(loanOption.equals("loanoption_withamt"))
		loanOption = "Find out TDSR & MSR for requested loan amount";
	
	loanTenor = loanTenor + " years";
	if(!intendedPropertyType.equals("International_Property"))
	{
		intendedPropertyCountry = "";
		intendedPropertyCurrency = "";
		intendedPropertyTypeMy = "";
	}
	else if(!intendedPropertyCountry.equals("Malaysia"))
	{
		intendedPropertyTypeMy = "";
	}
	
	intendedPropertyType = replaceAll(intendedPropertyType, "_", " ");
	//End renaming fields
	
	
	
	if(formType.equals("apply"))
	{
		txtFilePath = applyFilePath;
		
		txtStr = processCSVValue(name) + "," +
				 nric + "," +
				 contactHome + "," +
				 contactOffice + "," +
				 contactMobile + "," +
				 email + "," +
				 processCSVValue(propertyAddress) + "," +
				 propertyType + "," +
				 propertyOpt + "," +
				 builtInArea + "," +
				 builtInAreaUnit + "," +
				 landArea + "," +
				 landAreaUnit + "," +
				 isRenovated + "," +
				 processCSVValue(renovationCost) + "," +
				 renovationMonth + "," +
				 renovationYear + ",";	
	}
	else if(formType.equals("chat"))
	{
		txtFilePath = chatFilePath;	
		
		txtStr = designation + "," +
				 processCSVValue(name) + "," +
				 nric + "," +
				 contactHome + "," +
				 contactOffice + "," +
				 contactMobile + ",";
	}
	
	txtStr += processCSVValue(maxLoanAmount) + "," +
			 loanTenor + "," +
			 TDSR + "," +
			 MSR + "," +
			 loanType + "," +
			 loanOption + "," +
			 intendedPropertyType + "," +
			 intendedPropertyCountry + "," +
			 intendedPropertyCurrency + "," +
			 intendedPropertyTypeMy + "," +
			 
			 applicant1Bday + "," +
			 processCSVValue(applicant1MonthlyFixedIncome)  + "," +
			 processCSVValue(applicant1MonthlyVariableIncome)  + "," +
			 processCSVValue(applicant1AnnualBonus)  + "," +
			 processCSVValue(applicant1AnnualTradeIncome)  + "," +
			 processCSVValue(applicant1RentalIncome)  + "," +
			 processCSVValue(applicant1UnitTrusts)  + "," +
			 processCSVValue(applicant1UnitTrustsPledged)  + "," +
			 processCSVValue(applicant1BondsAndStocksAndShares)  + "," +
			 processCSVValue(applicant1BondsAndStocksAndSharesPledged)  + "," +
			 processCSVValue(applicant1ForeignCurrencyDep)  + "," +
			 processCSVValue(applicant1ForeignCurrencyDepPledged)  + "," +
			 processCSVValue(applicant1StructuredDep)  + "," +
			 processCSVValue(applicant1StructuredDepPledged)  + "," +
			 processCSVValue(applicant1FixedOrSavingsDep)  + "," +
			 processCSVValue(applicant1FixedOrSavingsDepPledged)  + "," +
			 processCSVValue(applicant1TotalCarLoans)  + "," +
			 processCSVValue(applicant1TotalPropertyLoans)  + "," +
			 processCSVValue(applicant1TotalPersonalRenovationLoans)  + "," +
			 processCSVValue(applicant1CCAmountType)  + "," +
			 processCSVValue(applicant1CCRepaymentAmount)  + "," +
			 processCSVValue(applicant1RCCAmountType)  + "," +
			 processCSVValue(applicant1RCCRepaymentAmount)  + "," +
			 processCSVValue(applicant1IsGuarantor)  + "," +
			 processCSVValue(applicant1GuarantorInfo)  + "," +
			 
			 applicant2Bday + "," +
			 processCSVValue(applicant2MonthlyFixedIncome)  + "," +
			 processCSVValue(applicant2MonthlyVariableIncome)  + "," +
			 processCSVValue(applicant2AnnualBonus)  + "," +
			 processCSVValue(applicant2AnnualTradeIncome)  + "," +
			 processCSVValue(applicant2RentalIncome)  + "," +
			 processCSVValue(applicant2UnitTrusts)  + "," +
			 processCSVValue(applicant2UnitTrustsPledged)  + "," +
			 processCSVValue(applicant2BondsAndStocksAndShares)  + "," +
			 processCSVValue(applicant2BondsAndStocksAndSharesPledged)  + "," +
			 processCSVValue(applicant2ForeignCurrencyDep)  + "," +
			 processCSVValue(applicant2ForeignCurrencyDepPledged)  + "," +
			 processCSVValue(applicant2StructuredDep)  + "," +
			 processCSVValue(applicant2StructuredDepPledged)  + "," +
			 processCSVValue(applicant2FixedOrSavingsDep)  + "," +
			 processCSVValue(applicant2FixedOrSavingsDepPledged)  + "," +
			 processCSVValue(applicant2TotalCarLoans)  + "," +
			 processCSVValue(applicant2TotalPropertyLoans)  + "," +
			 processCSVValue(applicant2TotalPersonalRenovationLoans)  + "," +
			 processCSVValue(applicant2CCAmountType)  + "," +
			 processCSVValue(applicant2CCRepaymentAmount)  + "," +
			 processCSVValue(applicant2RCCAmountType)  + "," +
			 processCSVValue(applicant2RCCRepaymentAmount)  + "," +
			 processCSVValue(applicant2IsGuarantor)  + "," +
			 processCSVValue(applicant2GuarantorInfo)  + "," +
			 
			 applicant3Bday + "," +
			 processCSVValue(applicant3MonthlyFixedIncome)  + "," +
			 processCSVValue(applicant3MonthlyVariableIncome)  + "," +
			 processCSVValue(applicant3AnnualBonus)  + "," +
			 processCSVValue(applicant3AnnualTradeIncome)  + "," +
			 processCSVValue(applicant3RentalIncome)  + "," +
			 processCSVValue(applicant3UnitTrusts)  + "," +
			 processCSVValue(applicant3UnitTrustsPledged)  + "," +
			 processCSVValue(applicant3BondsAndStocksAndShares)  + "," +
			 processCSVValue(applicant3BondsAndStocksAndSharesPledged)  + "," +
			 processCSVValue(applicant3ForeignCurrencyDep)  + "," +
			 processCSVValue(applicant3ForeignCurrencyDepPledged)  + "," +
			 processCSVValue(applicant3StructuredDep)  + "," +
			 processCSVValue(applicant3StructuredDepPledged)  + "," +
			 processCSVValue(applicant3FixedOrSavingsDep)  + "," +
			 processCSVValue(applicant3FixedOrSavingsDepPledged)  + "," +
			 processCSVValue(applicant3TotalCarLoans)  + "," +
			 processCSVValue(applicant3TotalPropertyLoans)  + "," +
			 processCSVValue(applicant3TotalPersonalRenovationLoans)  + "," +
			 processCSVValue(applicant3CCAmountType)  + "," +
			 processCSVValue(applicant3CCRepaymentAmount)  + "," +
			 processCSVValue(applicant3RCCAmountType)  + "," +
			 processCSVValue(applicant3RCCRepaymentAmount)  + "," +
			 processCSVValue(applicant3IsGuarantor)  + "," +
			 processCSVValue(applicant3GuarantorInfo)  + "," +
			 
			 applicant4Bday + "," +
			 processCSVValue(applicant4MonthlyFixedIncome)  + "," +
			 processCSVValue(applicant4MonthlyVariableIncome)  + "," +
			 processCSVValue(applicant4AnnualBonus)  + "," +
			 processCSVValue(applicant4AnnualTradeIncome)  + "," +
			 processCSVValue(applicant4RentalIncome)  + "," +
			 processCSVValue(applicant4UnitTrusts)  + "," +
			 processCSVValue(applicant4UnitTrustsPledged)  + "," +
			 processCSVValue(applicant4BondsAndStocksAndShares)  + "," +
			 processCSVValue(applicant4BondsAndStocksAndSharesPledged)  + "," +
			 processCSVValue(applicant4ForeignCurrencyDep)  + "," +
			 processCSVValue(applicant4ForeignCurrencyDepPledged)  + "," +
			 processCSVValue(applicant4StructuredDep)  + "," +
			 processCSVValue(applicant4StructuredDepPledged)  + "," +
			 processCSVValue(applicant4FixedOrSavingsDep)  + "," +
			 processCSVValue(applicant4FixedOrSavingsDepPledged)  + "," +
			 processCSVValue(applicant4TotalCarLoans)  + "," +
			 processCSVValue(applicant4TotalPropertyLoans)  + "," +
			 processCSVValue(applicant4TotalPersonalRenovationLoans)  + "," +
			 processCSVValue(applicant4CCAmountType)  + "," +
			 processCSVValue(applicant4CCRepaymentAmount)  + "," +
			 processCSVValue(applicant4RCCAmountType)  + "," +
			 processCSVValue(applicant4RCCRepaymentAmount)  + "," +
			 processCSVValue(applicant4IsGuarantor)  + "," +
			 processCSVValue(applicant4GuarantorInfo)  + "," +
			 
			 new java.util.Date();
			 
	try 
	{   
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(txtFilePath, true), "UTF-8");
		BufferedWriter fbw = new BufferedWriter(writer);
		fbw.write(txtStr);
		fbw.newLine();
		fbw.close();
		
	} 
	catch(IOException e) 
	{
	   out.println(e.getMessage());
	}
  
  
	String result;
	
	// Get system properties object
	Properties properties = System.getProperties();
	
	// Setup mail server
	properties.setProperty("mail.smtp.host", host);
	properties.setProperty("mail.smtp.starttls.enable", "true");
	//properties.setProperty("mail.smtp.user", user);
	//properties.setProperty("mail.smtp.password", pass);
	properties.setProperty("mail.smtp.port", smtpPort);
	//properties.setProperty("mail.smtp.auth", "true");
	
	// Get the default Session object.
	Session mailSession = Session.getDefaultInstance(properties, null);
	
	try{
		// Create a default MimeMessage object.
		MimeMessage message = new MimeMessage(mailSession);
		// Set From: header field of the header.
		message.setFrom(new InternetAddress(from));
		// Set To: header field of the header.
		InternetAddress[] addressTo = new InternetAddress[to.length];
		for (int i = 0; i < to.length; i++)
		{
			addressTo[i] = new InternetAddress(to[i]);
		}
		message.setRecipients(javax.mail.internet.MimeMessage.RecipientType.TO, addressTo); 
							   
		String actualText = "";
		
		actualText += "<div style=\"margin-bottom: 20px;\"><span style=\"font-weight: bold; font-size: 16px;\">Customer's Particulars</span><table cellpadding=\"6\" cellspacing=\"0\" border=\"1\">";
					  
		if(formType.equals("apply"))
		{
			message.setSubject("UOB TDSR Calculator - Apply Now");
			actualText += "<tr><td width=\"300\">Name</td><td width=\"300\">" + name + "</td></tr>" +
					 "<tr><td>NRIC</td><td> " + nric + "</td></tr>" +
					 "<tr><td>Contact (Home)</td><td> " + contactHome + "</td></tr>" +
					 "<tr><td>Contact (Office)</td><td> " +  contactOffice + "</td></tr>" +
					 "<tr><td>Contact (Mobile)</td><td> " + contactMobile + "</td></tr>" +
					 "<tr><td>Email</td><td> " + email + "</td></tr>" +
					 "<tr><td>Property Address</td><td> " + propertyAddress + "</td></tr>" +
					 "<tr><td>Property Type</td><td> " + propertyType + "</td></tr>" +
					 "<tr><td>Property Option</td><td> " + propertyOpt + "</td></tr>" +
					 "<tr><td>Built In Area</td><td> " + builtInArea + " " + builtInAreaUnit + "</td></tr>" +
					 
					 "<tr><td>Land Area</td><td> " + landArea + " " + landAreaUnit + "</td></tr>" +
					 "<tr><td>Is Renovated</td><td> " + isRenovated + "</td></tr>" +
					 "<tr><td>Renovation Cost</td><td> " + renovationCost + "</td></tr>" +
					 "<tr><td>Renovation Month</td><td> " + renovationMonth + "</td></tr>" +
					 "<tr><td>Renovation Year</td><td> " + renovationYear + "</td></tr></table></div>";
		}
		else if(formType.equals("chat"))
		{
			message.setSubject("UOB TDSR Calculator - Let's Chat");
			actualText += "<tr><td width=\"300\">Salutation</td><td width=\"300\"> " + designation +  "</td></tr>" + 
					 "<tr><td>Name</td><td> " + name + "</td></tr>" +
					 "<tr><td>NRIC</td><td> " + nric + "</td></tr>" +
					 "<tr><td>Contact (Home)</td><td> " + contactHome + "</td></tr>" +
					 "<tr><td>Contact (Office)</td><td> " + contactOffice + "</td></tr>" +
					 "<tr><td>Contact (Mobile)</td><td> " + contactMobile + "</td></tr></table></div>";
		}  
		
		actualText += "<div style=\"margin-bottom: 20px;\"><span style=\"font-weight: bold; font-size: 16px;\">TDSR Result</span><table cellpadding=\"6\" cellspacing=\"0\" border=\"1\">" +
					  "<tr><td width=\"300\">Property Loan Amount</td><td width=\"300\"> " + maxLoanAmount + "</td></tr>" +
					  "<tr><td>Property Loan Tenor</td><td> " + loanTenor + "</td></tr>" +
					  "<tr><td>TDSR</td><td> " + TDSR + "</td></tr>" +
					  "<tr><td>MSR</td><td> " + MSR + "</td></tr>" +
					   "<tr><td>Loan Type</td><td> " + loanType + "</td></tr>" +
					  "<tr><td>Loan Option</td><td> " + loanOption + "</td></tr>" +
					  "<tr><td>Intended Property Type</td><td> " + intendedPropertyType + "</td></tr>" +
					  "<tr><td>Intended Property Country</td><td> " + intendedPropertyCountry + "</td></tr>" +
					  "<tr><td>Intended Property Currency</td><td> " + intendedPropertyCurrency + "</td></tr>" +
					  "<tr><td>Intended Property Type (Malaysia)</td><td> " + intendedPropertyTypeMy + "</td></tr></table></div>";
					  
		actualText += "<div style=\"margin-bottom: 20px;\"><span style=\"font-weight: bold; font-size: 16px;\">Applicant 1's Information</span><table cellpadding=\"6\" cellspacing=\"0\" border=\"1\">" +
					  "<tr><td  width=\"400\">Date of Birth (Day/Month/Year)</td><td  width=\"400\"> " + applicant1Bday + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">INCOME</span></td></tr>" + 
					  "<tr><td>Monthly Fixed Income</td><td> " + applicant1MonthlyFixedIncome + "</td></tr>" +
					  "<tr><td>Monthly Variable Income</td><td> " + applicant1MonthlyVariableIncome + "</td></tr>" +
					  "<tr><td>Annual Bonus</td><td> " + applicant1AnnualBonus + "</td></tr>" +
					  "<tr><td>Annual Trade Income</td><td> " + applicant1AnnualTradeIncome + "</td></tr>" +
					  "<tr><td>Rental Income</td><td> " + replaceAll(applicant1RentalIncome, "|", "<br />") + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">FINANCIAL ASSETS</span></td></tr>" + 
					  "<tr><td>Unit Trusts</td><td> " + applicant1UnitTrusts + "</td></tr>" +
					  "<tr><td>Unit Trusts Pledged</td><td> " + applicant1UnitTrustsPledged + "</td></tr>" +
					  "<tr><td>Bonds and Stocks and Shares</td><td> " + applicant1BondsAndStocksAndShares + "</td></tr>" +
					  "<tr><td>Bonds and Stocks and Shares Pledged</td><td> " + applicant1BondsAndStocksAndSharesPledged + "</td></tr>" +
					  "<tr><td>Foreign Currency Deposits</td><td> " + applicant1ForeignCurrencyDep + "</td></tr>" +
					  "<tr><td>Foreign Currency Deposits Pledged</td><td> " + applicant1ForeignCurrencyDepPledged + "</td></tr>" +
					  "<tr><td>Structured Deposits</td><td> " + applicant1StructuredDep + "</td></tr>" +
					  "<tr><td>Structured Deposits Pledged</td><td> " + applicant1StructuredDepPledged + "</td></tr>" +
					  "<tr><td>Fixed or Savings Deposits</td><td> " + applicant1FixedOrSavingsDep + "</td></tr>" +
					  "<tr><td>Fixed or Savings Deposits Pledged</td><td> " + applicant1FixedOrSavingsDepPledged + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">DEBT OBLIGATIONS</span></td></tr>" + 
					  "<tr><td>Total Monthly Instalment Car Loans</td><td> " + applicant1TotalCarLoans + "</td></tr>" +
					  "<tr><td>Total Monthly Instalment Other Property Loans</td><td> " + applicant1TotalPropertyLoans + "</td></tr>" +
					  "<tr><td>Total Monthly Instalment Other Personal Renovation Loans</td><td> " + applicant1TotalPersonalRenovationLoans + "</td></tr>" +
					  "<tr><td>CC Amount Type</td><td> " + applicant1CCAmountType + "</td></tr>" +
					  "<tr><td>Total Monthly Repayment for Credit Cards</td><td> " + applicant1CCRepaymentAmount + "</td></tr>" +
					  "<tr><td>Unsecured Non-Card Credit Facility Amount Type</td><td> " + applicant1RCCAmountType + "</td></tr>" +
					  "<tr><td>Total Monthly Repayment for Unsecured Non-Card Credit Facilities</td><td> " + applicant1RCCRepaymentAmount + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">GUARANTOR STATUS</span></td></tr>" + 
					  "<tr><td>Guarantor for Any Loans?</td><td> " + applicant1IsGuarantor + "</td></tr>" +
					  "<tr><td>Guarantor Loan Amount</td><td> " + replaceAll(applicant1GuarantorInfo, "|", "<br />") + "</td></tr></table></div>";
		
		if(!applicant2Bday.equals(""))
		{
			actualText += "<div style=\"margin-bottom: 20px;\"><span style=\"font-weight: bold; font-size: 16px;\">Applicant 2's Information</span><table cellpadding=\"6\" cellspacing=\"0\" border=\"1\">" +
					  "<tr><td width=\"400\">Date of Birth (Day/Month/Year)</td><td width=\"400\"> " + applicant2Bday + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">INCOME</span></td></tr>" + 
					  "<tr><td>Monthly Fixed Income</td><td> " + applicant2MonthlyFixedIncome + "</td></tr>" +
					  "<tr><td>Monthly Variable Income</td><td> " + applicant2MonthlyVariableIncome + "</td></tr>" +
					  "<tr><td>Annual Bonus</td><td> " + applicant2AnnualBonus + "</td></tr>" +
					  "<tr><td>Annual Trade Income</td><td> " + applicant2AnnualTradeIncome + "</td></tr>" +
					  "<tr><td>Rental Income</td><td> " + replaceAll(applicant2RentalIncome, "|", "<br />") + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">FINANCIAL ASSETS</span></td></tr>" + 
					  "<tr><td>Unit Trusts</td><td> " + applicant2UnitTrusts + "</td></tr>" +
					  "<tr><td>Unit Trusts Pledged</td><td> " + applicant2UnitTrustsPledged + "</td></tr>" +
					  "<tr><td>Bonds and Stocks and Shares</td><td> " + applicant2BondsAndStocksAndShares + "</td></tr>" +
					  "<tr><td>Bonds and Stocks and Shares Pledged</td><td> " + applicant2BondsAndStocksAndSharesPledged + "</td></tr>" +
					  "<tr><td>Foreign Currency Deposits</td><td> " + applicant2ForeignCurrencyDep + "</td></tr>" +
					  "<tr><td>Foreign Currency Deposits Pledged</td><td> " + applicant2ForeignCurrencyDepPledged + "</td></tr>" +
					  "<tr><td>Structured Deposits</td><td> " + applicant2StructuredDep + "</td></tr>" +
					  "<tr><td>Structured Deposits Pledged</td><td> " + applicant2StructuredDepPledged + "</td></tr>" +
					  "<tr><td>Fixed or Savings Deposits</td><td> " + applicant2FixedOrSavingsDep + "</td></tr>" +
					  "<tr><td>Fixed or Savings Deposits Pledged</td><td> " + applicant2FixedOrSavingsDepPledged + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">DEBT OBLIGATIONS</span></td></tr>" + 
					  "<tr><td>Total Monthly Instalment Car Loans</td><td> " + applicant2TotalCarLoans + "</td></tr>" +
					  "<tr><td>Total Monthly Instalment Other Property Loans</td><td> " + applicant2TotalPropertyLoans + "</td></tr>" +
					  "<tr><td>Total Monthly Instalment Other Personal Renovation Loans</td><td> " + applicant2TotalPersonalRenovationLoans + "</td></tr>" +
					  "<tr><td>CC Amount Type</td><td> " + applicant2CCAmountType + "</td></tr>" +
					  "<tr><td>Total Monthly Repayment for Credit Cards</td><td> " + applicant2CCRepaymentAmount + "</td></tr>" +
					  "<tr><td>Unsecured Non-Card Credit Facility Amount Type</td><td> " + applicant2RCCAmountType + "</td></tr>" +
					  "<tr><td>Total Monthly Repayment for Unsecured Non-Card Credit Facilities</td><td> " + applicant2RCCRepaymentAmount + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">GUARANTOR STATUS</span></td></tr>" + 
					  "<tr><td>Guarantor for Any Loans?</td><td> " + applicant2IsGuarantor + "</td></tr>" +
					  "<tr><td>Guarantor Loan Amount</td><td> " + replaceAll(applicant2GuarantorInfo, "|", "<br />") + "</td></tr></table></div>";
		}
		if(!applicant3Bday.equals(""))
		{
			actualText += "<div style=\"margin-bottom: 20px;\"><span style=\"font-weight: bold; font-size: 16px;\">Applicant 3's Information</span><table cellpadding=\"6\" cellspacing=\"0\" border=\"1\">" +
					  "<tr><td width=\"400\">Date of Birth (Day/Month/Year)</td><td width=\"400\"> " + applicant3Bday + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">INCOME</span></td></tr>" + 
					  "<tr><td>Monthly Fixed Income</td><td> " + applicant3MonthlyFixedIncome + "</td></tr>" +
					  "<tr><td>Monthly Variable Income</td><td> " + applicant3MonthlyVariableIncome + "</td></tr>" +
					  "<tr><td>Annual Bonus</td><td> " + applicant3AnnualBonus + "</td></tr>" +
					  "<tr><td>Annual Trade Income</td><td> " + applicant3AnnualTradeIncome + "</td></tr>" +
					  "<tr><td>Rental Income</td><td> " + replaceAll(applicant3RentalIncome, "|", "<br />") + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">FINANCIAL ASSETS</span></td></tr>" + 
					  "<tr><td>Unit Trusts</td><td> " + applicant3UnitTrusts + "</td></tr>" +
					  "<tr><td>Unit Trusts Pledged</td><td> " + applicant3UnitTrustsPledged + "</td></tr>" +
					  "<tr><td>Bonds and Stocks and Shares</td><td> " + applicant3BondsAndStocksAndShares + "</td></tr>" +
					  "<tr><td>Bonds and Stocks and Shares Pledged</td><td> " + applicant3BondsAndStocksAndSharesPledged + "</td></tr>" +
					  "<tr><td>Foreign Currency Deposits</td><td> " + applicant3ForeignCurrencyDep + "</td></tr>" +
					  "<tr><td>Foreign Currency Deposits Pledged</td><td> " + applicant3ForeignCurrencyDepPledged + "</td></tr>" +
					  "<tr><td>Structured Deposits</td><td> " + applicant3StructuredDep + "</td></tr>" +
					  "<tr><td>Structured Deposits Pledged</td><td> " + applicant3StructuredDepPledged + "</td></tr>" +
					  "<tr><td>Fixed or Savings Deposits</td><td> " + applicant3FixedOrSavingsDep + "</td></tr>" +
					  "<tr><td>Fixed or Savings Deposits Pledged</td><td> " + applicant3FixedOrSavingsDepPledged + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">DEBT OBLIGATIONS</span></td></tr>" + 
					  "<tr><td>Total Monthly Instalment Car Loans</td><td> " + applicant3TotalCarLoans + "</td></tr>" +
					  "<tr><td>Total Monthly Instalment Other Property Loans</td><td> " + applicant3TotalPropertyLoans + "</td></tr>" +
					  "<tr><td>Total Monthly Instalment Other Personal Renovation Loans</td><td> " + applicant3TotalPersonalRenovationLoans + "</td></tr>" +
					  "<tr><td>CC Amount Type</td><td> " + applicant3CCAmountType + "</td></tr>" +
					  "<tr><td>Total Monthly Repayment for Credit Cards</td><td> " + applicant3CCRepaymentAmount + "</td></tr>" +
					  "<tr><td>Unsecured Non-Card Credit Facility Amount Type</td><td> " + applicant3RCCAmountType + "</td></tr>" +
					  "<tr><td>Total Monthly Repayment for Unsecured Non-Card Credit Facilities</td><td> " + applicant3RCCRepaymentAmount + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">GUARANTOR STATUS</span></td></tr>" + 
					  "<tr><td>Guarantor for Any Loans?</td><td> " + applicant3IsGuarantor + "</td></tr>" +
					  "<tr><td>Guarantor Loan Amount</td><td> " + replaceAll(applicant3GuarantorInfo, "|", "<br />") + "</td></tr></table></div>";
		}
		if(!applicant4Bday.equals(""))
		{
			actualText += "<div style=\"margin-bottom: 20px;\"><span style=\"font-weight: bold; font-size: 16px;\">Applicant 4's Information</span><table cellpadding=\"6\" cellspacing=\"0\" border=\"1\">" +
					  "<tr><td width=\"400\">Date of Birth (Day/Month/Year)</td><td width=\"400\"> " + applicant4Bday + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">INCOME</span></td></tr>" + 
					  "<tr><td>Monthly Fixed Income</td><td> " + applicant4MonthlyFixedIncome + "</td></tr>" +
					  "<tr><td>Monthly Variable Income</td><td> " + applicant4MonthlyVariableIncome + "</td></tr>" +
					  "<tr><td>Annual Bonus</td><td> " + applicant4AnnualBonus + "</td></tr>" +
					  "<tr><td>Annual Trade Income</td><td> " + applicant4AnnualTradeIncome + "</td></tr>" +
					  "<tr><td>Rental Income</td><td> " + replaceAll(applicant4RentalIncome, "|", "<br />") + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">FINANCIAL ASSETS</span></td></tr>" + 
					  "<tr><td>Unit Trusts</td><td> " + applicant4UnitTrusts + "</td></tr>" +
					  "<tr><td>Unit Trusts Pledged</td><td> " + applicant4UnitTrustsPledged + "</td></tr>" +
					  "<tr><td>Bonds and Stocks and Shares</td><td> " + applicant4BondsAndStocksAndShares + "</td></tr>" +
					  "<tr><td>Bonds and Stocks and Shares Pledged</td><td> " + applicant4BondsAndStocksAndSharesPledged + "</td></tr>" +
					  "<tr><td>Foreign Currency Deposits</td><td> " + applicant4ForeignCurrencyDep + "</td></tr>" +
					  "<tr><td>Foreign Currency Deposits Pledged</td><td> " + applicant4ForeignCurrencyDepPledged + "</td></tr>" +
					  "<tr><td>Structured Deposits</td><td> " + applicant4StructuredDep + "</td></tr>" +
					  "<tr><td>Structured Deposits Pledged</td><td> " + applicant4StructuredDepPledged + "</td></tr>" +
					  "<tr><td>Fixed or Savings Deposits</td><td> " + applicant4FixedOrSavingsDep + "</td></tr>" +
					  "<tr><td>Fixed or Savings Deposits Pledged</td><td> " + applicant4FixedOrSavingsDepPledged + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">DEBT OBLIGATIONS</span></td></tr>" + 
					  "<tr><td>Total Monthly Instalment Car Loans</td><td> " + applicant4TotalCarLoans + "</td></tr>" +
					  "<tr><td>Total Monthly Instalment Other Property Loans</td><td> " + applicant4TotalPropertyLoans + "</td></tr>" +
					  "<tr><td>Total Monthly Instalment Other Personal Renovation Loans</td><td> " + applicant4TotalPersonalRenovationLoans + "</td></tr>" +
					  "<tr><td>CC Amount Type</td><td> " + applicant4CCAmountType + "</td></tr>" +
					  "<tr><td>Total Monthly Repayment for Credit Cards</td><td> " + applicant4CCRepaymentAmount + "</td></tr>" +
					  "<tr><td>Unsecured Non-Card Credit Facility Amount Type</td><td> " + applicant4RCCAmountType + "</td></tr>" +
					  "<tr><td>Total Monthly Repayment for Unsecured Non-Card Credit Facilities</td><td> " + applicant4RCCRepaymentAmount + "</td></tr>" +
					  "<tr><td colspan=\"2\"><span style=\"color: #6d9dda\">GUARANTOR STATUS</span></td></tr>" + 
					  "<tr><td>Guarantor for Any Loans?</td><td> " + applicant4IsGuarantor + "</td></tr>" +
					  "<tr><td>Guarantor Loan Amount</td><td> " + replaceAll(applicant4GuarantorInfo, "|", "<br />") + "</td></tr></table></div>";
		}
		
		
		message.setContent(actualText, "text/html; charset=ISO-8859-1");
		// Send message
		//Transport.send(message);
		
		Transport transport = mailSession.getTransport("smtp");
		transport.connect(host, user, pass);
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();
		
		result = "Sent message successfully....";
	  
	} catch (MessagingException mex) {
		mex.printStackTrace();
		result = "Error: unable to send message....";
	}
   
   
%>

<%!
	String currencySymbol = "S$";
	
	public String processPostValue(String value, boolean addCurrency){
		String returnVal = "";
		
		if(value != null && !value.equals("0") && !value.equals(""))
		{
			if(addCurrency == true)
			{
				returnVal = currencySymbol + value;
			}
			else
			{
				returnVal = value;
			}
			
		}
		
		return returnVal;
	}
	public String processCSVValue(String value){
		String returnVal = "";
		
		if(value.indexOf(",") != -1)
		{
			returnVal = "\"" + value + "\"";
		}
		else
			returnVal = value;
		
		return returnVal;
	}
	
	public static String replaceAll(String target, String from, String to) {   
		//   target is the original string
		//   from   is the string to be replaced
		//   to     is the string which will used to replace
		//  returns a new String!
		int start = target.indexOf(from);
		if (start == -1) return target;
		int lf = from.length();
		char [] targetChars = target.toCharArray();
		StringBuffer buffer = new StringBuffer();
		int copyFrom = 0;
		while (start != -1) {
			buffer.append (targetChars, copyFrom, start - copyFrom);
			buffer.append (to);
			copyFrom = start + lf;
			start = target.indexOf (from, copyFrom);
		}
		buffer.append (targetChars, copyFrom, targetChars.length - copyFrom);
		return buffer.toString();
	}
%>
